﻿using System.Web.Mvc;

namespace YK.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return Redirect("/Web/index.html");
        }

        public ActionResult Login()
        {
            return Redirect("/Web/index.html");
        }

    }
}
