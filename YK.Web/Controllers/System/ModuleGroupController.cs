﻿using System.Web.Mvc;
using Core;
using YK.Service.System.ModuleGroup;

namespace YK.Web.Controllers.Systems
{
    public class ModuleGroupController : BaseController
    {
        private readonly IModuleGroupService _moduleGroupService;

        public ModuleGroupController(
            IModuleGroupService moduleGroupService)
        {
            _moduleGroupService = moduleGroupService;
        }

        public JsonResult GetAllModuleGroups()
        {
            return JsonWithContext(_moduleGroupService.GetAllModuleGroups(), JsonRequestBehavior.DenyGet);
        }
    }
}