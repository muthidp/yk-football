﻿using System.Web.Mvc;
using Core;
using Core.DTO.Sys;
using YK.Service.System.User;

namespace YK.Web.Controllers.System
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(
            IUserService userService)
        {
            _userService = userService;
        }

        public JsonResult GetCurrentUser()
        {
            return JsonWithContext(_userService.GetCurrentUser(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetUserInfo(string username)
        {
            return JsonWithContext(_userService.GetUserInfo(username), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetUserInstructors(long? institutionId)
        {
            return JsonWithContext(_userService.GetUserInstructors(institutionId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel)
        {
            return JsonWithContext(_userService.GetAllWithBaseQuery(institutionId, ref queryModel),
                JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            return JsonWithContext(_userService.GetAll(institutionId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(UserModel dataModel)
        {
            return JsonWithContext(_userService.Save(dataModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long userId)
        {
            return JsonWithContext(_userService.Delete(userId), JsonRequestBehavior.DenyGet);
        }
    }
}