﻿using System.Web.Mvc;
using Core;
using YK.Service.System.Institution;

namespace YK.Web.Controllers.System
{
    public class InstitutionController : BaseController
    {
        private readonly IInstitutionService _institutionService;

        public InstitutionController(
            IInstitutionService institutionService
            )
        {
            _institutionService = institutionService;
        }

        public JsonResult GetInstitutionByCode(string institutionCode)
        {
            return JsonWithContext(_institutionService.FindOne(institutionCode), JsonRequestBehavior.AllowGet);
        }
    }
}