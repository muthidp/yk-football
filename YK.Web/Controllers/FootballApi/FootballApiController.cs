﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using YK.Service.FootballAPI;

namespace YK.Web.Controllers.FootballApi
{
    public class FootballApiController : BaseController
    {
        private readonly IFootballApiService _footballApiService;

        public FootballApiController(
            IFootballApiService footballApiService)
        {
            _footballApiService = footballApiService;
        }

        public JsonResult GetCompetitions()
        {
            var data = _footballApiService.GetCompetitions();
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTodayMatches(int competitionId)
        {
            var data = _footballApiService.GetTodayMatches(competitionId);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMatchesByDate(int competitionId, string date)
        {
            var data = _footballApiService.GetMatchesByDate(competitionId, date);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStandings(int competitionId)
        {
            var data = _footballApiService.GetStandings(competitionId);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

    }
}