﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.League;

namespace YK.Web.Controllers.Yk
{
    public class LeagueController : BaseController
    {
        private readonly ILeagueService _leagueService;

        public LeagueController(
            ILeagueService leagueService)
        {
            _leagueService = leagueService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var leagues = _leagueService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", leagues},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var leagues = _leagueService.GetAll();
            return JsonWithContext(leagues, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetOne(long? leagueId)
        {
            var league = _leagueService.GetOne(leagueId);
            return JsonWithContext(league, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(LeagueModel League)
        {
            return JsonWithContext(_leagueService.Save(League), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long LeagueId)
        {
            return JsonWithContext(_leagueService.Delete(LeagueId), JsonRequestBehavior.DenyGet);
        }
    }
}