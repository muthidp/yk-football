﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.Tag;

namespace YK.Web.Controllers.Yk
{
    public class TagController : BaseController
    {
        private readonly ITagService _TagService;

        public TagController(
            ITagService TagService)
        {
            _TagService = TagService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var courses = _TagService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _TagService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(TagModel Tag)
        {
            return JsonWithContext(_TagService.Save(Tag), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long TagId)
        {
            return JsonWithContext(_TagService.Delete(TagId), JsonRequestBehavior.DenyGet);
        }
    }
}