﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.Troll;

namespace YK.Web.Controllers.Yk
{
    public class TrollController : BaseController
    {
        private readonly ITrollService _trollService;

        public TrollController(
            ITrollService trollService)
        {
            _trollService = trollService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var trolls = _trollService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", trolls},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetNewestTrolls(int? limit = null, long? excludeTrollId = null)
        {
            return JsonWithContext(_trollService.GetNewestTrolls(limit, excludeTrollId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetSingleTroll(long trollId)
        {
            return JsonWithContext(_trollService.GetSingleTroll(trollId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(TrollModel dataModel)
        {
            return JsonWithContext(_trollService.Save(dataModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult SaveComment(TrollCommentModel dataModel)
        {
            return JsonWithContext(_trollService.SaveComment(dataModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult LikeTroll(long? trollId, long? userId)
        {
            return JsonWithContext(_trollService.LikeTroll(trollId,userId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetComments(long trollId)
        {
            return JsonWithContext(_trollService.GetComments(trollId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetTopTrolls(int? limit=null)
        {
            return JsonWithContext(_trollService.GetTopTrolls(limit), JsonRequestBehavior.DenyGet);
        }

        public JsonResult VoteComment(long commentId, long userId, bool isUpVote)
        {
            return JsonWithContext(_trollService.VoteComment(commentId, userId, isUpVote), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetComment(long commentId)
        {
            return JsonWithContext(_trollService.GetComment(commentId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult SaveReply(CommentReplyModel model)
        {
            return JsonWithContext(_trollService.SaveReply(model), JsonRequestBehavior.DenyGet);
        }
    }
}