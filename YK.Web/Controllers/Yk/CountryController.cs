﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;
using YK.Service.Yk.Country;

namespace YK.Web.Controllers.Yk
{
    public class CountryController : BaseController
    {
        private readonly ICountryService _countryService;

        public CountryController(
            ICountryService countryService )
        {
            _countryService = countryService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var courses = _countryService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _countryService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(CountryModel country)
        {
            return JsonWithContext(_countryService.Save(country), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long countryId)
        {
            return JsonWithContext(_countryService.Delete(countryId), JsonRequestBehavior.DenyGet);
        }
    }
}