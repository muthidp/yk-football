﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.Ad;

namespace YK.Web.Controllers.Yk
{
    public class AdController : BaseController
    {
        private readonly IAdService _AdService;

        public AdController(
            IAdService AdService)
        {
            _AdService = AdService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var courses = _AdService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _AdService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(AdModel Ad)
        {
            return JsonWithContext(_AdService.Save(Ad), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long AdId)
        {
            return JsonWithContext(_AdService.Delete(AdId), JsonRequestBehavior.DenyGet);
        }
    }
}