﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Facebook;
using YK.Service.Yk.Post;

namespace YK.Web.Controllers.Yk
{
    public class PostController : BaseController
    {
        private readonly IPostService _postService;
        private readonly IFbService _fbService;

        public PostController(
            IPostService postService,
            IFbService fbService)
        {
            _postService = postService;
            _fbService = fbService;
        }

        public JsonResult Save(PostModel dataModel)
        {
            return JsonWithContext(_postService.Save(dataModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetSinglePost(long postId)
        {
            return JsonWithContext(_postService.GetSinglePost(postId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetComments(long postId)
        {
            return JsonWithContext(_postService.GetComments(postId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult SaveComment(CommentModel dataModel)
        {
            return JsonWithContext(_postService.SaveComment(dataModel), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetNewestPosts(int? limit = null, long? excludePostId = null)
        {
            return JsonWithContext(_postService.GetNewestPosts(limit, excludePostId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetHomeTimeline()
        {
            return JsonWithContext(_postService.GetHomeTimeline(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetTimeline(long? leagueId, long? categoryId, long? teamId, long? countryId, BaseQueryModel queryModel)
        {
            var posts = _postService.GetTimeline(leagueId, categoryId, teamId, countryId, ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", posts},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult VoteComment(long commentId, long userId, bool isUpVote)
        {
            return JsonWithContext(_postService.VoteComment(commentId, userId, isUpVote), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetComment(long commentId)
        {
            return JsonWithContext(_postService.GetComment(commentId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult SaveReply(CommentReplyModel model)
        {
            return JsonWithContext(_postService.SaveReply(model), JsonRequestBehavior.DenyGet);
        }

        public JsonResult PostAtricleToFacebook(PostModel post, string message, string link)
        {
            return JsonWithContext(_fbService.PostToFacebook(post, message, link), JsonRequestBehavior.DenyGet);
        }

        public JsonResult PostTrollToFacebook(TrollModel troll, string message, string link)
        {
            return JsonWithContext(_fbService.PostToFacebook(troll, message, link), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long postId)
        {
            return JsonWithContext(_postService.Delete(postId), JsonRequestBehavior.DenyGet);
        }
    }
}