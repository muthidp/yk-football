﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.Category;

namespace YK.Web.Controllers.Yk
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(
            ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var courses = _categoryService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _categoryService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAllForMenu()
        {
            var cats = _categoryService.GetAllForMenu();
            return JsonWithContext(cats, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(CategoryModel category)
        {
            return JsonWithContext(_categoryService.Save(category), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long categoryId)
        {
            return JsonWithContext(_categoryService.Delete(categoryId), JsonRequestBehavior.DenyGet);
        }
    }
}