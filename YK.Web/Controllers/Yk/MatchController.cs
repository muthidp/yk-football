﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.FootballApi;
using Core.DTO.Yk;
using Newtonsoft.Json.Linq;
using YK.Service.Yk.Match;

namespace YK.Web.Controllers.Yk
{
    public class MatchController : BaseController
    {
        private readonly IMatchService _service;

        public MatchController(
            IMatchService service)
        {
            _service = service;
        }

        public JsonResult SaveComment(MatchModel matchModel, List<MatchCommentModel> comments)
        {
            return JsonWithContext(_service.SaveComment(matchModel, comments), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetComments(long? footballMatchId)
        {
            return JsonWithContext(_service.GetComments(footballMatchId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetMatchesThisWeek()
        {
            return JsonWithContext(_service.GetMatchesThisWeek(), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetMatchesByDate(string date)
        {
            return JsonWithContext(_service.GetMatchesByDate(date), JsonRequestBehavior.DenyGet);
        }

        public JsonResult SyncFixtures(string date)
        {
            var info = _service.SyncFixtures(date);
            return JsonWithContext(info, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLastTeamStanding(long? teamId)
        {
            var data =_service.GetLastTeamStanding(teamId);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteComment(long? matchId, long? matchCommentId)
        {
            var data = _service.DeleteComment(matchId, matchCommentId);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMatchEvents(long? matchId)
        {
            var data = _service.GetMatchEvents(matchId);
            return JsonWithContext(data, JsonRequestBehavior.AllowGet);
        }

    }
}