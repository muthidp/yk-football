﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;
using Core.DTO.Yk;
using YK.Service.Yk.Team;

namespace YK.Web.Controllers.Yk
{
    public class TeamController : BaseController
    {
        private readonly ITeamService _TeamService;

        public TeamController(
            ITeamService TeamService)
        {
            _TeamService = TeamService;
        }

        public JsonResult GetAllForView(BaseQueryModel queryModel)
        {
            var courses = _TeamService.GetAllWithBaseQuery(ref queryModel);
            return JsonWithContext(new Dictionary<string, object>
            {
                {"list", courses},
                {"totalData", queryModel.TotalData},
                {"totalPage", queryModel.TotalPage}
            }, JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetAll(long? institutionId)
        {
            var courses = _TeamService.GetAll();
            return JsonWithContext(courses, JsonRequestBehavior.DenyGet);
        }

        public JsonResult Save(TeamModel Team)
        {
            return JsonWithContext(_TeamService.Save(Team), JsonRequestBehavior.DenyGet);
        }

        public JsonResult Delete(long TeamId)
        {
            return JsonWithContext(_TeamService.Delete(TeamId), JsonRequestBehavior.DenyGet);
        }

        public JsonResult GetTeamsToShowInMenu()
        {
            return JsonWithContext(_TeamService.GetTeamsToShowInMenu(), JsonRequestBehavior.DenyGet);
        }
    }
}