﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Core;

namespace YK.Web.Controllers.Tools
{
    public class ToolsController : BaseController
    {


        public JsonResult GetThursday()
        {
            var today = DateTime.Today;
            

            return JsonWithContext(null, JsonRequestBehavior.AllowGet);
        }

        private DateTime FindThursday(DateTime date)
        {
            for (int i = 7; i > 0; i--)
            {
                var newDate = date.AddDays(0);
                var dayOfWeek = newDate.DayOfWeek;
                if (dayOfWeek == DayOfWeek.Thursday)
                {
                    return newDate;
                }
            }
            return date;
        }
    }
}