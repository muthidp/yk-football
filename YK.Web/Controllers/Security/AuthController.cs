﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Text;
using Core;
using Core.DTO.Sys;
using DotNetOpenAuth.AspNet.Clients;
using Microsoft.Web.WebPages.OAuth;
using Newtonsoft.Json.Linq;
using YK.Service.Authentication;
using YK.Service.Facebook;
using YK.Service.System.User;
using YK.Web.Filters;

namespace YK.Web.Controllers.Security
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AuthController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        private readonly IFbService _fbService;

        public AuthController(
            IAuthenticationService authenticationService,
            IUserService userService,
            IFbService fbService)
        {
            _authenticationService = authenticationService;
            _userService = userService;
            _fbService = fbService;
        }

        [AllowAnonymous]
        public JsonResult Login(string userId, string password)
        {
            return JsonWithContext(_authenticationService.Authenticate(userId, password), JsonRequestBehavior.DenyGet);
        }

        [AllowAnonymous]
        public JsonResult IsLogin()
        {
            return JsonWithContext(_authenticationService.IsLogin(), JsonRequestBehavior.DenyGet);
        }

        [AllowAnonymous]
        public JsonResult Logout()
        {
            _authenticationService.Logout();
            return JsonWithContext(true, JsonRequestBehavior.DenyGet);
        }

        public JsonResult ExternalLoginList()
        {
            return JsonWithContext(OAuthWebSecurity.RegisteredClientData, JsonRequestBehavior.DenyGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ExternalLogin(string provider)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalCallback"));
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ExternalCallback()
        {
            var result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalCallback"));
            if (result.IsSuccessful)
            {
                Session["fb_data"] = result.ExtraData;

                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                var user = new UserModel()
                {
                    UserUniqueId = result.ProviderUserId,
                    Username = result.UserName,
                    Provider = result.Provider
                };
                var info = _userService.SaveUserExternalLogin(user);
                if (info.IsSuccess())
                    FormsAuthentication.SetAuthCookie(user.Username, false);
            }

            return RedirectToAction("Index", "Home");
        }

        private JObject HttpRequest(string url)
        {
            var webRequest = WebRequest.Create(url);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            JObject result = null;
            try
            {
                var webResponse = webRequest.GetResponse();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(webResponse.GetResponseStream());
                    var data = sr.ReadToEnd();
                    result = JObject.Parse(data);
                }
                finally
                {
                    if (sr != null) sr.Close();
                }
            }
            catch (WebException ex)
            {
                StreamReader errorStream = null;
                try
                {
                    errorStream = new StreamReader(ex.Response.GetResponseStream());
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (errorStream != null) errorStream.Close();
                }

            }

            return result;
        }

    }
}