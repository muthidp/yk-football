﻿using System.Web.Mvc;
using Core;
using Core.DTO.Md;
using YK.Service.Md.FileUpload;

namespace YK.Web.Controllers.Md
{
    public class FileUploadController : BaseController
    {
        private readonly IFileUploadService _service;

        public FileUploadController(
            IFileUploadService service)
        {
            _service = service;
        }

        [HttpPost]
        public JsonResult Upload(FileUploadModel data)
        {
            return JsonWithContext(_service.Upload(data), JsonRequestBehavior.DenyGet);
        }
    }
}