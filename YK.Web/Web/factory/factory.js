﻿angular.module('YK')
    .factory('InstitutionService', [
        '$resource', function ($resource) {
            return $resource('/Institution/:verb', {}, {
                GetAllInstitution: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllInstitution'
                    }
                },
                GetInstitutionByCode: {
                    method: 'POST',
                    params: {
                        verb: 'GetInstitutionByCode'
                    }
                },
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])
    .factory('UserService', [
        '$resource', function ($resource) {
            return $resource('/User/:verb', {}, {
                GetCurrentUser: {
                    method: 'POST',
                    params: {
                        verb: 'GetCurrentUser'
                    }
                },
                GetUserInfo: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserInfo'
                    }
                },
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetUserInstructors: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserInstructors'
                    }
                },
                GetUserStudents: {
                    method: 'POST',
                    params: {
                        verb: 'GetUserStudents'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                },
                CheckLimit: {
                    method: 'POST',
                    params: {
                        verb: 'CheckLimit'
                    }
                }
            });
        }
    ])
    .factory('RoleService', [
        '$resource', function ($resource) {
            return $resource('/Role/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])
    .factory('AuthenticationService', [
        '$resource', function ($resource) {
            return $resource('/Auth/:verb', {}, {
                Login: {
                    method: 'POST',
                    params: {
                        verb: 'Login'

                    }
                },
                IsLogin: {
                    method: 'POST',
                    params: {
                        verb: 'IsLogin'
                    }
                },
                Logout: {
                    method: 'POST',
                    params: {
                        verb: 'Logout'
                    }
                },
                ExternalLoginList: {
                    method: 'POST',
                    params: {
                        verb: 'ExternalLoginList'
                    }
                },
                ExternalLogin: {
                    method: 'POST',
                    params: {
                        verb: 'ExternalLogin'
                    }
                }
            });
        }
    ])
    .factory('ModuleGroupService', [
        '$resource', function ($resource) {
            return $resource('/ModuleGroup/:verb', {}, {
                GetAllModuleGroups: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllModuleGroups'
                    }
                }
            });
        }
    ])

    .factory('CategoryService', [
        '$resource', function ($resource) {
            return $resource('/Category/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                },
				GetAllForMenu: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForMenu'
                    }
                }
            });
        }
    ])

    .factory('CountryService', [
        '$resource', function ($resource) {
            return $resource('/Country/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('TagService', [
        '$resource', function ($resource) {
            return $resource('/Tag/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('TeamService', [
        '$resource', function ($resource) {
            return $resource('/Team/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                },
				GetTeamsToShowInMenu: {
                    method: 'POST',
                    params: {
                        verb: 'GetTeamsToShowInMenu'
                    }
                }
            });
        }
    ])

    .factory('LeagueService', [
        '$resource', function ($resource) {
            return $resource('/League/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                GetOne: {
                    method: 'POST',
                    params: {
                        verb: 'GetOne'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('PostService', [
        '$resource', function ($resource) {
            return $resource('/Post/:verb', {}, {
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                GetSinglePost: {
                    method: 'POST',
                    params: {
                        verb: 'GetSinglePost'
                    }
                },
                GetComments: {
                    method: 'POST',
                    params: {
                        verb: 'GetComments'
                    }
                },
                SaveComment: {
                    method: 'POST',
                    params: {
                        verb: 'SaveComment'
                    }
                },
                GetNewestPosts: {
                    method: 'POST',
                    params: {
                        verb: 'GetNewestPosts'
                    }
                },
                GetTimeline: {
                    method: 'POST',
                    params: {
                        verb: 'GetTimeline'
                    }
                },
                GetHomeTimeline: {
                    method: 'POST',
                    params: {
                        verb: 'GetHomeTimeline'
                    }
                },
                VoteComment: {
                    method: 'POST',
                    params: {
                        verb: 'VoteComment'
                    }
                },
                GetComment: {
                    method: 'POST',
                    params: {
                        verb: 'GetComment'
                    }
                },
                SaveReply: {
                    method: 'POST',
                    params: {
                        verb: 'SaveReply'
                    }
                },
                PostArticleToFacebook: {
                    method: 'POST',
                    params: {
                        verb: 'PostArticleToFacebook'
                    }
                },
                PostTrollToFacebook: {
                    method: 'POST',
                    params: {
                        verb: 'PostTrollToFacebook'
                    }
                },
				Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('AdService', [
        '$resource', function ($resource) {
            return $resource('/Ad/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetAll: {
                    method: 'POST',
                    params: {
                        verb: 'GetAll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                Delete: {
                    method: 'POST',
                    params: {
                        verb: 'Delete'
                    }
                }
            });
        }
    ])

    .factory('TrollService', [
        '$resource', function ($resource) {
            return $resource('/Troll/:verb', {}, {
                GetAllForView: {
                    method: 'POST',
                    params: {
                        verb: 'GetAllForView'
                    }
                },
                GetNewestTrolls: {
                    method: 'POST',
                    params: {
                        verb: 'GetNewestTrolls'
                    }
                },
                GetSingleTroll: {
                    method: 'POST',
                    params: {
                        verb: 'GetSingleTroll'
                    }
                },
                Save: {
                    method: 'POST',
                    params: {
                        verb: 'Save'
                    }
                },
                SaveComment: {
                    method: 'POST',
                    params: {
                        verb: 'SaveComment'
                    }
                },
                LikeTroll: {
                    method: 'POST',
                    params: {
                        verb: 'LikeTroll'
                    }
                },
                GetComments: {
                    method: 'POST',
                    params: {
                        verb: 'GetComments'
                    }
                },
                GetTopTrolls: {
                    method: 'POST',
                    params: {
                        verb: 'GetTopTrolls'
                    }
                },
                VoteComment: {
                    method: 'POST',
                    params: {
                        verb: 'VoteComment'
                    }
                },
                GetComment: {
                    method: 'POST',
                    params: {
                        verb: 'GetComment'
                    }
                },
                SaveReply: {
                    method: 'POST',
                    params: {
                        verb: 'SaveReply'
                    }
                }
            });
        }
    ])

    .factory('FootballApiService', [
        '$resource', function ($resource) {
            return $resource('/FootballApi/:verb', {}, {
                GetCompetitions: {
                    method: 'POST',
                    params: {
                        verb: 'GetCompetitions'
                    }
                },
                GetTodayMatches: {
                    method: 'POST',
                    params: {
                        verb: 'GetTodayMatches'
                    }
                },
                GetMatchesByDate: {
                    method: 'POST',
                    params: {
                        verb: 'GetMatchesByDate'
                    }
                },
                GetStandings: {
                    method: 'POST',
                    params: {
                        verb: 'GetStandings'
                    }
                }
            });
        }
    ])

    .factory('MatchService', [
        '$resource', function ($resource) {
            return $resource('/Match/:verb', {}, {
                SaveComment: {
                    method: 'POST',
                    params: {
                        verb: 'SaveComment'
                    }
                },
                GetComments: {
                    method: 'POST',
                    params: {
                        verb: 'GetComments'
                    }
                },
                GetMatchesThisWeek: {
                    method: 'POST',
                    params: {
                        verb: 'GetMatchesThisWeek'
                    }
                },
                GetMatchesByDate: {
                    method: 'POST',
                    params: {
                        verb: 'GetMatchesByDate'
                    }
                },
                GetLastTeamStanding: {
                    method: 'POST',
                    params: {
                        verb: 'GetLastTeamStanding'
                    }
                },
                DeleteComment: {
                    method: 'POST',
                    params: {
                        verb: 'DeleteComment'
                    }
                },
                GetMatchEvents: {
                    method: 'POST',
                    params: {
                        verb: 'GetMatchEvents'
                    }
                }
            });
        }
    ])

;

