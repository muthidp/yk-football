﻿angular.module('YK')

    .service('SessionService', [function () {
        var session = null;
        var login = false;
        return {
            get: function () {
                return session;
            },
            set: function (data) {
                session = data;
            },
            isLogin: function () {
                return session != null;
            },
            setIsLogin: function (data) {
                login = data;
            }
        };
    }])

    .service('NewsRouteService', [function () {
        var query = null;
        return {
            get: function () {
                return query;
            },
            set: function (data) {
                query = data;
            }
        };
    }])

    .service('TrollRouteService', [function () {
        var query = null;
        return {
            get: function () {
                return query;
            },
            set: function (data) {
                query = data;
            }
        };
    }])

    .service('DateService', [function () {
        return {
            formatDDMMMYYYY: function (date) {
                return moment(date).format('DD MMM YYYY');
            },
            formatCalendar: function (date) {
                return moment(date).calendar();
            }
        };
    }])

;
