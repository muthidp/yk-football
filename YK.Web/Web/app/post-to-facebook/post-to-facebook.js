angular.module('YK').controller('PostToFacebookCtrl', function ($scope, $uibModalInstance, data, link, type, PostService, SessionService) {

    $scope.user = SessionService.get();
    if($scope.user.Provider!='facebook'){
        toastr.warning('Please login using your facebook account');
        $uibModalInstance.close();
        return;
    };

    $scope.filter = {
        message: '',
        link: link,
        type: type
    };

    $scope.onPost = function () {
        var json;
        var callback = function(){
            if(json.data.success){
                toastr.success('Successfully posted to facebook');
                $uibModalInstance.close();
            }
        };
        if($scope.filter.type=='troll'){
            json = PostService.PostTrollToFacebook({
                troll: data,
                message: $scope.filter.message,
                link: $scope.filter.link
            },callback);
        } else {
            json = PostService.PostArticleToFacebook({
                post: data,
                message: $scope.filter.message,
                link: $scope.filter.link
            },callback);
        }
    };


});
