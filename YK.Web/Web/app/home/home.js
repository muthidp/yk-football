angular.module('YK').controller('HomeCtrl', function ($scope, $rootScope, $location, PostService, DateService, LeagueService, NewsRouteService) {

    $rootScope.waitInitialized(function () {
        $scope.myInterval = 3000;
        $scope.noWrapSlides = false;
        $scope.active = 0;

        $scope.newestPosts = [];
        var getNewestPosts = function () {
            var json = PostService.GetNewestPosts({
                limit: 3
            }, function () {
                var data = json.data;
                $scope.newestPosts = data;
            });
        };
        getNewestPosts();

        $scope.articles = [];
        $scope.articles = [1,2,3,4,5,6,7,8,9];

        $scope.threeArticles = [1,2,3];

        $scope.goToArticle = function (postId) {
            $location.path('postDetail/' + postId);
        };

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 12,
            SortBy: 'CreatedTime',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['CreatedTime']
            }
        };

        $scope.timeline = [];
        $scope.sectionOne = [];
        $scope.sectionTwo = [];
        $scope.sectionThree = [];
        var getTimeline = function () {
            $rootScope.showLoading();
            var json = PostService.GetHomeTimeline(function () {
                $scope.timeline = json.data;
                $rootScope.hideLoading();
            });
        };
        getTimeline();

        $scope.dateService = DateService;
		
		$scope.goToNewsTimeline = function(CountryId){
			var query = {
				categoryId: 0,
				leagueId: 0,
				teamId: 0,
				countryId: CountryId
			};		

			console.log("QQ ", query);
			
			NewsRouteService.set(query);
			var currentPath = $location.path();
			if(currentPath=='/blogTimeline'){
				$route.reload();
			} else {
				$location.path('blogTimeline');
			} 
         };
			
    });

});
