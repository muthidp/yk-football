angular.module('YK').controller('PostCtrl', function ($scope, $rootScope, $linq, $location, $routeParams, CategoryService, LeagueService, TagService, TeamService, PostService, SessionService) {
    $rootScope.waitInitialized(function () {

        var user = SessionService.get();
        console.log("USER HERE ", user);

        var postId = $routeParams.postId;

        $scope.data = {
            Title: '',
            Body: '',
            CategoryId: null,
            LeagueId: null,
            TeamId: null,
            Tags: [],
            PictureUrl: '',
            UserId: user.UserId
        };

        var getPost = function () {
            var json = PostService.GetSinglePost({
                postId: postId
            }, function () {
                $scope.data = json.data;
            });
        };

        if (postId > 0) {
            getPost();
        }

        $scope.disabled = false;
        $scope.menu = [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['format-block'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'quote', 'paragraph'],
            ['link', 'image'],
            ['css-class']
        ];

        $scope.cssClasses = ['test1', 'test2'];

        $scope.setDisabled = function () {
            $scope.disabled = !$scope.disabled;
        };

        $scope.pictureFile = {};
        $scope.onPickPicture = function (file) {
            $scope.data.PictureUrl = file.RelativePath;
        };

        $scope.categories = [];
        var jsonCat = CategoryService.GetAll(function () {
            var data = jsonCat.data;
            $scope.categories = data;
        });

        $scope.leagues = [];
        var jsonL = LeagueService.GetAll(function () {
            var data = jsonL.data;
            $scope.leagues = data;
        });

        $scope.teams = [];
        var jsonTeams = TeamService.GetAll(function () {
            var data = jsonTeams.data;
            $scope.teams = data;
        });

        $scope.tagText = '';
        $scope.onAddTag = function (tag) {
            if ($scope.tagText != null && $scope.tagText != '') {
                var any = $linq.Enumerable().From($scope.data.Tags)
                    .Where(function (x) {
                        return x == tag
                    })
                    .Any();

                if (!any) {
                    $scope.data.Tags.push(angular.copy(tag));
                }
            }
            $scope.tagText = '';
        };

        $scope.onDeleteTag = function (index) {
            $scope.data.Tags.splice(index, 1);
        };

        $scope.onSavePost = function () {
            var json = PostService.Save({
                dataModel: $scope.data
            }, function () {
                var data = json.data;
                if (data.success) {
                    toastr.success('Saved succesfully.');
                    var post = data.datas.post;
                    $location.path('/postDetail/' + post.PostId);
                } else {
                    if (json.data.validations != null) {
                        $scope.validations = json.data.validations;
                    }
                }
            })
        };
    });
});
