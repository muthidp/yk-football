angular.module('YK').controller('LeagueTimelineCtrl', function ($scope, $rootScope, $location, PostService, NewsRouteService, LeagueService, DateService) {

    $rootScope.waitInitialized(function () {

        var query = NewsRouteService.get();
        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 12,
            SortBy: 'CreatedTime',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['CreatedTime']
            }
        };

        $scope.getPostPerSection = function(index){
            var postPerSection = 4;
            var a = $scope.posts.slice((index*postPerSection), postPerSection);
            return a;
        };

        $scope.posts = [];
        $scope.postSectionOne = [];
        $scope.postSectionTwo = [];
        $scope.postSectionThree = [];
        $scope.Fetch = function () {
            var json = PostService.GetTimeline({
                leagueId: query.leagueId,
                categoryId: query.categoryId,				
                teamId: query.teamId,
				countryId: query.countryId,
                queryModel: $scope.SearchQuery
            }, function () {
                var data = json.data;
                $scope.posts = data.list;

                $scope.postSectionOne = $scope.getPostPerSection(0);
                $scope.postSectionTwo = $scope.getPostPerSection(1);
                $scope.postSectionThree = $scope.getPostPerSection(2);

                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        };
        if (query != null) {
            $scope.Fetch();

            if (query.leagueId > 0) {
                var json = LeagueService.GetOne({
                    leagueId: query.leagueId
                }, function () {
                    $scope.league = json.data;
                });
            }
        };

        $scope.getPreviewArticleContent = function (content) {
            var text = $(content).text();
            if (text != null)
                return text.substring(0, 300) + "...";
            return null;
        };

        $scope.dateService = DateService;

        $scope.goToArticle = function (postId) {
            $location.path('postDetail/' + postId);
        };

        $scope.onPageChanged = function () {
            $scope.Fetch();
        };

    });

});
