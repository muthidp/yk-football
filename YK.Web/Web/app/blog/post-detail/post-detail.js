angular.module('YK').controller('PostDetailCtrl', function ($scope, $rootScope, $routeParams, $location, $uibModal, SessionService, PostService) {

    $rootScope.waitInitialized(function () {
        $scope.user = SessionService.get();
		
		$scope.isAdmin = false;
		if($scope.user.Role.RoleCode == 'ROOT')
			$scope.isAdmin = true;
		
        var postId = $routeParams.postId;

        $scope.post = {};
        var getPost = function (cb) {
            var json = PostService.GetSinglePost({
                postId: postId
            }, function () {
                $scope.post = json.data;
                cb();
            });
        };

        var getComments = function () {
            var json = PostService.GetComments({
                postId: postId
            }, function () {
                $scope.post.Comments = json.data;
            });
        };
        getPost(function () {
            getComments()
        });

        $scope.filter = {
            comment: '',
            reply: ''
        };
        $scope.onSaveComment = function () {
            if ($scope.filter.comment != null && $scope.filter.comment != '') {
                var dataModel = {
                    PostId: postId,
                    UserId: $scope.user.UserId,
                    Content: angular.copy($scope.filter.comment)
                };
                var json = PostService.SaveComment({
                    dataModel: dataModel
                }, function () {
                    if (json.data.success) {
                        toastr.success('Comment added.');
                        getComments();
                        $scope.filter.comment = '';
                    }
                });
            }
        };

        $scope.showDateTime = function (date) {
            return moment(date).calendar();
        };

        $scope.showDateTimeNewestPost = function (date) {
            return moment(date).format('DD MMM YYYY');
        };

        $scope.newestPosts = [];
        var getNewestPosts = function () {
            var json = PostService.GetNewestPosts({
                excludePostId: postId
            }, function () {
                $scope.newestPosts = json.data;
            });
        };
        getNewestPosts();

        $scope.onVote = function (index, commentId, isUpVote) {

            if($scope.user==null){
                toastr.warning('Please loginn first.');
                return;
            }

            var json = PostService.VoteComment({
                commentId: commentId,
                userId: $scope.user.UserId,
                isUpVote: isUpVote
            }, function () {
                var data = json.data;
                if (data.success) {
                    var updatedComment = data.datas.comment;
                    $scope.post.Comments[index] = updatedComment;
                }
            });
        };

        $scope.onReplyClicked = function(index, value){
            angular.forEach($scope.post.Comments, function(c,i){
                c.isReplyClicked = false;
            });
            $scope.post.Comments[index].isReplyClicked = value;
        };

        $scope.onSaveReply = function(comment){
            if ($scope.filter.reply != null && $scope.filter.reply != '') {
                var dataModel = {
                    CommentId: comment.CommentId,
                    UserId: $scope.user.UserId,
                    Content: angular.copy($scope.filter.reply)
                };
                var json = PostService.SaveReply({
                    model: dataModel
                }, function () {
                    var data = json.data;
                    if (data.success) {
                        toastr.success('Reply added.');
                        comment.Replies = data.datas.replies;
                        $scope.filter.reply = '';
                    }
                });
            }
        };

        $rootScope.$on('sign-in', function(){
            $scope.user = SessionService.get();
        });

        var postFbModal = function (post, link) {
            $uibModal.open({
                templateUrl: APP_URL + 'post-to-facebook/post-to-facebook.html',
                controller: 'PostToFacebookCtrl',
                animation: true,
                keyboard: false,
                backdrop: 'static',
                resolve: {
                    data: function () {
                        return angular.copy(post);
                    },
                    link: function(){
                        return angular.copy(link);
                    },
                    type: function(){
                        return 'post'
                    }
                }
            }).result.then(function () {

                });
        };

        $scope.postToFacebook = function (post) {
            var link = $location.absUrl();
            postFbModal(post, link);
        };
		
		$scope.edit = function () {
             window.location = "/Web/index.html#/post/" + postId ;
        };
		
		$scope.delete = function () {
            var sel = confirm("Are you sure you want to delete this post?");
            if (sel) {
                var json = PostService.Delete({
                    postId: postId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        window.location = "/Web/index.html#/home" ;
                    }
                });
            }
        };
    });

});
