angular.module('YK').controller('CategoryCtrl',function($scope, $rootScope, $uibModal, CategoryService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'CategoryCode',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['CategoryCode', 'CategoryLabel']
            }
        };

        $scope.categories = [];
        $scope.Fetch = function () {
            var json = CategoryService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.categories = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        };
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/category/category-form/category-form.html',
                controller: 'CategoryFormCtrl',
                animation: true,
                resolve: {
                    category: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (category) {
            openModal(angular.copy(category));
        };
        $scope.onRowDelete = function (category) {
            var sel = confirm("Are you sure you want to delete category [" + category.CategoryLabel + "] ?");
            if (sel) {
                var json = CategoryService.Delete({
                    categoryId: category.CategoryId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };
    });

});
