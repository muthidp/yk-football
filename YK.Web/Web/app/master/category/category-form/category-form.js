angular.module('YK').controller('CategoryFormCtrl', function ($scope, $uibModalInstance, category, CategoryService) {

    $scope.category = category;
    if ($scope.category == null) {
        $scope.category = {};
    }
    $scope.onSave = function () {
        var json = CategoryService.Save({
            category: $scope.category
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };

    $scope.fileResult = {};
    $scope.onPick = function(fileResult) {
        console.log("FILE RESULT ", fileResult);
        $scope.category.PictureUrl = fileResult.RelativePath;
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
