angular.module('YK').controller('AdCtrl',function($scope, $rootScope, $uibModal, AdService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'AdIdentifier',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['AdIdentifier', 'LocationPage']
            }
        };

        $scope.ads = [];
        $scope.Fetch = function () {
            var json = AdService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.ads = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        }
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/Ad/ad-form/ad-form.html',
                controller: 'AdFormCtrl',
                animation: true,
                size: 'lg',
                resolve: {
                    ad: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (Ad) {
            openModal(angular.copy(Ad));
        };
        $scope.onRowDelete = function (Ad) {
            var sel = confirm("Are you sure you want to delete Ad [" + Ad.AdIdentifier + "] ?");
            if (sel) {
                var json = AdService.Delete({
                    AdId: Ad.AdId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };
    });

});
