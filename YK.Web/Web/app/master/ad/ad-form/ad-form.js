angular.module('YK').controller('AdFormCtrl', function ($scope, $uibModalInstance, ad, AdService, LeagueService) {

    $scope.ad = ad;
    if ($scope.ad == null) {
        $scope.ad = {};
    }
    $scope.onSave = function () {
        var json = AdService.Save({
            Ad: $scope.ad
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };
	
    $scope.fileResult = {};
    $scope.onPick = function(fileResult) {
        //console.log("FILE RESULT ", fileResult);
        $scope.ad.ImageUrl = fileResult.RelativePath;
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
