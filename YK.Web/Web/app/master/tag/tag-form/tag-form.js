angular.module('YK').controller('TagFormCtrl', function ($scope, $uibModalInstance, tag, TagService) {
    console.log("Tag edit ::", tag)
    $scope.tag= tag;
    if ($scope.tag == null) {
        $scope.tag= {};
    }
    $scope.onSave = function () {
        var json = TagService.Save({
            Tag: $scope.tag
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
