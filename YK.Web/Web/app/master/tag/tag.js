angular.module('YK').controller('TagCtrl',function($scope, $rootScope, $uibModal, TagService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'TagLabel',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['TagLabel']
            }
        };

        $scope.tags = [];
        $scope.Fetch = function () {
            var json = TagService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.tags = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        }
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/tag/tag-form/tag-form.html',
                controller: 'TagFormCtrl',
                animation: true,
                resolve: {
                    tag: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (Tag) {
            openModal(angular.copy(Tag));
        };
        $scope.onRowDelete = function (Tag) {
            var sel = confirm("Are you sure you want to delete Tag [" + Tag.TagLabel + "] ?");
            if (sel) {
                var json = TagService.Delete({
                    TagId: Tag.TagId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };
    });

});
