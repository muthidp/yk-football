angular.module('YK').controller('LeagueFormCtrl', function ($scope, $uibModalInstance, league, LeagueService, CountryService) {

    $scope.league = league;
    if ($scope.league == null) {
        $scope.league = {};
    }
    $scope.onSave = function () {
        var json = LeagueService.Save({
            League: $scope.league
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };

    $scope.countries = [];
    var getCountries = function () {
        var json = CountryService.GetAll(function () {
            $scope.countries = json.data;
        });
    };
    getCountries();

    $scope.fileResult = {};
    $scope.onPick = function (fileResult) {
        console.log("FILE RESULT ", fileResult);
        $scope.league.LogoUrl = fileResult.RelativePath;
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
