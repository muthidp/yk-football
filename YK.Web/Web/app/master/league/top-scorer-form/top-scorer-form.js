angular.module('YK').controller('TopScorerFormCtrl', function ($scope, $uibModalInstance, league, LeagueService, CountryService) {

    $scope.onSave = function () {
        var json = LeagueService.Save({
            League: $scope.league
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };
	
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
