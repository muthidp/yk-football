angular.module('YK').controller('LeagueCtrl',function($scope, $rootScope, $uibModal, LeagueService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'LeagueCode',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['LeagueCode', 'LeagueLabel', 'Description']
            }
        };

        $scope.leagues = [];
        $scope.Fetch = function () {
            var json = LeagueService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.leagues = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        }
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/League/League-form/League-form.html',
                controller: 'LeagueFormCtrl',
                animation: true,
                size: 'lg',
                resolve: {
                    league: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (League) {
            openModal(angular.copy(League));
        };
        $scope.onRowDelete = function (League) {
            var sel = confirm("Are you sure you want to delete League [" + League.LeagueLabel + "] ?");
            if (sel) {
                var json = LeagueService.Delete({
                    LeagueId: League.LeagueId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };		
		
		var openTopScorerModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/league/top-scorer-form/top-scorer-form.html',
                controller: 'TopScorerFormCtrl',
                animation: true,
                size: 'lg',
                resolve: {
                    league: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onEditTopScorer = function (league) {
            openTopScorerModal(league);
        };		
		
    });

});
