angular.module('YK').controller('CountryFormCtrl',function($scope, $uibModalInstance, country, CountryService){
    $scope.country = country;
    if ($scope.country == null) {
        $scope.country = {};
    }
    $scope.onSave = function () {
        var json = CountryService.Save({
            country: $scope.country
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };

    $scope.fileResult = {};
    $scope.onPick = function(fileResult) {
        $scope.country.LogoUrl = fileResult.RelativePath;
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };

});
