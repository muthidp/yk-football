angular.module('YK').controller('CountryCtrl',function($scope, $rootScope, $uibModal, CountryService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'CountryCode',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['CountryCode', 'CountryLabel']
            }
        };

        $scope.countries = [];
        $scope.Fetch = function () {
            var json = CountryService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.countries = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        };
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/country/country-form/country-form.html',
                controller: 'CountryFormCtrl',
                animation: true,
                resolve: {
                    country: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (country) {
            openModal(angular.copy(country));
        };
        $scope.onRowDelete = function (country) {
            var sel = confirm("Are you sure you want to delete category [" + country.CountryLabel + "] ?");
            if (sel) {
                var json = CountryService.Delete({
                    countryId: country.CountryId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };
    });

});
