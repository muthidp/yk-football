angular.module('YK').controller('TeamCtrl',function($scope, $rootScope, $uibModal, TeamService){
    $rootScope.waitInitialized(function () {

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 10,
            SortBy: 'TeamCode',
            IsSortAsc: true,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['TeamCode', 'TeamLabel', 'Description']
            }
        };

        $scope.teams = [];
        $scope.Fetch = function () {
            var json = TeamService.GetAllForView({
                queryModel: $scope.SearchQuery
            },function () {
                var data = json.data;
                $scope.teams = data.list;
                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;
            });
        }
        $scope.Fetch();

        var openModal = function (data) {
            $uibModal.open({
                templateUrl: APP_URL + 'master/team/team-form/team-form.html',
                controller: 'TeamFormCtrl',
                animation: true,
                size: 'lg',
                resolve: {
                    team: function () {
                        return data;
                    }
                }
            }).result.then(function () {
                    $scope.Fetch();
                });
        };
        $scope.onCreateNew = function () {
            openModal(null);
        };
        $scope.onRowEdit = function (Team) {
            openModal(angular.copy(Team));
        };
        $scope.onRowDelete = function (Team) {
            var sel = confirm("Are you sure you want to delete Team [" + Team.TeamLabel + "] ?");
            if (sel) {
                var json = TeamService.Delete({
                    TeamId: Team.TeamId
                }, function () {
                    if (json.data.success) {
                        toastr.success('Deleted successfully.');
                        $scope.Fetch();
                    }
                });
            }
        };
        $scope.onSearch = function () {
            $scope.Fetch();
        };
        $scope.onPageChanged = function () {
            //$scope.SearchQuery.Page = page;
            $scope.Fetch();
        };
        $scope.onSort = function (sortField) {
            if ($scope.SearchQuery.SortBy == sortField)
                $scope.SearchQuery.IsSortAsc = !$scope.SearchQuery.IsSortAsc;
            else
                $scope.SearchQuery.IsSortAsc = true;
            $scope.SearchQuery.SortBy = sortField;
            $scope.Fetch();
        };
    });

});
