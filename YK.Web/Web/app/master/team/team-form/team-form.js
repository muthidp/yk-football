angular.module('YK').controller('TeamFormCtrl', function ($scope, $uibModalInstance, team, TeamService, LeagueService) {

    $scope.team = team;
    if ($scope.team == null) {
        $scope.team = {};
    }
    $scope.onSave = function () {
        var json = TeamService.Save({
            Team: $scope.team
        }, function () {
            if (json.data.success) {
                toastr.success('Saved succesfully.');
                $uibModalInstance.close();
            } else {
                if (json.data.validations != null) {
                    $scope.validations = json.data.validations;
                }
            }
        });
    };

    $scope.leagues = [];
    var json = LeagueService.GetAll({
        queryModel: $scope.SearchQuery
    }, function () {
        $scope.leagues = json.data;
        console.log("Leagues ::", $scope.leagues);
    });

    $scope.fileResult = {};
    $scope.onPick = function(fileResult) {
        console.log("FILE RESULT ", fileResult);
        $scope.team.LogoUrl = fileResult.RelativePath;
    };
    $scope.onCancel = function () {
        $uibModalInstance.close();
    };
});
