angular.module('YK').controller('NavigationCtrl'
    , function ($rootScope, $scope, $timeout, $location, $q, $linq, $uibModal, $route, SessionService, AuthenticationService, ModuleGroupService,
                LeagueService, CategoryService, TeamService, NewsRouteService) {

        $rootScope.waitInitialized(function () {
            $scope.moduleGroups = [];
            var fetchModuleGroups = function () {
                $scope.moduleGroups = [];
                var jsonMg = ModuleGroupService.GetAllModuleGroups(function () {
                    var data = jsonMg.data;
                    if(data!=null){
                        angular.forEach(data, function(v,i){
                            if($scope.hasAcl(v.AclKey)){
                                $scope.moduleGroups.push(v);
                            }
                        });
                    }
                });
            };
            fetchModuleGroups();

            $scope.user = SessionService.get();

            $scope.navigatePage = function (route) {
                if (route != null && route != "")
                    $location.path(route);
            };

            $scope.hasAcl = function (aclKey) {
                if (aclKey.IsNullOrEmpty()) return true;
                var result = false;
                if ($scope.user != null && $scope.user.Acls != null) {
                    angular.forEach($scope.user.Acls, function (acl, i) {
                        if (aclKey == acl) result = true;
                    });
                }
                return result;
            };

            $scope.onLogout = function () {
                var sel = confirm('Are you sure you want to logout?');
                if (sel) {
                    var json = AuthenticationService.Logout(function () {
                        var data = json.data;
                        if (data) {
                            toastr.success("Logout successfully");
                            SessionService.set(null);
                            $rootScope.$broadcast('sign-out');
                            $scope.user = SessionService.get();
                            fetchModuleGroups();
                            $location.path('home');
                        }
                    });
                }
            };

            var signInModal = function (data) {
                $uibModal.open({
                    templateUrl: APP_URL + 'internal/sign-in/sign-in.html',
                    controller: 'SignInCtrl',
                    animation: true,
                    resolve: {
                        category: function () {
                            return data;
                        }
                    }
                }).result.then(function () {
                        $scope.user = SessionService.get();
                        fetchModuleGroups();
                        $rootScope.$broadcast('sign in');
                    });
            };

            $scope.onClickSignIn = function () {
                signInModal(null);
            };

            var signUpModal = function (data) {
                $uibModal.open({
                    templateUrl: APP_URL + 'internal/sign-up/sign-up.html',
                    controller: 'SignUpCtrl',
                    animation: true,
                    resolve: {
                        category: function () {
                            return data;
                        }
                    }
                }).result.then(function () {
                        $scope.onClickSignIn();
                    });
            };

            $scope.onClickJoin = function () {
                signUpModal(null);
            };

            $scope.tabs = [
                {title: 'Dynamic Title 1', content: 'Dynamic content 1'},
                {title: 'Dynamic Title 2', content: 'Dynamic content 2', disabled: true}
            ];


            var fetchNewsData = function () {
                var jsonLeague = LeagueService.GetAll();
                //var jsonCategory = CategoryService.GetAll();
				var jsonCategory = CategoryService.GetAllForMenu();
                var jsonTeam = TeamService.GetAll();

                $q.all([jsonLeague.$promise, jsonCategory.$promise, jsonTeam.$promise])['finally'](function () {
                    $scope.leagues = jsonLeague.data;
                    $scope.categories = jsonCategory.data;
                    $scope.teams = jsonTeam.data;
                });
            };
            fetchNewsData();

            $scope.selectedCategory = null;
            $scope.onSelectCategory = function(cat){
                $scope.selectedCategory = cat;
            };

            $scope.goToNewsTimeline = function(CategoryId, teamId){
                var query = {
                    categoryId: CategoryId != null ? CategoryId : null,
                    leagueId: 0,
                    teamId: teamId != null ? teamId : null,
					countryId: 0
                };
                NewsRouteService.set(query);
				
                //var currentPath = $location.path();
                //if(currentPath=='/leagueTimeline'){
                    //$route.reload();
                //} else {
                    //$location.path('leagueTimeline');
                //} 
				
				var currentPath = $location.path();
                if(currentPath=='/blogTimeline'){
                    $route.reload();
                } else {
                    $location.path('blogTimeline');
                } 
            };

            $scope.onAddNewPost = function(){
                $location.path('post/0');
            };

            $scope.$on('signin', function(){
                $root.onClickSignIn();
            });

        });
    });
