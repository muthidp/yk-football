angular.module('YK').controller('SignUpCtrl', function ($scope, $uibModalInstance, UserService) {

    $scope.user = {
        UserUniqueId: '',
        Username: '',
        Password: '',
        Email: '',
        Provider: 'local'
    };

    $scope.validations = {};
    $scope.onRegister = function () {
        var json = UserService.Save({
            dataModel: $scope.user
        }, function () {
            if (json.data.success) {
                toastr.success("Successfully registered. Please login now.");
                $uibModalInstance.close();
            } else {
                if(json.data.validations!=null){
                    $scope.validations = json.data.validations;
                }
            };
        });
    }

});
