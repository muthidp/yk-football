angular.module('YK').controller('SignInCtrl', function ($scope, $rootScope, $uibModalInstance, SessionService, AuthenticationService, UserService) {

    $scope.user = {
        userId: null,
        password: null
    };

    $scope.onLogin = function () {
        var jsonLogin = AuthenticationService.Login({
            userId: $scope.user.userId,
            password: $scope.user.password
        }, function () {
            var data = jsonLogin.data;
            if (data.success) {
                var user = data.datas.user;
                var json = UserService.GetUserInfo({
                    username: user.Username
                }, function () {
                    var userInfo = json.data;
                    SessionService.set(userInfo);
                    SessionService.setIsLogin(true);
                    toastr.success("Login success");
                    broadcastSignUp();
                    $uibModalInstance.close();
                });
            } else {
                if (data.validations != null) {
                    angular.forEach(data.validations, function (v, i) {
                        toastr.error(v);
                    });
                }
                SessionService.setIsLogin(false);
            }
        });
    };

    var broadcastSignUp = function () {
        $rootScope.$broadcast('sign-in');
    };

    $scope.providers = [];
    var getExternalLoginList = function () {
        var json = AuthenticationService.ExternalLoginList(function () {
            $scope.providers = json.data;
        });
    };
    getExternalLoginList();

    $scope.onExternalLogin = function () {
        //var json = AuthenticationService.ExternalLogin({
        //    provider: 'facebook'
        //}, function(){
        //    console.log("BRO ", json);
        //});

        window.$windowScope = $scope;
        window.location.assign('/auth/externalLogin?provider=facebook')
        //window.location('/auth/externalLogin?provider=facebook');
        //var oauthWindow = window.open('/auth/externalLogin?provider=facebook', "Authenticate Account", "location=0,status=0,width=600,height=750");

    };

});
