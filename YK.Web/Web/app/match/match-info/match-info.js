angular.module('YK').controller('MatchInfoCtrl', function ($scope, $uibModal, match, league, SessionService, MatchService) {

    $scope.match = match;
    $scope.league = league;
    $scope.user = SessionService.get();

    $scope.listComments = [];
    $scope.filter = {
        comment: ''
    };

    $scope.otherMatches = [];
    var getOtherMatches = function () {

        var others = [];
        angular.forEach($scope.league.Matches, function (v, i) {
            if (others.length < 3) {
                if (v.MatchId != $scope.match.MatchId) {
                    others.push(v);
                }
            }
        });
        $scope.otherMatches = others;
    };
    getOtherMatches();

    var getComments = function () {
        var json = MatchService.GetComments({
            footballMatchId: $scope.match.FootballMatchId
        }, function () {
            $scope.listComments = json.data;
        })
    };
    getComments();

    $scope.hasAcl = function (aclKey) {
        if (aclKey.IsNullOrEmpty()) return true;
        var result = false;
        if ($scope.user != null && $scope.user.Acls != null) {
            angular.forEach($scope.user.Acls, function (acl, i) {
                if (aclKey == acl) result = true;
            });
        }
        return result;
    };
    $scope.hasAddMatchCommentAcl = $scope.hasAcl('ADD_MATCH_COMMENT');

    $scope.onAddComment = function () {
        $scope.listComments.push({
            Comment: angular.copy($scope.filter.comment),
            MatchCommentId: null
        });
        var json = MatchService.SaveComment({
            matchModel: $scope.match,
            comments: $scope.listComments
        }, function () {
            var jsonData = json.data;
            if (jsonData.success) {
                $scope.match = jsonData.datas.match;
                $scope.listComments = jsonData.datas.comments;

                $scope.filter.comment = '';

                toastr.success('Comment added.');
            }
        });
    };

    $scope.onDeleteComment = function (matchId, matchCommentId) {
        var sel = confirm('Are you sure you want to delete this comment?');
        if (sel) {
            var json = MatchService.DeleteComment({
                matchId: $scope.match.FootballMatchId,
                matchCommentId: matchCommentId
            }, function () {
                if (json.data.success) {
                    //getComments();
                    $scope.listComments = json.data.datas.comments;
                    toastr.success('Comment deleted.');
                }
            })
        }
    }

    $scope.getStatus = function (status) {
        if (status != 'FT' && status != 'HT' && status != 'ET') {
            return 'vs';
        }
        return status;
    };

    var getLastTeamStanding = function (teamId, teamType) {
        var json = MatchService.GetLastTeamStanding({
            teamId: teamId
        }, function () {
            if (teamType == 'local') {
                $scope.localTeamStanding = json.data;
            } else {
                $scope.visitorTeamStanding = json.data;
            }
        })
    };

    var getLastStandingForBothTeam = function () {
        if ($scope.match != null) {
            $scope.localTeamStanding = [];
            $scope.visitorTeamStanding = [];
            getLastTeamStanding($scope.match.FootballlLocalTeamId, 'local');
            getLastTeamStanding($scope.match.FootballlLocalVisitorName, 'visitor')
        }
    };
    getLastStandingForBothTeam();

    $scope.events = [];
    var getMatchEvents = function () {
        var json = MatchService.GetMatchEvents({
            matchId: $scope.match.MatchId
        }, function () {
            $scope.events = json.data;
        })
    };
    getMatchEvents();
})
;
