angular.module('YK').controller('MatchCtrl', function ($scope, $rootScope, $uibModal, $timeout, $interval, SessionService, LeagueService, PostService, FootballApiService, MatchService, $q) {

    $rootScope.waitInitialized(function () {
        // LIST OF COMPETITIONS, GET THIS FROM FOOTBALL API

        $scope.listCompetitions = [];

        $scope.filter = {
            date: new Date(),
            dateOpened: false
        };

        $scope.openDate = function () {
            $scope.filter.dateOpened = true;
        };

        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        $scope.leagues = [1, 2, 3, 4];
        $scope.matches = [1, 2, 3, 4, 5, 6];

        $scope.user = SessionService.get();

        $scope.filter = {
            date: new Date()
        };

        $scope.addDate = function () {
            $scope.filter.date = moment($scope.filter.date).add(1, 'days');
            getMatchesThisWeek();
        };
        $scope.minusDate = function () {
            $scope.filter.date = moment($scope.filter.date).add(-1, 'days');
            getMatchesThisWeek();
        };

        $scope.displayDate = function (date) {
            return moment(date).format('DD/MM/YYYY');
        };

        $scope.newestPosts = [];
        var getNewestPosts = function () {
            var json = PostService.GetNewestPosts(function () {
                $scope.newestPosts = json.data;
            });
        };
        getNewestPosts();

        //getCompetitions(function(){
        //    getMatches();
        //});

        var openModal = function (match, league) {
            $uibModal.open({
                templateUrl: APP_URL + 'match/match-info/match-info.html',
                controller: 'MatchInfoCtrl',
                animation: true,
                size: 'lg',
                resolve: {
                    match: function () {
                        return angular.copy(match)
                    },
                    league: function () {
                        return angular.copy(league)
                    }
                }
            }).result.then(function () {
                });
        };
        $scope.onOpenMatchInfo = function (match, league) {
            openModal(match, league);
        };

        var getMatchesThisWeek = function () {
            var json = MatchService.GetMatchesByDate({
                    date: moment($scope.filter.date).format('MM/DD/YYYY')
                },
                function () {
                    $scope.listCompetitions = json.data;
                })
        };
        getMatchesThisWeek();

        $scope.getDate = function (date) {
            return moment(date).format('DD/MM/YYYY');
        };

        $scope.getTime = function (date) {
            return moment(date).format('hh:mm');
        };

        $scope.getStatus = function (status) {
            if (status != 'FT' && status != 'HT' && status != 'ET') {
                return 'vs';
            }
            return status;
        };

        var stop;
        $scope.startInterval = function () {
            // Don't start a new fight if we are already fighting
            if (angular.isDefined(stop)) return;
            stop = $interval(function () {
                getMatchesThisWeek();
            }, 60 * 1000);
        };

        $scope.stopInterval = function () {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
            }
        };
        $scope.startInterval();

        $scope.$on('$destroy', function () {
            // Make sure that the interval is destroyed too
            $scope.stopInterval();
        });
		
		$scope.getLeagues = function(){
			var jsonLeague = LeagueService.GetAll(function(){
                $scope.leagues = jsonLeague.data;
				
				console.log("Leagues ::", $scope.leagues);
            })
		};
		
		$scope.getLeagues();
    })

});
