angular.module('YK').controller('TrollTimelineCtrl', function ($scope, $rootScope, $uibModal, $location, SessionService, DateService, TrollService, TrollRouteService) {

    $rootScope.waitInitialized(function () {

        $scope.user = SessionService.get();

        $scope.trolls = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        $scope.SearchQuery = {
            Page: 1,
            RowPerPage: 12,
            SortBy: 'CreatedTime',
            IsSortAsc: false,
            TotalData: 0,
            MaxPage: 8,
            Search: {
                Keyword: '',
                Fields: ['CreatedTime']
            }
        };

        $scope.trolls = [];
        var fetch = function () {
            $rootScope.showLoading();
            var json = TrollService.GetAllForView({
                queryModel: $scope.SearchQuery
            }, function () {
                var data = json.data;
                $scope.trolls = data.list;

                $scope.SearchQuery.TotalData = data.totalData;
                $scope.SearchQuery.TotalPage = data.totalPage;

                $rootScope.hideLoading();
            });
        };
        fetch();

        $scope.dateService = DateService;

        var signInModal = function (cb) {
            $uibModal.open({
                templateUrl: APP_URL + 'internal/sign-in/sign-in.html',
                controller: 'SignInCtrl',
                animation: true,
                keyboard: false,
                backdrop: 'static',
            }).result.then(function () {
                    $scope.user = SessionService.get();
                    cb();
                });
        };

        $scope.onClickSignIn = function (cb) {
            signInModal(cb);
        };

        $scope.onLikeTroll = function (troll, index) {
            if ($scope.user == null) {
                $scope.onClickSignIn(function () {
                    $scope.onLikeTroll(troll, index);
                });
            } else {
                var json = TrollService.LikeTroll({
                    trollId: troll.TrollId,
                    userId: $scope.user.UserId
                }, function () {
                    var result = json.data;
                    if (result.success) {
                        var updatedTroll = result.datas.troll;
                        $scope.trolls[index] = updatedTroll;
                    }
                });
            }
        };

        $scope.isTrollLikedByMe = function (troll) {
            if ($scope.user == null) return false;

            var result = false;
            if (troll.UserLikes != null) {
                angular.forEach(troll.UserLikes, function (v, i) {
                    if (v == $scope.user.Username)
                        result = true;
                });
            }
            return result;
        };

        $scope.onOpenTroll = function (troll) {
            TrollRouteService.set(angular.copy(troll));
            //$location.path('singleTroll');
            $scope.goToTroll(troll.TrollId);
        };

        $scope.topTrolls = [];
        var getTopTrolls = function () {
            var json = TrollService.GetTopTrolls({
                limit: 5
            }, function () {
                $scope.topTrolls = json.data;
            });
        };
        getTopTrolls();

        var postFbModal = function (troll, link) {
            $uibModal.open({
                templateUrl: APP_URL + 'post-to-facebook/post-to-facebook.html',
                controller: 'PostToFacebookCtrl',
                animation: true,
                keyboard: false,
                backdrop: 'static',
                resolve: {
                    data: function () {
                        return angular.copy(troll);
                    },
                    link: function(){
                        return angular.copy(link);
                    },
                    type: function(){
                        return 'troll'
                    }
                }
            }).result.then(function () {

                });
        };

        $scope.postToFacebook = function (troll) {
            console.log('hahaha', $location.absUrl());
            var link = getBaseUrl()+'#/singleTroll/'+troll.TrollId;
            postFbModal(troll, link);
        };

        var getBaseUrl = function(){
            var url = $location.absUrl().split('?')[0];
            var baseUrl = url.split('#')[0];
            return baseUrl;
        };

        $scope.goToTroll = function (trollId) {
            $location.path('singleTroll/' + trollId);
        };

    });
});
