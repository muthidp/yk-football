angular.module('YK').controller('TrollCreationCtrl',function($scope, $rootScope, $location, $uibModal, $route, SessionService, TrollService){

    $rootScope.waitInitialized(function(){

        console.log("RUN");
        if(!$rootScope.isLogin){

            var signInModal = function (data) {
                $uibModal.open({
                    templateUrl: APP_URL + 'internal/sign-in/sign-in.html',
                    controller: 'SignInCtrl',
                    animation: true,
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        category: function () {
                            return data;
                        }
                    }
                }).result.then(function () {
                        $scope.user = SessionService.get();
                        $route.reload();
                    });
            };

            $scope.onClickSignIn = function () {
                signInModal(null);
            };
            $scope.onClickSignIn();
        };

        $scope.user = SessionService.get();

        $scope.data = {
            Title: '',
            ContentUrl: '',
            UserId: $scope.user!=null ? $scope.user.UserId : null
        };

        $scope.pictureFile = {};
        $scope.onPickPicture = function (file) {
            $scope.data.ContentUrl = file.RelativePath;
        };

        $scope.onSavePost = function () {
            var json = TrollService.Save({
                dataModel: $scope.data
            }, function () {
                var data = json.data;
                if (data.success) {
                    toastr.success('Saved succesfully.');
                    var troll = data.datas.troll;
                    $location.path('/trollTimeline');
                } else {
                    if (json.data.validations != null) {
                        $scope.validations = json.data.validations;
                    }
                }
            })
        };


    });


});
