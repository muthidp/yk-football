angular.module('YK').controller('SingleTrollCtrl', function ($scope, $rootScope, $uibModal, $routeParams, $location, TrollRouteService, SessionService, TrollService, DateService) {

    $rootScope.waitInitialized(function () {
        $scope.user = SessionService.get();

        var trollId = $routeParams.trollId;

        $scope.troll = TrollRouteService.get();

        $scope.filter = {
            comment: ''
        };

        var getTroll = function(){
            var json = TrollService.GetSingleTroll({
                trollId: trollId
            }, function(){
                $scope.troll = json.data;
            })
        };
        getTroll();

        var getComments = function(){
            var json = TrollService.GetComments({
                trollId: $scope.troll.TrollId
            }, function(){
                $scope.troll.Comments = json.data;
            })
        };

        $scope.onSaveComment = function(){
            var json = TrollService.SaveComment({
                TrollId: $scope.troll.TrollId,
                UserId: $scope.user.UserId,
                Content: $scope.filter.comment
            }, function(){
                if (json.data.success) {
                    toastr.success('Comment added.');
                    getComments();
                    $scope.filter.comment = '';
                }
            });
        };

        $scope.dateService = DateService;

        var signInModal = function (cb) {
            $uibModal.open({
                templateUrl: APP_URL + 'internal/sign-in/sign-in.html',
                controller: 'SignInCtrl',
                animation: true,
                keyboard: false,
                backdrop: 'static',
            }).result.then(function () {
                    $scope.user = SessionService.get();
                    cb();
                });
        };

        $scope.onClickSignIn = function (cb) {
            signInModal(cb);
        };

        $scope.onLikeTroll = function(){
            if($scope.user==null){
                $scope.onClickSignIn(function(){
                    $scope.onLikeTroll();
                });
            } else {
                var json = TrollService.LikeTroll({
                    trollId: $scope.troll.TrollId,
                    userId: $scope.user.UserId
                }, function () {
                    var result = json.data;
                    if (result.success) {
                        var updatedTroll = result.datas.troll;
                        $scope.troll = updatedTroll;
                    }
                });
            }
        };

        $scope.isTrollLikedByMe = function(){
            if($scope.user==null) return false;

            var result = false;
            if($scope.troll==null) return false;
            if($scope.troll.UserLikes!=null){
                angular.forEach($scope.troll.UserLikes, function(v,i){
                    if(v==$scope.user.Username)
                        result = true;
                });
            }
            return result;
        };

        $scope.onVote = function (index, commentId, isUpVote) {

            if($scope.user==null){
                toastr.warning('Please loginn first.');
                return;
            }

            var json = TrollService.VoteComment({
                commentId: commentId,
                userId: $scope.user.UserId,
                isUpVote: isUpVote
            }, function () {
                var data = json.data;
                if (data.success) {
                    var updatedComment = data.datas.comment;
                    $scope.troll.Comments[index] = updatedComment;
                }
            });
        };

        $scope.onReplyClicked = function(index, value){
            angular.forEach($scope.troll.Comments, function(c,i){
                c.isReplyClicked = false;
            });
            $scope.troll.Comments[index].isReplyClicked = value;
        };

        $scope.onSaveReply = function(comment){
            if ($scope.filter.reply != null && $scope.filter.reply != '') {
                var dataModel = {
                    CommentId: comment.CommentId,
                    UserId: $scope.user.UserId,
                    Content: angular.copy($scope.filter.reply)
                };
                var json = TrollService.SaveReply({
                    model: dataModel
                }, function () {
                    var data = json.data;
                    if (data.success) {
                        toastr.success('Reply added.');
                        comment.Replies = data.datas.replies;
                        $scope.filter.reply = '';
                    }
                });
            }
        };

        $rootScope.$on('sign-in', function(){
            $scope.user = SessionService.get();
        });


        $scope.showDateTime = function (date) {
            return moment(date).calendar();
        };

        $scope.showDateTimeNewestPost = function (date) {
            return moment(date).format('DD MMM YYYY');
        };

        var postFbModal = function (troll, link) {
            $uibModal.open({
                templateUrl: APP_URL + 'post-to-facebook/post-to-facebook.html',
                controller: 'PostToFacebookCtrl',
                animation: true,
                keyboard: false,
                backdrop: 'static',
                resolve: {
                    data: function () {
                        return angular.copy(troll);
                    },
                    link: function(){
                        return angular.copy(link);
                    },
                    type: function(){
                        return 'troll'
                    }
                }
            }).result.then(function () {

                });
        };

        $scope.postToFacebook = function (troll) {
            var link = $location.absUrl();
            postFbModal(troll, link);
        };
    });
});
