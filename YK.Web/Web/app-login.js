angular.module('YK', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate', 'ngResource']);

angular.module('YK').config(function($routeProvider) {
    /* Add New Routes Above */
    $routeProvider.otherwise({redirectTo:'/home'});

});

angular.module('YK').run(function($rootScope, $location, AuthenticationService, UserService) {
    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $rootScope.loginUrl = '/Web/login.html';
    var checkLoginStatus = function (callbackIfSuccess) {
        var jsonResult = AuthenticationService.IsLogin(function () {
            var isLoggedIn = jsonResult.data;
            if (isLoggedIn) {
                var jsonResultUser = UserService.GetCurrentUser(function (){
                    var user = jsonResultUser.data;
                    if ($location.path() == null || $location.path() == '' || $location.path() == '/') {
                        window.location = "/Web/index.html#" + user.DefaultRoute;
                    }
                    else {
                        window.location = "/Web/index.html#" + $location.$$url;
                    }
                });
            }
        });
    };
    checkLoginStatus();

});

angular.module('YK')
    .controller('LoginCtrl',
    function LoginCtrl($rootScope, $scope, $resource, $location, $timeout, AuthenticationService, UserService, SessionService) {
        console.log("LOGIN CONTROLLER");

        $scope.user = {
            username: null,
            password: null
        };

        $scope.onLogin = function () {
            var jsonLogin = AuthenticationService.Login({
                email: $scope.user.username,
                password: $scope.user.password
            }, function() {
                var data = jsonLogin.data;
                if (data.success) {
                    var user = data.datas.user;
                    var json = UserService.GetUserInfo({
                        username: user.Username
                    }, function() {
                        var userInfo = json.data;
                        SessionService.set(userInfo);
                        $timeout(function() {
                            window.location = "/Web/index.html#" + userInfo.Role.DefaultRoute;
                        }, 1000);
                    });
                } else {
                    if (data.validations != null) {
                        angular.forEach(data.validations, function(v, i) {
                            //$rootScope.addInfo("error", v.message);
                        });
                    }
                }
            });
        };

    });

