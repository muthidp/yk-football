var APP_URL = '/Web/app/';

toastr.options = {
    closeButton: true,
    progressBar: false,
    showMethod: 'slideDown',
    timeOut: 2000,
    positionClass: 'toast-top-center',
};

angular.module('YK', [
    'ui.bootstrap',
    'ui.utils',
    'ngRoute',
    'ngAnimate',
    'ngResource',
    'ngUpload',
    'ngSanitize',
    'blueimp.fileupload',
    'wysiwyg.module',
    'angular-linq']);

angular.module('YK')
    .config(function ($routeProvider) {

        $routeProvider.when('/master/user', {templateUrl: 'app/master/user/user.html'});
        $routeProvider.when('/master/category', {templateUrl: 'app/master/category/category.html'});
        $routeProvider.when('/master/tag', {templateUrl: 'app/master/tag/tag.html'});
        $routeProvider.when('/master/team', {templateUrl: 'app/master/team/team.html'});
        $routeProvider.when('/master/league', {templateUrl: 'app/master/league/league.html'});
        $routeProvider.when('/post/:postId', {templateUrl: 'app/blog/post/post.html'});
        $routeProvider.when('/master/ad', {templateUrl: 'app/master/ad/ad.html'});

        $routeProvider.when('/postDetail/:postId', {templateUrl: 'app/blog/post-detail/post-detail.html'});
        $routeProvider.when('/home', {templateUrl: 'app/home/home.html'});
        $routeProvider.when('/leagueTimeline', {templateUrl: 'app/blog/league-timeline/league-timeline.html'});
        $routeProvider.when('/master/country', {templateUrl: 'app/master/country/country.html'});
        $routeProvider.when('/addTroll', {templateUrl: 'app/troll/troll-creation/troll-creation.html'});
        $routeProvider.when('/trollTimeline', {templateUrl: 'app/troll/troll-timeline/troll-timeline.html'});
        $routeProvider.when('/singleTroll/:trollId', {templateUrl: 'app/troll/single-troll/single-troll.html'});
        $routeProvider.when('/match', {templateUrl: 'app/match/match.html'});
		$routeProvider.when('/blogTimeline', {templateUrl: 'app/blog/blog-timeline/blog-timeline.html'});
        /* Add New Routes Above */
        $routeProvider.otherwise({redirectTo: '/home'});

    })

    .config(function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q) {
            return {
                'response': function (response) {
                    if (response.data.exceptionOccurred == true) {
                        toastr.error('System error. Please contact administrator');
                        return $q.reject(response);
                    }

                    if(response.data!=null){
                        if(response.data.data!=null){
                            if (response.data.data.exceptionOccurred == true) {
                                toastr.error('System error. Please contact administrator');
                                return $q.reject(response);
                            }
                        }
                    }
                    return response;
                },
                'responseError': function (rejection) {
                    toastr.error('System error. Please contact administrator');
                    return $q.reject(rejection);
                }
            };
        });

        $httpProvider.defaults.useXDomain = true;
        //$httpProvider.defaults.withCredentials = true;
        //delete $httpProvider.defaults.headers.common["X-Requested-With"];
        //$httpProvider.defaults.headers.common["Accept"] = "application/json";
        //$httpProvider.defaults.headers.common["Content-Type"] = "application/json";
    });

angular.module('YK').run(function ($rootScope, $location, $timeout, AuthenticationService, UserService, SessionService, AdService) {
    var applicationInitialized = false;
    $rootScope.safeApply = function (fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    $rootScope.startLoading = false;
    $rootScope.showLoading = function () {
        $rootScope.isLoading = true;
    };
    $rootScope.hideLoading = function () {
        $rootScope.isLoading = false;
    };

    var getIsLogin = function () {
        $rootScope.isLogin = SessionService.isLogin();
    };
    getIsLogin();

    $rootScope.loginUrl = '/Web/login.html';
    var checkLoginStatus = function (callbackIfSuccess) {
        $rootScope.showLoading();
        var jsonResult = AuthenticationService.IsLogin(function () {
            var isLoggedIn = jsonResult.data;
            if (isLoggedIn) {
                var jsonResultUser = UserService.GetCurrentUser(function () {
                    var user = jsonResultUser.data;
                    SessionService.set(user);
                    getIsLogin();
                    //if ($location.path() == null || $location.path() == '' || $location.path() == '/') {
                    //    window.location = "/Web/index.html#" + user.DefaultRoute;
                    //}
                    //else {
                    //    window.location = "/Web/index.html#" + $location.$$url;
                    //}
                    getAllAds();
                    $rootScope.hideLoading();
                });
            } else {
                //if ($location.$$absUrl.indexOf("login") <= -1) {
                //    if ($location.path() == null || $location.path() == '' || $location.path()) {
                //        window.location = $rootScope.loginUrl + '#/';
                //    }
                //    else {
                //        window.location = $rootScope.loginUrl + '#' + $location.$$url;
                //    }
                //}
                SessionService.setIsLogin(false);
                getAllAds();
                $rootScope.hideLoading();
            }
        });
    };
    checkLoginStatus();

    $rootScope.ads = [];
    var getAllAds = function () {
        var json = AdService.GetAll(function () {
            $rootScope.ads = json.data;
            applicationInitialized = true;
        });
    };

    $rootScope.waitInitialized = function (context) {
        if (applicationInitialized) {
            $rootScope.$broadcast('ads-refresh');
            context();
        } else {
            $timeout(function () {
                $rootScope.waitInitialized(context);
            }, 100);
        }
    };

    $rootScope.$on('sign-in', function (event, args) {
        getIsLogin();
    });
    $rootScope.$on('sign-out', function (event, args) {
        getIsLogin();
    });


});
