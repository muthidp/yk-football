angular.module('YK').directive("adsPlayer", function ($rootScope) {
    return function (scope, elem, attrs) {
        $rootScope.waitInitialized(function(){
            var element = elem[0];
            var attribute = attrs;

            var refreshAd = function() {
                var ad = null;
                if ($rootScope.ads != null) {
                    angular.forEach($rootScope.ads, function (v, i) {
                        if (v.AdIdentifier == element.id) {
                            if(v.ImageUrl!=null) {
                                ad = v;
                                var path = v.ImageUrl.replace(/\\/g, '/');
                                element.style.backgroundImage = 'url("' + path + '")';
                                element.style.backgroundSize = 'contain';
                                element.innerText = '';
                            }
                        }
                    });
                };
            };
            refreshAd();

            $rootScope.$on('ads-refresh', function () {
                refreshAd();
            });
        });

    };
});
