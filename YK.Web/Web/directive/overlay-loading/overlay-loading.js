angular.module("YK")
    .directive("overlayLoading", function() {
        return {
            restrict: 'E',
            scope: {
                startLoading: "="
            },
            template:   '<div ng-if="startLoading" ng-style="loadingImageCss.container">' +
            '   <div ng-style="loadingImageCss.tableContainer">' +
            '       <p ng-style="loadingImageCss.cellContainer">' +
            '           <img ng-src="{{loadingImage}}" />' +
            '       </p>' +
            '   </div>' +
            '</div>',
            controller: ['$scope', function ($scope) {
                $scope.loadingImage = '/Web/directive/overlay-loading/loader.gif';
                $scope.loadingImageCss = {
                    container: {
                        'position': 'fixed',
                        'top': '0em',
                        'right': '0em',
                        'width': '100%',
                        'height': '100%',
                        'z-index': '99',
                        'background-color': 'rgba(96, 92, 92, 0.75)'
                    },
                    tableContainer: {
                        'display': 'table',
                        'position': 'relative',
                        'width': '100%',
                        'height': '100%',
                        'text-align': 'center'
                    },
                    cellContainer: {
                        'display': 'table-cell',
                        'vertical-align': 'middle'
                    }
                };
            }]
        };
    });
