angular.module('YK').directive('ykFileUploader', function () {
    return {
        restrict: 'EA',
        scope: {
            fileResult: '=',
            pickFn: '&'
        },
        replace: true,
        transclude: true,
        templateUrl: 'directive/file-upload/file-upload.html',
        controller: function ($scope, $rootScope, fileUpload) {
            var defaultHandler = fileUpload.defaults;
            console.log("FILE UPLOAD DIRECTIVE CONTROLLWR");
            $scope.uploadConfig = {
                url: '/FileUpload/Upload',
                add: function (e, data) {
                    $scope.queue = [];
                    $scope.preview = false;
                    $scope.previewSrc = '';

                    // Modify the state
                    defaultHandler.add(e, data);
                    data.submit();
                },
                done: function (e, data) {
                    var fileResult = data.result.data.datas.file;
                    $scope.fileResult = fileResult;
                    $scope.pickFn(
                        {
                            result: $scope.fileResult
                        });
                }
            };
        },
        link: function (scope, element, attrs) {
        }
    };
});
