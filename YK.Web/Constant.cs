﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace YK.Web
{
    public class Constant
    {
        public static string WEBSITE_NAME
        {
            get
            {
                return ConfigurationManager.AppSettings["WEBSITE_NAME"];
            }
        }

        public static string FB_APP_ID
        {
            get
            {
                return ConfigurationManager.AppSettings["FB_APP_ID"];
            }
        }

        public static string FB_APP_SECRET
        {
            get
            {
                return ConfigurationManager.AppSettings["FB_APP_SECRET"];
            }
        }
    }
}