﻿using Autofac;
using Autofac.Integration.Mvc;
using Core;
using EMER.Web;
using YK.Service;

namespace YK.Web
{
    public class AutofacConfig
    {
        public static void Register(ContainerBuilder builder)
        {
            RegisterControllers(builder);
            RegisterServices(builder);
            builder.RegisterFilterProvider();
            builder.RegisterType<ContextProvider>().As<IContextProvider>().InstancePerHttpRequest();
        }

        private static void WireProperties(ContainerBuilder builder)
        {
            //builder.RegisterType<WebApiAuthorizeAttribute>().PropertiesAutowired();
        }

        private static void RegisterControllers(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            Dependency.Register(builder);

            ServiceConfig.RegisterRepositories(builder);
        }
    }
}