﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using DotNetOpenAuth.AspNet.Clients;
using Owin;
using Microsoft.Web.WebPages.OAuth;

namespace YK.Web
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            //var facebookAuthenticationOptions = new FacebookAuthenticationOptions()
            //{
            //    AppId = Constant.FB_APP_ID,
            //    AppSecret = Constant.FB_APP_SECRET,
            //    AuthenticationType = "Facebook",
            //    SignInAsAuthenticationType = "ExternalCookie",
            //    Provider = new FacebookAuthenticationProvider
            //    {
            //        OnAuthenticated = async ctx =>
            //        {
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.Country, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.Gender, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.MobilePhone, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.OtherPhone, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.HomePhone, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.StateOrProvince, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.Email, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.Country, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.Actor, ctx.User["birthday"].ToString()));
            //            ctx.Identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, ctx.User["birthday"].ToString()));
            //        }
            //    }
            //};
            //facebookAuthenticationOptions.Scope.Add("user_birthday");
            //facebookAuthenticationOptions.Scope.Add("first_name");
            //facebookAuthenticationOptions.Scope.Add("last_name");
            //facebookAuthenticationOptions.Scope.Add("publish_stream");
            //facebookAuthenticationOptions.Scope.Add("user_likes");
            //facebookAuthenticationOptions.Scope.Add("friends_likes");
            //facebookAuthenticationOptions.Scope.Add("user_about_me");
            //facebookAuthenticationOptions.Scope.Add("user_friends");
            //facebookAuthenticationOptions.Scope.Add("user_actions.news");
            //facebookAuthenticationOptions.Scope.Add("user_actions.video");
            //facebookAuthenticationOptions.Scope.Add("user_education_history");
            //facebookAuthenticationOptions.Scope.Add("manage_pages");
            //facebookAuthenticationOptions.Scope.Add("user_interests");
            //facebookAuthenticationOptions.Scope.Add("user_location");
            //facebookAuthenticationOptions.Scope.Add("user_photos");
            //facebookAuthenticationOptions.Scope.Add("user_relationships");
            //facebookAuthenticationOptions.Scope.Add("user_relationship_details");
            //facebookAuthenticationOptions.Scope.Add("user_status");
            //facebookAuthenticationOptions.Scope.Add("user_tagged_places");
            //facebookAuthenticationOptions.Scope.Add("user_videos");
            //facebookAuthenticationOptions.Scope.Add("user_website");
            //facebookAuthenticationOptions.Scope.Add("read_friendlists");
            //facebookAuthenticationOptions.Scope.Add("read_stream");
            //facebookAuthenticationOptions.Scope.Add("email");
            
            //app.UseFacebookAuthentication(facebookAuthenticationOptions);



            var fb = new Dictionary<string, object>();
            fb.Add("scope", "email, publish_actions, user_photos");
            OAuthWebSecurity.RegisterFacebookClient(
                appId: Constant.FB_APP_ID,
                appSecret: Constant.FB_APP_SECRET,
                displayName: "Facebook",
                extraData: fb);

            //OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}
