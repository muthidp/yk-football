﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;

namespace YK.Service.System.Institution
{
    public class InstitutionService : IInstitutionService
    {
        private readonly IRepository<sys_institution> _institutionRepository;

        public InstitutionService(
            IRepository<sys_institution> institutionRepository )
        {
            _institutionRepository = institutionRepository;
        }

        public List<InstitutionModel> GetAll()
        {
            return _institutionRepository.Query().Where(e => e.IsActive)
                .ToList().Select(e => e.TransformTo<InstitutionModel>()).ToList();
        }

        public InstitutionModel FindOne(string institutionCode)
        {
            var ins =
                _institutionRepository.Query()
                    .FirstOrDefault(e => e.InstitutionCode.Equals(institutionCode) && e.IsActive);

            return ins != null ? ins.TransformTo<InstitutionModel>() : null;
        }
    }
}
