﻿using System.Collections.Generic;
using Core.DTO.Sys;

namespace YK.Service.System.Institution
{
    public interface IInstitutionService
    {
        List<InstitutionModel> GetAll();
        InstitutionModel FindOne(string institutionCode);
    }
}
