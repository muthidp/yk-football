﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;

namespace YK.Service.System.User
{
    public class UserValidator : BaseValidator<UserModel>
    {
        private readonly IRepository<sys_user> _userRepository;

        public UserValidator(
            IRepository<sys_user> userRepository )
        {
            _userRepository = userRepository;
        }

        public override IOperationInfo Validate(UserModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.UserUniqueId))
            {
                info.AttachValidation("UserUniqueId", "Please enter User ID");
            }
            else
            {
                if (String.IsNullOrEmpty(dataModel.Username))
                {
                    info.AttachValidation("Username", "Please enter Name");
                }

                var existingUser =
                    _userRepository.GetActive()
                        .FirstOrDefault(e => e.UserUniqueId.ToLower().Equals(dataModel.UserUniqueId.ToLower()));

                if (existingUser != null)
                {
                    // IF NEW USER
                    if (!(dataModel.UserId > 0))
                    {
                        info.AttachValidation("UserUniqueId", "User ID already exist");
                    }
                    // CURRENT USER CHANGE USERNAME, BUT ALREADY EXIST
                    else if(dataModel.UserId != existingUser.UserId)
                    {
                        info.AttachValidation("UserUniqueId", "User ID already exist");
                    }
                }
            }

            return info;
        }
    }
}
