﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Core;
using Core.DTO.Sys;
using Core.Helper;
using DAL.Database.Sys;
using Newtonsoft.Json.Linq;

namespace YK.Service.System.User
{
    public class UserService : IUserService
    {
        private readonly IRepository<sys_user> _userRepository;
        private readonly IRepository<sys_role_acl> _roleAclRepository;
        private readonly IRepository<sys_role> _roleRepository;
        private readonly IRepository<sys_institution> _institutionRepository;
        private readonly IValidator<UserModel> _userValidator;

        public UserService(
            IRepository<sys_user> userRepository,
            IRepository<sys_role_acl> roleAclRepository,
            IRepository<sys_role> roleRepository,
            IRepository<sys_institution> institutionRepository,
            IValidator<UserModel> userValidator)
        {
            _userRepository = userRepository;
            _roleAclRepository = roleAclRepository;
            _roleRepository = roleRepository;
            _institutionRepository = institutionRepository;
            _userValidator = userValidator;
        }

        public UserInfoModel GetCurrentUser()
        {
            var userId = HttpContext.Current.User.Identity.Name;
            var user = _userRepository.Query().FirstOrDefault(e => e.IsActive && (e.UserUniqueId.Equals(userId))) ??
                       _userRepository.Query().FirstOrDefault(e => e.IsActive && (e.Username.Equals(userId)));

            if (user == null) return null;
            return BuildUserInfo(user);
        }

        public UserInfoModel BuildUserInfo(sys_user user)
        {
            if (user == null) return null;
            var model = user.TransformTo<UserInfoModel>();
            model.DefaultRoute = user.Role != null ? user.Role.DefaultRoute : "";

            model.Role = user.Role.TransformTo<RoleModel>();
            model.Institution = user.Institution.TransformTo<InstitutionModel>();
            model.Acls = new List<string>();

            if (model.Role != null)
            {
                var acls =
                    _roleAclRepository.Query()
                        .Where(e => e.RoleId == model.Role.RoleId && e.IsActive && e.AclId != null)
                        .Select(e => e.Acl.Key)
                        .ToList();
                model.Acls = acls;
            }

            return model;
        }

        public UserInfoModel GetUserInfo(string username)
        {
            var user = _userRepository.Query().FirstOrDefault(e => e.IsActive && e.Username.Equals(username));
            if (user == null) return null;

            return BuildUserInfo(user);
        }

        public List<UserInfoModel> GetUserInstructors(long? institutionId)
        {
            var users = _userRepository.Query()
                .Where(e => e.IsActive && e.Role.RoleCode.Equals("INSTRUCTOR"))
                .ToList();

            return users.Select(BuildUserInfo).ToList();
        }

        //For CRUD
        public List<UserInfoModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel)
        {
            var users = _userRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .QueryResult(ref queryModel)
                .ToList();


            List<UserInfoModel> userinfo = new List<UserInfoModel>();

            foreach (var u in users)
            {
                UserInfoModel user = u.TransformTo<UserInfoModel>();

                var role = _roleRepository.Query().Where(r => r.RoleId == user.RoleId
                    && r.IsActive).FirstOrDefault();

                user.Role = new RoleModel();
                if (role != null)
                {
                    user.Role = role.TransformTo<RoleModel>();
                }

                var institution = _institutionRepository.Query().Where(f => f.InstitutionId == user.InstitutionId
                   && f.IsActive).FirstOrDefault();


                user.Institution = new InstitutionModel();
                if (institution != null)
                    user.Institution = institution.TransformTo<InstitutionModel>();

                userinfo.Add(user);
            }

            return userinfo;
        }

        public List<UserInfoModel> GetAll(long? institutionId)
        {
            var Users = _userRepository.Query()
                .Where(e => e.InstitutionId == institutionId &&
                            e.IsActive)
                .ToList();

            List<UserInfoModel> userinfo = new List<UserInfoModel>();

            Users.ForEach(u =>
            {
                UserInfoModel user = u.TransformTo<UserInfoModel>();

                var role = _roleRepository.Query().Where(r => r.RoleId == user.RoleId
                    && r.IsActive).FirstOrDefault();


                user.Role = new RoleModel();
                if (role != null)
                    user.Role = role.TransformTo<RoleModel>();

                var institution = _institutionRepository.Query().Where(f => f.InstitutionId == user.InstitutionId
                   && f.IsActive).FirstOrDefault();

                user.Institution = new InstitutionModel();
                if (institution != null)
                    user.Institution = institution.TransformTo<InstitutionModel>();

                userinfo.Add(user);
            });

            return userinfo;
        }

        public IOperationInfo Save(UserModel dataModel)
        {
            var info = _userValidator.GenerateOperationInfo();
            try
            {
                info = _userValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var user = dataModel.UserId > 0
                        ? _userRepository.Query().First(e => e.UserId == dataModel.UserId)
                        : new sys_user();

                    user = dataModel.MapExcludeKeyTo(user);
                    user.UserId = dataModel.UserId;
                    user.Password = dataModel.Password.GetMd5Hash();

                    user.RoleId = dataModel.RoleId > 0 ? dataModel.RoleId : null;
                    //user.InstitutionId = dataModel.InstitutionId;
                    user.Deactivated = false;

                    _userRepository.Save(user);
                    _userRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long userId)
        {
            var info = new OperationInfo();
            try
            {
                var user = _userRepository.Query().FirstOrDefault(e => e.UserId == userId);
                if (user != null)
                {
                    _userRepository.SoftDelete(user);
                    _userRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo SaveUserExternalLogin(UserModel dataModel)
        {
            var info = _userValidator.GenerateOperationInfo();
            try
            {
                var user =
                    _userRepository.Query()
                        .FirstOrDefault(e => e.UserUniqueId == dataModel.UserUniqueId && e.IsActive) ??
                    new sys_user();

                user = dataModel.MapExcludeKeyTo(user);
                user.Deactivated = false;

                user.UserAvatar = GetFacebookPhoto(user.UserUniqueId);

                _userRepository.Save(user);
                _userRepository.Commit();

                info.Success();

            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        private string GetFacebookPhoto(string id)
        {
            var url = String.Format("http://graph.facebook.com/{0}/picture?width=250&height=250", id);
            var serverPath = HttpContext.Current.Server.MapPath("~");
            var filebucket = Path.Combine(serverPath,String.Format("Filebucket"));
            if (!Directory.Exists(filebucket))
            {
                Directory.CreateDirectory(filebucket);
            }
            var photoFolder = Path.Combine(filebucket, "FacebookPhoto");
            if (!Directory.Exists(photoFolder))
            {
                Directory.CreateDirectory(photoFolder);
            }
            var filePath = Path.Combine(photoFolder, String.Format("{0}.jpg", id));
            WebClient webClient = new WebClient();
            webClient.DownloadFile(url, filePath);

            var relativePath = filePath.Replace(serverPath, "");
            return "\\"+relativePath;
        }

        private JObject HttpRequest(string url)
        {
            var webRequest = WebRequest.Create(url);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            JObject result = null;
            try
            {
                var webResponse = webRequest.GetResponse();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(webResponse.GetResponseStream());
                    var data = sr.ReadToEnd();
                    result = JObject.Parse(data);
                }
                finally
                {
                    if (sr != null) sr.Close();
                }
            }
            catch (WebException ex)
            {
                StreamReader errorStream = null;
                try
                {
                    errorStream = new StreamReader(ex.Response.GetResponseStream());
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (errorStream != null) errorStream.Close();
                }

            }

            return result;
        }
    }
}
