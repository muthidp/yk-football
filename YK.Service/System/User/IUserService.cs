﻿using System.Collections.Generic;
using Core;
using Core.DTO.Sys;

namespace YK.Service.System.User
{
    public interface IUserService
    {
        UserInfoModel GetCurrentUser();
        UserInfoModel GetUserInfo(string username);
        List<UserInfoModel> GetUserInstructors(long? institutionId);
        List<UserInfoModel> GetAllWithBaseQuery(long? institutionId, ref BaseQueryModel queryModel);
        List<UserInfoModel> GetAll(long? institutionId);
        IOperationInfo Save(UserModel dataModel);
        IOperationInfo Delete(long userId);
        IOperationInfo SaveUserExternalLogin(UserModel dataModel);
    }
}
