﻿using System.Collections.Generic;
using System.Linq;
using Core;
using Core.DTO.Sys;
using DAL.Database.Sys;

namespace YK.Service.System.ModuleGroup
{
    public class ModuleGroupService : IModuleGroupService
    {
        private readonly IRepository<sys_module_group> _moduleGroupRepository;

        public ModuleGroupService(
            IRepository<sys_module_group> moduleGroupRepository )
        {
            _moduleGroupRepository = moduleGroupRepository;
        }

        public List<ModuleGroupInfo> GetAllModuleGroups()
        {
            var result = new List<ModuleGroupInfo>();
            var moduleGroups = _moduleGroupRepository.Query().Where(e => e.IsActive).OrderBy(e=> e.Sequence).ToList();
            moduleGroups.ForEach(mg =>
            {
                var moduleGroup = mg.TransformTo<ModuleGroupInfo>();
                moduleGroup.AclKey = mg.Acl != null ? mg.Acl.Key : "";
                moduleGroup.Modules = new List<ModuleModel>();
                if (mg.Modules != null)
                {
                    mg.Modules.Where(e=> e.IsActive).ToList().ForEach(m =>
                    {
                        var module = m.TransformTo<ModuleModel>();
                        module.AclKey = m.Acl != null ? m.Acl.Key : "";
                        moduleGroup.Modules.Add(module);
                    });
                }
                result.Add(moduleGroup);
            });
            return result;
        } 

    }
}
