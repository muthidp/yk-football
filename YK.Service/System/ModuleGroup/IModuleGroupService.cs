﻿using System.Collections.Generic;
using Core.DTO.Sys;

namespace YK.Service.System.ModuleGroup
{
    public interface IModuleGroupService
    {
        List<ModuleGroupInfo> GetAllModuleGroups();
    }
}
