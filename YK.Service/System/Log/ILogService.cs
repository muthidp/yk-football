﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Database.Sys;

namespace YK.Service.System.Log
{
    public interface ILogService
    {
        void Log(string identifier, LogStatus status, Exception ex = null);
    }
}
