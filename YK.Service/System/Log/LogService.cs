﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace YK.Service.System.Log
{
    public class LogService : ILogService
    {
        private readonly IRepository<sys_log> _logRepository;

        public LogService(
            IRepository<sys_log> logRepository)
        {
            _logRepository = logRepository;
        }

        public void Log(string identifier, LogStatus status, Exception ex = null)
        {
            var log = new sys_log()
            {
                LogIdentifier = identifier,
                LogDetail = ex != null ? (ex.InnerException != null ? ex.InnerException.Message : ex.Message) : "OK",
                Status = status
            };
            _logRepository.Save(log);
            _logRepository.Commit();
        }
    }
}
