﻿using Core;

namespace YK.Service
{
    public abstract class BaseValidator<T> : IValidator<T>
    {
        public abstract IOperationInfo Validate(T dataModel, IOperationInfo info);

        public IValidationInfo GenerateValidationInfo()
        {
            return new ValidationInfo();
        }

        public IOperationInfo GenerateOperationInfo()
        {
            return new OperationInfo();
        }
    }
}
