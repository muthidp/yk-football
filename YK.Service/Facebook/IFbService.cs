﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Facebook
{
    public interface IFbService
    {
        IOperationInfo PostToFacebook(TrollModel troll, string message, string link);
        IOperationInfo PostToFacebook(PostModel troll, string message, string link);
    }
}
