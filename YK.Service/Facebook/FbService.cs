﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Core;
using Core.DTO.Yk;
using DAL.Database.Sys;

namespace YK.Service.Facebook
{
    public class FbService : IFbService
    {
        public FbService()
        {

        }

        public IOperationInfo PostToFacebook(TrollModel troll, string message, string link)
        {
            var info = new OperationInfo();
            try
            {
                if (String.IsNullOrEmpty(troll.Title)) message = "";

                var parameters = ""
                                 + AppendKeyvalue("name", "YK Football")
                                 + AppendKeyvalue("link", link)
                                 + AppendKeyvalue("picture", troll.ContentUrl)
                                 + AppendKeyvalue("message", message);

                Post(parameters);
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo PostToFacebook(PostModel post, string message, string link)
        {
            var info = new OperationInfo();
            try
            {
                if (String.IsNullOrEmpty(post.Title)) message = "";

                var parameters = ""
                + AppendKeyvalue("name", "YK Football")
                + AppendKeyvalue("link", link)
                + AppendKeyvalue("picture", post.PictureUrl)
                + AppendKeyvalue("message", message);

                Post(parameters);
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        private string BuildPostUrl()
        {
            var sessionFbData = HttpContext.Current.Session["fb_data"];
            var fbExtraData = (IDictionary<string, string>)sessionFbData;
            var id = fbExtraData["id"];
            var token = fbExtraData["accesstoken"];
            string path = String.Format("https://graph.facebook.com/{0}", id);

            var url = path + "/feed?" +
            AppendKeyvalue("access_token", token);

            return url;
        }

        private void Post(string parameters)
        {
            var url = BuildPostUrl();
            var webRequest = WebRequest.Create(url);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = Encoding.ASCII.GetBytes(parameters);
            webRequest.ContentLength = bytes.Length;
            Stream os = webRequest.GetRequestStream();
            os.Write(bytes, 0, bytes.Length);
            os.Close();

            // Send the request to Facebook, and query the result to get the confirmation code
            try
            {
                var webResponse = webRequest.GetResponse();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(webResponse.GetResponseStream());
                    string PostID = sr.ReadToEnd();
                }
                finally
                {
                    if (sr != null) sr.Close();
                }
            }
            catch (WebException ex)
            {
                // To help with debugging, we grab
                // the exception stream to get full error details
                StreamReader errorStream = null;
                try
                {
                    errorStream = new StreamReader(ex.Response.GetResponseStream());
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (errorStream != null) errorStream.Close();
                }

            }
        }

        public static string AppendKeyvalue(string key, string value)
        {
            return string.Format("{0}={1}&", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value));
        }
    }
}
