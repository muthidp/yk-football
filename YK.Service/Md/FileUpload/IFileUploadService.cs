﻿using Core;
using Core.DTO.Md;

namespace YK.Service.Md.FileUpload
{
    public interface IFileUploadService
    {
        IOperationInfo Upload(FileUploadModel model);
    }
}
