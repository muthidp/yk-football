﻿using Autofac;
using Core;
using Core.DTO.Sys;
using Core.DTO.Yk;
using YK.Service.Authentication;
using YK.Service.Facebook;
using YK.Service.FootballAPI;
using YK.Service.Md.FileUpload;
using YK.Service.System.Institution;
using YK.Service.System.Log;
using YK.Service.System.ModuleGroup;
using YK.Service.System.User;
using YK.Service.Yk.Category;
using YK.Service.Yk.Tag;
using YK.Service.Yk.Team;
using YK.Service.Yk.League;
using YK.Service.Yk.Post;
using YK.Service.Yk.Ad;
using YK.Service.Yk.Country;
using YK.Service.Yk.Match;
using YK.Service.Yk.Troll;

namespace YK.Service
{
    public class Dependency
    {
        public static ContainerBuilder Register(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().InstancePerRequest();
            builder.RegisterType<InstitutionService>().As<IInstitutionService>().InstancePerRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<UserValidator>().As<IValidator<UserModel>>().InstancePerRequest();

            builder.RegisterType<ModuleGroupService>().As<IModuleGroupService>().InstancePerRequest();

            builder.RegisterType<FileUploadService>().As<IFileUploadService>().InstancePerRequest();

            builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerRequest();
            builder.RegisterType<CategoryValidator>().As<IValidator<CategoryModel>>().InstancePerRequest();

            builder.RegisterType<TagService>().As<ITagService>().InstancePerRequest();
            builder.RegisterType<TagValidator>().As<IValidator<TagModel>>().InstancePerRequest();

            builder.RegisterType<TeamService>().As<ITeamService>().InstancePerRequest();
            builder.RegisterType<TeamValidator>().As<IValidator<TeamModel>>().InstancePerRequest();

            builder.RegisterType<LeagueService>().As<ILeagueService>().InstancePerRequest();
            builder.RegisterType<LeagueValidator>().As<IValidator<LeagueModel>>().InstancePerRequest();

            builder.RegisterType<PostService>().As<IPostService>().InstancePerRequest();
            builder.RegisterType<PostValidator>().As<IValidator<PostModel>>().InstancePerRequest();

            builder.RegisterType<AdService>().As<IAdService>().InstancePerRequest();
            builder.RegisterType<AdValidator>().As<IValidator<AdModel>>().InstancePerRequest();

            builder.RegisterType<CountryService>().As<ICountryService>().InstancePerRequest();
            builder.RegisterType<CountryValidator>().As<IValidator<CountryModel>>().InstancePerRequest();

            builder.RegisterType<TrollService>().As<ITrollService>().InstancePerRequest();
            builder.RegisterType<TrollValidator>().As<IValidator<TrollModel>>().InstancePerRequest();

            builder.RegisterType<FbService>().As<IFbService>().InstancePerRequest();

            builder.RegisterType<FootballApiService>().As<IFootballApiService>().InstancePerRequest();
            builder.RegisterType<MatchService>().As<IMatchService>().InstancePerRequest();

            builder.RegisterType<LogService>().As<ILogService>().InstancePerRequest();

            return builder;
        }
    }
}
