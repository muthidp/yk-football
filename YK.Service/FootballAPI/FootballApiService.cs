﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.FootballApi;
using DAL.Database.Sys;
using DAL.Database.Yk;
using Newtonsoft.Json.Linq;
using YK.Service.Yk.Match;

namespace YK.Service.FootballAPI
{
    public class FootballApiService : IFootballApiService
    {
        private readonly IRepository<sys_application_setting> _applicationSettingRepository;
        private string ApiKey = null;
        public FootballApiService(
            IRepository<sys_application_setting> applicationSettingRepository)
        {
            _applicationSettingRepository = applicationSettingRepository;
            ApiKey = GetApiKey();
        }

        private string GetApiKey()
        {
            var setting = _applicationSettingRepository.GetActive()
                .FirstOrDefault(e => e.KeyValue.Equals("FOOTBALL_API_KEY"));
            if (setting == null) return null;

            return setting.SettingValueDefault;
        }

        public JArray GetCompetitions()
        {
            var url =
                "http://api.football-api.com/2.0/competitions?Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        public JArray GetTodayMatches(int competitionId)
        {
            var url = "http://api.football-api.com/2.0/matches?comp_id=" + competitionId + "&match_date=" + DateTime.Today.ToString("dd.MM.yyyy") + "&Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        public JArray GetLiveMatches()
        {
            var url = "http://http://api.football-api.com/2.0/matches?Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        public JArray GetMatchesByDate(int competitionId, string date)
        {
            var url = "http://api.football-api.com/2.0/matches?comp_id=" + competitionId +
                "&match_date=" + date +
                "&Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        public JArray GetMatchesByDateRange(int competitionId, string fromDate, string toDate)
        {
            var url = "http://api.football-api.com/2.0/matches?comp_id=" + competitionId +
                "&from_date=" + fromDate +
                "&to_date=" + toDate +
                "&Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        public JArray GetStandings(int competitionId)
        {
            var url =
                "http://api.football-api.com/2.0/standings/" + competitionId + "?Authorization=" + ApiKey;
            var result = HttpRequest(url);
            return result;
        }

        private JArray HttpRequest(string url)
        {
            var webRequest = WebRequest.Create(url);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "GET";
            JArray result = null;
            try
            {
                var webResponse = webRequest.GetResponse();
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(webResponse.GetResponseStream());
                    var data = sr.ReadToEnd();
                    result = JArray.Parse(data);
                }
                finally
                {
                    if (sr != null) sr.Close();
                }
            }
            catch (WebException ex)
            {
                StreamReader errorStream = null;
                try
                {
                    errorStream = new StreamReader(ex.Response.GetResponseStream());
                }
                finally
                {
                    if (errorStream != null) errorStream.Close();
                }

            }

            return result;
        }
    }
}
