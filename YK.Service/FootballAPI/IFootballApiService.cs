﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace YK.Service.FootballAPI
{
    public interface IFootballApiService
    {
        JArray GetCompetitions();
        JArray GetTodayMatches(int competitionId);
        JArray GetMatchesByDate(int competitionId, string date);
        JArray GetStandings(int competitionId);
        JArray GetMatchesByDateRange(int competitionId, string fromDate, string toDate);
    }
}
