﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YK.Service
{
    public static class MapperHelper
    {
        public static T2 MapExcludeKeyTo<T1, T2>(this T1 source, T2 dest)
            where T1 : class
            where T2 : class
        {
            if (source == null)
                return null;

            if (dest == null)
                return null;

            var dataType = source.GetType();
            var dataTypeProperties = dataType.GetProperties();
            var targetType = dest.GetType();

            foreach (var dataTypeProperty in dataTypeProperties)
            {
                if (!dataTypeProperty.CanRead)
                    continue;

                var theProperty = targetType.GetProperty(dataTypeProperty.Name);

                if (theProperty == null)
                    continue;

                if (!theProperty.CanWrite)
                    continue;

                if (theProperty.PropertyType != dataTypeProperty.PropertyType)
                    continue;

                // Exclude key attribute in the target
                var hasKey = Attribute.IsDefined(theProperty, typeof(KeyAttribute));
                if (!hasKey)
                    theProperty.SetValue(dest, dataTypeProperty.GetValue(source));
            }

            return dest;
        }
    }
}
