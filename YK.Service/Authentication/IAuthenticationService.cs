﻿using Core;

namespace YK.Service.Authentication
{
    public interface IAuthenticationService
    {
        IOperationInfo Authenticate(string userId, string password);
        bool IsLogin();
        void Logout();
    }
}
