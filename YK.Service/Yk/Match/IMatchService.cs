﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.FootballApi;
using Core.DTO.Yk;
using Newtonsoft.Json.Linq;

namespace YK.Service.Yk.Match
{
    public interface IMatchService
    {
        List<MatchCommentModel> GetComments(long? footballMatchId);
        IOperationInfo SaveComment(MatchModel matchModel, List<MatchCommentModel> comments);
        void SyncTodayMatches();
        IOperationInfo SyncFixtures(string date);
        List<LeagueWithMatches> GetMatchesThisWeek();
        List<LeagueWithMatches> GetMatchesByDate(string date);
        List<string> GetLastTeamStanding(long? teamId);
        IOperationInfo DeleteComment(long? matchId, long? matchCommentId);
        List<MatchEventModel> GetMatchEvents(long? matchId);
    }
}
