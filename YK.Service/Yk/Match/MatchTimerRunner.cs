﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Mvc;

namespace YK.Service.Yk.Match
{
    public class MatchTimerRunner : IRegisteredObject
    {
        private Timer _timerTodayMatches;
        private Timer _timerFixtures;
        private readonly int _minutes;
        private readonly int _hour;

        private readonly IMatchService _matchService;
        public MatchTimerRunner()
        {
            const int seconds = 1000;
            _minutes = 60 * seconds;
            _hour = 60 * _minutes;

            _matchService = DependencyResolver.Current.GetService<IMatchService>();

            StartTimer();
        }

        private void StartTimer()
        {
            _timerTodayMatches = new Timer(SyncMatch, null, 1000, 3 * _minutes);
            _timerFixtures = new Timer(SyncFixtures, null, 1000, 1000);
        }

        private void SyncMatch(object state)
        {
            _matchService.SyncTodayMatches();
        }

        private void SyncFixtures(object state)
        {
            var now = DateTime.Now;
            var nowComparison = now.ToString("HHmmss");
            if (nowComparison == "001500")
            {
                _matchService.SyncFixtures(DateTime.Today.ToString("MM/dd/yyyy"));
            }
        }

        public void Stop(bool immediate)
        {
            _timerTodayMatches.Dispose();

            HostingEnvironment.UnregisterObject(this);
        }
    }
}
