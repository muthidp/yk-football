﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using Core;
using Core.DTO.FootballApi;
using Core.DTO.Yk;
using DAL.Database.Sys;
using DAL.Database.Yk;
using LinqKit;
using Newtonsoft.Json.Linq;
using YK.Service.FootballAPI;
using YK.Service.System.Log;
using YK.Service.Yk.League;

namespace YK.Service.Yk.Match
{
    public class MatchService : IMatchService
    {
        private readonly IRepository<yk_match> _matchRepository;
        private readonly IRepository<yk_match_comment> _matchCommentRepository;
        private readonly IFootballApiService _footballApiService;
        private readonly ILeagueService _leagueService;
        private readonly ILogService _logService;
        private readonly IRepository<yk_match_event> _matchEventRepository;

        public MatchService(
            IRepository<yk_match> matchRepository,
            IRepository<yk_match_comment> matchCommentRepository,
            IFootballApiService footballApiService,
            ILeagueService leagueService,
            ILogService logService,
            IRepository<yk_match_event> matchEventRepository)
        {
            _matchRepository = matchRepository;
            _matchCommentRepository = matchCommentRepository;
            _footballApiService = footballApiService;
            _leagueService = leagueService;
            _logService = logService;
            _matchEventRepository = matchEventRepository;
        }

        private long? StringToLong(string label)
        {
            long value;
            if (!Int64.TryParse(label, out value))
            {
                return null;
            }
            return value;
        }

        public IOperationInfo SaveMatch(JObject matchModel)
        {
            var info = new OperationInfo();
            try
            {
                if (matchModel != null)
                {
                    var match = SaveMatchVoid(matchModel);
                    info.AttachData("match", match);
                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public yk_match SaveMatchVoid(JObject matchModel)
        {
            var model = MatchHelper.BuildYkMatch(matchModel);
            return SaveMatchEntity(model);
        }

        public yk_match SaveMatchEntity(yk_match matchModel)
        {
            var match = _matchRepository.GetActive()
                .FirstOrDefault(e => e.FootballMatchId == matchModel.FootballMatchId) ?? new yk_match();

            var events = new List<yk_match_event>();
            if (matchModel.Events != null)
            {
                matchModel.Events.ToList().ForEach(e =>
                {
                    events.Add(e);
                });
            }

            match = matchModel.MapExcludeKeyTo(match);
            match.IsActive = true;
            if (!(match.MatchId > 0))
            {
                match.CreatedBy = HttpContext.Current.User.Identity.Name;
                match.CreatedTime = DateTime.Now;
            }
            match.LastUpdatedBy = HttpContext.Current.User.Identity.Name;
            match.LastUpdatedTime = DateTime.Now;

            _matchRepository.Save(match);
            _matchRepository.Commit();

            var matchEvents = _matchEventRepository.Query().Where(e => e.MatchId == match.MatchId).ToList();
            _matchEventRepository.BulkDelete(matchEvents);
            _matchEventRepository.Commit();

            if (events != null)
            {
                var list = new List<yk_match_event>();
                events.ToList().ForEach(e =>
                {
                    var matchEvent = e;
                    matchEvent.MatchId = match.MatchId;
                    matchEvent.IsActive = true;
                    matchEvent.CreatedBy = HttpContext.Current.User.Identity.Name;
                    matchEvent.CreatedTime = DateTime.Now;
                    matchEvent.LastUpdatedBy = HttpContext.Current.User.Identity.Name;
                    matchEvent.LastUpdatedTime = DateTime.Now;
                    list.Add(matchEvent);
                });
                _matchEventRepository.BulkInsert(list);
            }
            _matchEventRepository.Commit();

            return match;
        }

        public List<MatchCommentModel> GetComments(long? footballMatchId)
        {
            var matchComments = _matchCommentRepository.GetActive()
                .Where(e => e.Match.FootballMatchId == footballMatchId)
                .ToList()
                .Select(e => e.TransformTo<MatchCommentModel>())
                .ToList();
            return matchComments;
        }

        public IOperationInfo SaveComment(MatchModel matchModel, List<MatchCommentModel> comments)
        {
            var info = new OperationInfo();
            try
            {
                var currentComments =
                    _matchCommentRepository.Query().Where(e => e.MatchId == matchModel.MatchId).ToList();
                _matchCommentRepository.BulkDelete(currentComments);

                if (comments != null)
                {
                    comments.ForEach(c =>
                    {
                        if (String.IsNullOrEmpty(c.Comment)) return;
                        var comm = new yk_match_comment()
                        {
                            MatchId = matchModel.MatchId,
                            Comment = c.Comment
                        };
                        _matchCommentRepository.Save(comm);
                    });
                }
                _matchCommentRepository.Commit();

                info.AttachData("match", matchModel);
                info.AttachData("comments", GetComments(matchModel.FootballMatchId));

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        private void SetSystemCurrentUserContext()
        {
            HttpContext.Current = new HttpContext(new HttpRequest(null, "http://localhost:83", null), new HttpResponse(null));
            var identity = new GenericIdentity("SYSTEM");
            HttpContext.Current.User = new GenericPrincipal(identity, new string[] { });
        }

        public void SyncTodayMatches()
        {
            try
            {
                SetSystemCurrentUserContext();
                var leagues = _leagueService.GetAll();
                leagues.ForEach(l =>
                {
                    int competitionId = l.FootballApiId != null ? (int)l.FootballApiId : 0;
                    var json = _footballApiService.GetTodayMatches(competitionId);
                    if (json == null) return;
                    var matches = json.ToList().Select(e => MatchHelper.BuildYkMatch((JObject)e)).ToList();
                    matches.ForEach(m =>
                    {
                        SaveMatchEntity(m);
                    });
                });
                //_logService.Log("SYNC_TODAY_MATCHES", LogStatus.OK);
            }
            catch (Exception ex)
            {
                _logService.Log("SYNC_TODAY_MATCHES", LogStatus.ERROR, ex);
            }
        }

        public IOperationInfo SyncFixtures(string date)
        {
            var info = new OperationInfo();
            try
            {
                SetSystemCurrentUserContext();
                var startDate = FindThursday();
                if (!string.IsNullOrEmpty(date))
                {
                    if (!DateTime.TryParse(date, out startDate))
                    {
                        startDate = DateTime.Today;
                    };
                }

                var endDate = startDate.AddDays(8);
                var leagues = _leagueService.GetAll();

                leagues.ForEach(l =>
                {
                    int competitionId = l.FootballApiId != null ? (int)l.FootballApiId : 0;
                    var json = _footballApiService.GetMatchesByDateRange(competitionId, startDate.ToString("dd.MM.yyyy"), endDate.ToString("dd.MM.yyyy"));
                    if (json == null) return;
                    var matches = json.ToList().Select(e => MatchHelper.BuildYkMatch((JObject)e)).ToList();
                    matches.ForEach(m =>
                    {
                        SaveMatchEntity(m);
                    });
                });
                _logService.Log("SYNC_FIXTURES", LogStatus.OK);
                info.Success();
            }
            catch (Exception ex)
            {
                _logService.Log("SYNC_FIXTURES", LogStatus.ERROR, ex);
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        private DateTime FindThursday()
        {
            var today = DateTime.Today;
            for (int i = 0; i > -7; i--)
            {
                var newDate = today.AddDays(i);
                var dayOfWeek = newDate.DayOfWeek;
                if (dayOfWeek == DayOfWeek.Thursday)
                {
                    return newDate;
                }
            }
            return today;
        }

        public List<LeagueWithMatches> GetMatchesThisWeek()
        {
            var startDate = FindThursday();
            var endDate = startDate.AddDays(6) + TimeSpan.Parse("23:59:59");

            var matches = _matchRepository.GetActive()
                .Where(e => e.MatchDateTime >= startDate && e.MatchDateTime <= endDate).ToList();

            var result = new List<LeagueWithMatches>();
            if (!matches.Any()) return result;
            var leagues = _leagueService.GetAll();
            leagues.ForEach(l =>
            {
                var leagueModel = l.TransformTo<LeagueWithMatches>();
                leagueModel.Matches =
                    matches.Where(e => e.FootballLeagueId == l.FootballApiId)
                    .OrderBy(e => e.MatchDateTime)
                        .ToList()
                        .Select(e => e.TransformTo<MatchModel>())
                        .ToList();
                result.Add(leagueModel);
            });
            return result;
        }

        public List<LeagueWithMatches> GetMatchesByDate(string date)
        {
            var startDate = DateTime.Today;
            if (!string.IsNullOrEmpty(date))
            {
                startDate = DateTime.Parse(date).Date;
            }
            var endDate = startDate + TimeSpan.Parse("23:59:59");

            var matches = _matchRepository.GetActive()
                .Where(e => e.MatchDateTime >= startDate && e.MatchDateTime <= endDate).ToList();

            var result = new List<LeagueWithMatches>();
            //if (!matches.Any()) return result;
            var leagues = _leagueService.GetAll();
            leagues.ForEach(l =>
            {
                var leagueModel = l.TransformTo<LeagueWithMatches>();
                leagueModel.Matches =
                    matches.Where(e => e.FootballLeagueId == l.FootballApiId)
                    .OrderBy(e => e.MatchDateTime)
                        .ToList()
                        .Select(e => e.TransformTo<MatchModel>())
                        .ToList();
                result.Add(leagueModel);
            });
            return result;
        }

        public List<string> GetLastTeamStanding(long? teamId)
        {
            var matches = _matchRepository.GetActive()
                .Where(
                    e =>
                        e.MatchStatus == "FT" &&
                        (e.FootballlLocalTeamId == teamId || e.FootballlVisitorTeamId == teamId))
                        .Take(5)
                        .OrderBy(e => e.MatchDateTime)
                .ToList();

            var result = new List<string>();
            matches.ForEach(m =>
            {
                var status = "-";
                if (teamId == m.FootballlLocalTeamId)
                {
                    var teamScore = long.Parse(m.LocalTeamScore);
                    var opponentScore = long.Parse(m.VisitorTeamScore);
                    if (teamScore > opponentScore)
                    {
                        status = "W";
                    }
                    else if (teamScore == opponentScore)
                    {
                        status = "D";
                    }
                    else
                    {
                        status = "L";
                    }
                }
                else if (teamId == m.FootballlVisitorTeamId)
                {
                    var teamScore = long.Parse(m.VisitorTeamScore);
                    var opponentScore = long.Parse(m.LocalTeamScore);
                    if (teamScore > opponentScore)
                    {
                        status = "W";
                    }
                    else if (teamScore == opponentScore)
                    {
                        status = "D";
                    }
                    else
                    {
                        status = "L";
                    }
                }
                result.Add(status);
            });
            if (result.Count < 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    result.Add("-");
                    if (result.Count == 5) break;
                }
            }
            return result;
        }

        public IOperationInfo DeleteComment(long? matchId, long? matchCommentId)
        {
            var info = new OperationInfo();
            try
            {
                var matchComment = _matchCommentRepository.Query().FirstOrDefault(e => e.MatchCommentId == matchCommentId);
                if (matchComment != null)
                {
                    _matchCommentRepository.Delete(matchComment);
                    _matchCommentRepository.Commit();
                }

                info.AttachData("comments", GetComments(matchId));
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public List<MatchEventModel> GetMatchEvents(long? matchId)
        {
            var data = _matchEventRepository.GetActive()
                .Where(e => e.MatchId == matchId)
                .OrderBy(e=> e.Minute)
                .ToList()
                .Select(e => e.TransformTo<MatchEventModel>())
                .ToList();

            return data;
        }

    }
}
