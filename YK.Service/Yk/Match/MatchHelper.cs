﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Database.Yk;
using Newtonsoft.Json.Linq;

namespace YK.Service.Yk.Match
{
    public static class MatchHelper
    {
        public static yk_match BuildYkMatch(JObject match)
        {
            if (match == null) return null;
            var result = new yk_match()
            {
                FootballMatchId = long.Parse(match["id"].ToString()),
                FootballLeagueId = long.Parse(match["comp_id"].ToString()),
                FootballlLocalTeamId = long.Parse(match["localteam_id"].ToString()),
                FootballlLocalTeamName = match["localteam_name"].ToString(),
                LocalTeamScore = match["localteam_score"].ToString(),
                FootballlVisitorTeamId = long.Parse(match["visitorteam_id"].ToString()),
                FootballlLocalVisitorName = match["visitorteam_name"].ToString(),
                VisitorTeamScore = match["visitorteam_score"].ToString(),
                MatchStatus = match["status"].ToString(),
                MatchEtScore = match["et_score"].ToString(),
                MatchHtScore = match["ht_score"].ToString(),
                MatchFtScore = match["ft_score"].ToString(),
                //MatchCommentaryAvailable = match["commentary_available"].ToString(),
                Events = new List<yk_match_event>()
            };
            var fbMatchDate = match["formatted_date"].ToString();
            var fbMatchTime = match["time"].ToString();

            var fbMatchDateSplitted = fbMatchDate.Split('.').ToList();
            var fbMatchDateFormatted = fbMatchDateSplitted[1] + "/" + fbMatchDateSplitted[0] + "/" + fbMatchDateSplitted[2];

            result.MatchDateTime = DateTime.Parse(fbMatchDateFormatted) + TimeSpan.Parse(fbMatchTime);

            if (match["events"] != null)
            {
                match["events"].ToList().ForEach(e =>
                {
                    int extraMin;
                    int.TryParse(e["extra_min"].ToString(), out extraMin);
                    var matchEvent = new yk_match_event()
                    {
                        FootballMatchEventId = long.Parse(e["id"].ToString()),
                        MatchId = result.MatchId,
                        FootballMatchId = result.FootballMatchId,
                        Type = e["type"].ToString(),
                        Minute = int.Parse(e["minute"].ToString()),
                        ExtraMinute = extraMin,
                        FootballTeamId = e["team"].ToString() == "localteam" ? result.FootballlLocalTeamId : result.FootballlVisitorTeamId,
                        Player = e["player"].ToString(),
                        Assist = e["assist"].ToString(),
                        Result = e["result"].ToString()
                    };
                    result.Events.Add(matchEvent);
                });
            }

            return result;
        }
    }
}
