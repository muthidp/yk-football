﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Troll
{
    public interface ITrollService
    {
        List<TrollModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<TrollModel> GetNewestTrolls(int? limit = null, long? excludeTrollId = null);
        IOperationInfo Save(TrollModel dataModel);
        IOperationInfo SaveComment(TrollCommentModel dataModel);
        IOperationInfo LikeTroll(long? trollId, long? userId);
        List<TrollCommentModel> GetComments(long trollId);
        List<TrollModel> GetTopTrolls(int? limit = null);
        TrollModel GetSingleTroll(long trollId);

        IOperationInfo VoteComment(long commentId, long userId, bool isUpVote);
        TrollCommentModel GetComment(long commentId);
        IOperationInfo SaveReply(CommentReplyModel model);
    }
}
