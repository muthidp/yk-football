﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Troll
{
    public class TrollService : ITrollService
    {
        private readonly IRepository<yk_troll> _trollRepository;
        private readonly IValidator<TrollModel> _trollValidator;
        private readonly IRepository<yk_troll_comment> _trollCommentRepository;
        private readonly IRepository<yk_troll_user_like> _trollUserLikeRepository;
        private readonly IRepository<yk_troll_comment_reply> _commentReplyRepository;
        private readonly IRepository<yk_troll_comment_vote> _commentVoteRepository;

        public TrollService(
            IRepository<yk_troll> trollRepository,
            IValidator<TrollModel> trollValidator,
            IRepository<yk_troll_comment> trollCommentRepository,
            IRepository<yk_troll_user_like> trollUserLikeRepository,
            IRepository<yk_troll_comment_reply> commentReplyRepository,
            IRepository<yk_troll_comment_vote> commentVoteRepository)
        {
            _trollRepository = trollRepository;
            _trollValidator = trollValidator;
            _trollCommentRepository = trollCommentRepository;
            _trollUserLikeRepository = trollUserLikeRepository;
            _commentReplyRepository = commentReplyRepository;
            _commentVoteRepository = commentVoteRepository;
        }

        private TrollModel BuildModel(yk_troll troll)
        {
            var trollModel = troll.TransformTo<TrollModel>();
            trollModel.UserName = troll.User != null ? troll.User.Username : "";
            trollModel.Comments = new List<TrollCommentModel>();
            trollModel.UserLikes = new List<string>();
            var comments = troll.Comments.Where(e => e.IsActive)
                .OrderByDescending(e => e.CreatedTime)
                .ToList();
            comments.ForEach(c =>
            {
                var comment = BuildComment(c);
                trollModel.Comments.Add(comment);
            });
            var userLikes = troll.UserLikes.Where(e => e.IsActive).ToList();
            userLikes.ForEach(u =>
            {
                trollModel.UserLikes.Add(u.User != null ? u.User.Username : "");
            });
            return trollModel;
        }

        public TrollModel GetSingleTroll(long trollId)
        {
            var troll = _trollRepository.GetActive()
                .FirstOrDefault(e => e.TrollId == trollId);
            return BuildModel(troll);
        }

        public List<TrollModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var trolls = _trollRepository.GetActive();
            var queryResult = BaseQueryExpression<yk_troll>.QueryResult(trolls, ref queryModel);

            return queryResult.ToList().Select(BuildModel).ToList();
        }

        public List<TrollModel> GetNewestTrolls(int? limit = null, long? excludeTrollId = null)
        {
            var trolls = _trollRepository.GetActive().OrderByDescending(e => e.CreatedTime).AsQueryable();
            if (excludeTrollId > 0)
            {
                trolls = trolls.Where(e => e.TrollId != excludeTrollId);
            }
            trolls = limit > 0 ? trolls.Take((int)limit) : trolls.Take(5);

            return trolls.ToList().Select(BuildModel).ToList();
        }

        public List<TrollModel> GetTopTrolls(int? limit = null)
        {
            var trolls = _trollRepository.GetActive()
                .OrderByDescending(e => e.UserLikes.Count)
                .AsQueryable();

            trolls = limit > 0 ? trolls.Take((int)limit) : trolls.Take(5);

            return trolls.ToList().Select(BuildModel).ToList();
        }

        public IOperationInfo Save(TrollModel dataModel)
        {
            var info = _trollValidator.GenerateOperationInfo();
            try
            {
                info = _trollValidator.Validate(dataModel, info);

                var post = dataModel.TrollId > 0
                    ? _trollRepository.Query().FirstOrDefault(e => e.TrollId == dataModel.TrollId)
                    : new yk_troll();

                post = dataModel.MapExcludeKeyTo(post);
                _trollRepository.Save(post);
                _trollRepository.Commit();

                info.AttachData("troll", post.TransformTo<TrollModel>());
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo SaveComment(TrollCommentModel dataModel)
        {
            var info = new OperationInfo();
            try
            {
                var comment = new yk_troll_comment()
                {
                    UserId = dataModel.UserId,
                    TrollId = dataModel.TrollId,
                    Content = dataModel.Content
                };
                _trollCommentRepository.Save(comment);
                _trollCommentRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo LikeTroll(long? trollId, long? userId)
        {
            var info = new OperationInfo();
            try
            {
                var like = _trollUserLikeRepository.GetActive()
                    .FirstOrDefault(e => e.TrollId == trollId && e.UserId == userId) ?? new yk_troll_user_like();

                like.TrollId = trollId;
                like.UserId = userId;

                _trollUserLikeRepository.Save(like);
                _trollUserLikeRepository.Commit();

                info.AttachData("troll", GetSingleTroll((long)trollId));

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public List<TrollCommentModel> GetComments(long trollId)
        {
            var troll = _trollRepository.GetActive()
                .First(e => e.TrollId == trollId);

            var result = new List<TrollCommentModel>();
            var comments = troll.Comments.Where(e => e.IsActive)
                .OrderByDescending(e => e.CreatedTime)
                .ToList();
            comments.ForEach(c =>
            {
                var comment = c.TransformTo<TrollCommentModel>();
                comment.Username = c.User != null ? c.User.Username : "";
                result.Add(comment);
            });

            return result;
        }

        public TrollCommentModel GetComment(long commentId)
        {
            var comment = _trollCommentRepository.GetActive()
                .First(e => e.TrollCommentId == commentId);
            return BuildComment(comment);
        }

        private TrollCommentModel BuildComment(yk_troll_comment c)
        {
            var data = c.TransformTo<TrollCommentModel>();
            data.CommentId = c.TrollCommentId;
            data.Username = c.User != null ? c.User.Username : "";
            data.UpVotes = c.CommentVotes.Where(e => e.IsActive && e.IsUpVote).Count();
            data.DownVotes = c.CommentVotes.Where(e => e.IsActive && !e.IsUpVote).Count();
            data.Replies = GetReplies(c.TrollCommentId);
            data.UserAvatar = c.User!=null ? c.User.UserAvatar : null;
            return data;
        }

        public List<CommentReplyModel> GetReplies(long? commentId)
        {
            var data = _commentReplyRepository.GetActive()
                .Where(e => e.CommentId == commentId)
                .OrderByDescending(e => e.CreatedTime)
                .Include(e => e.User)
                .ToList();
            return data.Select(BuildReply).ToList();
        }

        public IOperationInfo VoteComment(long commentId, long userId, bool isUpVote)
        {
            var info = new OperationInfo();
            try
            {
                var commentVote = _commentVoteRepository.GetActive()
                    .FirstOrDefault(e => e.CommentId == commentId && e.UserId == userId) ??
                                  new yk_troll_comment_vote();

                commentVote.CommentId = commentId;
                commentVote.UserId = userId;
                commentVote.IsUpVote = isUpVote;

                _commentVoteRepository.Save(commentVote);
                _commentVoteRepository.Commit();

                var comment = GetComment(commentId);
                info.datas.Add("comment", comment);
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        private CommentReplyModel BuildReply(yk_troll_comment_reply entity)
        {
            var model = entity.TransformTo<CommentReplyModel>();
            model.Username = entity.User != null ? entity.User.Username : "";
            model.UserAvatar = entity.User != null ? entity.User.UserAvatar : null; 
            return model;
        }

        public IOperationInfo SaveReply(CommentReplyModel model)
        {
            var info = new OperationInfo();
            try
            {
                var data = new yk_troll_comment_reply()
                {
                    CommentId = model.CommentId,
                    UserId = model.UserId,
                    Content = model.Content
                };
                _commentReplyRepository.Save(data);
                _commentReplyRepository.Commit();

                var replies = GetReplies(model.CommentId);
                info.datas.Add("replies", replies);

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

    }
}
