﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Troll
{
    public class TrollValidator : BaseValidator<TrollModel>
    {
        private readonly IRepository<yk_troll> _trollRepository;

        public TrollValidator(
            IRepository<yk_troll> trollRepository )
        {
            _trollRepository = trollRepository;
        }

        public override IOperationInfo Validate(TrollModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.Title))
            {
                info.AttachValidation("Title", "Title cannot be empty");
            }

            if (String.IsNullOrEmpty(dataModel.ContentUrl))
            {
                info.AttachValidation("Content", "Content cannot be empty");
            }

            return info;
        }
    }
}
