﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Country
{
    public class CountryService : ICountryService
    {
        private readonly IRepository<yk_country> _countryRepository;
        private readonly IValidator<CountryModel> _countryValidator;

        public CountryService(
            IRepository<yk_country> countryRepository,
            IValidator<CountryModel> countryValidator)
        {
            _countryRepository = countryRepository;
            _countryValidator = countryValidator;
        }

        public List<CountryModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var countries = _countryRepository.GetActive()
                .QueryResult(ref queryModel)
                .ToList();

            return countries.Select(e => e.TransformTo<CountryModel>()).ToList();
        }

        public List<CountryModel> GetAll()
        {
            var countries = _countryRepository.GetActive()
                .ToList();
            return countries.Select(e => e.TransformTo<CountryModel>()).ToList();
        }

        public IOperationInfo Save(CountryModel dataModel)
        {
            var info = _countryValidator.GenerateOperationInfo();
            try
            {
                info = _countryValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.CountryId > 0
                        ? _countryRepository.Query().First(e => e.CountryId == dataModel.CountryId)
                        : new yk_country();

                    course = dataModel.MapExcludeKeyTo(course);
                    _countryRepository.Save(course);
                    _countryRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long countryId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _countryRepository.Query().FirstOrDefault(e => e.CountryId == countryId);
                if (course != null)
                {
                    _countryRepository.SoftDelete(course);
                    _countryRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
