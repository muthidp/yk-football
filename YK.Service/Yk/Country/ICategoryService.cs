﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Country
{
    public interface ICountryService
    {
        List<CountryModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<CountryModel> GetAll();
        IOperationInfo Save(CountryModel dataModel);
        IOperationInfo Delete(long countryId);
    }
}
