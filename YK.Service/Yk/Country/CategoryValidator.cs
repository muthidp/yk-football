﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Country
{
    public class CountryValidator : BaseValidator<CountryModel>
    {
        private readonly IRepository<yk_country> _countryRepository;

        public CountryValidator(
            IRepository<yk_country> countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public override IOperationInfo Validate(CountryModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.CountryCode))
            {
                info.AttachValidation("CountryCode", "Country code cannot be empty.");
            }
            if (String.IsNullOrEmpty(dataModel.CountryLabel))
            {
                info.AttachValidation("CountryLabel", "Country label cannot be empty.");
            }

            if (info.Valid())
            {
                var country = _countryRepository.GetActive()
                    .FirstOrDefault(e => e.CountryCode == dataModel.CountryCode);

                if (country != null && country.CountryCode != dataModel.CountryCode)
                {
                    info.AttachValidation("Duplicate", "Country code [" + dataModel.CountryCode + "] is already existed.");
                }
            }

            return info;
        }
    }
}
