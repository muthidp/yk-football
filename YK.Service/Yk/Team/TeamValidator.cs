﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Team
{
    public class TeamValidator : BaseValidator<TeamModel>
    {
        private readonly IRepository<yk_team> _TeamRepository;

        public TeamValidator(
            IRepository<yk_team> TeamRepository)
        {
            _TeamRepository = TeamRepository;
        }

        public override IOperationInfo Validate(TeamModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.TeamLabel))
            {
                info.AttachValidation("TeamLabel", "Team label cannot be empty.");
            }

            if (info.Valid())
            {
                var Team = _TeamRepository.GetActive()
                    .FirstOrDefault(e => e.TeamCode == dataModel.TeamCode);

                if (Team != null && Team.TeamCode != dataModel.TeamCode)
                {
                    info.AttachValidation("Duplicate", "Team code [" + dataModel.TeamCode + "] is already existed.");
                }

                var Team1 = _TeamRepository.GetActive()
                   .FirstOrDefault(e => e.TeamLabel == dataModel.TeamLabel);

                if (Team1 != null && Team1.TeamLabel != dataModel.TeamLabel)
                {
                    info.AttachValidation("Duplicate", "Team label [" + dataModel.TeamLabel + "] is already existed.");
                }
            }

            return info;
        }
    }
}
