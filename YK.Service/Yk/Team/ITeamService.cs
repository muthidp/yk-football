﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Team
{
    public interface ITeamService
    {
        List<TeamModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<TeamModel> GetAll();
        IOperationInfo Save(TeamModel dataModel);
        IOperationInfo Delete(long TeamId);
        List<TeamModel> GetTeamsToShowInMenu();
    }
}
