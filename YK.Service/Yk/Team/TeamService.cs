﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Team
{
    public class TeamService : ITeamService
    {
        private readonly IRepository<yk_team> _TeamRepository;
        private readonly IValidator<TeamModel> _TeamValidator;
        private readonly IRepository<yk_league> _LeagueRepository;

        public TeamService(
            IRepository<yk_team> TeamRepository,
            IValidator<TeamModel> TeamValidator,
            IRepository<yk_league> LeagueRepository)
        {
            _TeamRepository = TeamRepository;
            _TeamValidator = TeamValidator;
            _LeagueRepository = LeagueRepository;
        }

        public List<TeamModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var teams = _TeamRepository.GetActive()
                .QueryResult(ref queryModel)
                .ToList();

            List<TeamModel> info = new List<TeamModel>();

            teams.ForEach(t =>
            {
                TeamModel te = t.TransformTo<TeamModel>();
                var league = _LeagueRepository.GetActive().Where(l => l.LeagueId == te.LeagueId)
                  .FirstOrDefault();

                if (league != null)
                    te.LeagueLabel = league.LeagueLabel;
                else
                    te.LeagueLabel = "";

                info.Add(te);
            });

            return info;
        }

        public List<TeamModel> GetAll()
        {
            var teams = _TeamRepository.GetActive()
                .ToList();

            List<TeamModel> info = new List<TeamModel>();

            teams.ForEach(t =>
            {
                TeamModel te = t.TransformTo<TeamModel>();
                var league = _LeagueRepository.GetActive().Where(l => l.LeagueId == te.LeagueId)
                  .FirstOrDefault();

                if (league != null)
                    te.LeagueLabel = league.LeagueLabel;
                else
                    te.LeagueLabel = "";

                info.Add(te);
            });

            return info;
        }

        public IOperationInfo Save(TeamModel dataModel)
        {
            var info = _TeamValidator.GenerateOperationInfo();
            try
            {
                info = _TeamValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.TeamId > 0
                        ? _TeamRepository.Query().First(e => e.TeamId == dataModel.TeamId)
                        : new yk_team();

                    course = dataModel.MapExcludeKeyTo(course);
                    _TeamRepository.Save(course);
                    _TeamRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long TeamId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _TeamRepository.Query().FirstOrDefault(e => e.TeamId == TeamId);
                if (course != null)
                {
                    _TeamRepository.SoftDelete(course);
                    _TeamRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public List<TeamModel> GetTeamsToShowInMenu()
        {
            var teams = _TeamRepository.GetActive()
                .Where(t => t.ShowInMenu)
                .ToList();

            List<TeamModel> info = new List<TeamModel>();

            teams.ForEach(t =>
            {
                TeamModel te = t.TransformTo<TeamModel>();
                var league = _LeagueRepository.GetActive().Where(l => l.LeagueId == te.LeagueId)
                  .FirstOrDefault();

                if (league != null)
                    te.LeagueLabel = league.LeagueLabel;
                else
                    te.LeagueLabel = "";

                info.Add(te);
            });

            return info;
        }

    }
}
