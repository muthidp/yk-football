﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Post
{
    public class PostValidator : BaseValidator<PostModel>
    {
        private readonly IRepository<yk_post> _postRepository;

        public PostValidator(
            IRepository<yk_post> postRepository )
        {
            _postRepository = postRepository;
        }

        public override IOperationInfo Validate(PostModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.Title))
            {
                info.AttachValidation("Title", "Title cannot be empty");
            }

            if (!(dataModel.CategoryId > 0))
            {
                info.AttachValidation("Category", "Category cannot be empty");
            }

            return info;
        }

    }
}
