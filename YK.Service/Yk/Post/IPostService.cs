﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Post
{
    public interface IPostService
    {
        List<PostModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<PostModel> GetNewestPosts(int? limit = null, long? excludePostId = null);
        HomeTimelineModel GetHomeTimeline();
        PostModel GetSinglePost(long postId);
        List<CommentModel> GetComments(long postId);
        IOperationInfo Save(PostModel dataModel);
        IOperationInfo SaveComment(CommentModel dataModel);
        List<PostModel> GetTimeline(long? leagueId, long? categoryId, long? teamId, long? countryId, ref BaseQueryModel queryModel);
        IOperationInfo VoteComment(long commentId, long userId, bool isUpVote);
        CommentModel GetComment(long commentId);
        IOperationInfo SaveReply(CommentReplyModel model);
        IOperationInfo Delete(long postId);
    }
}
