﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.WebSockets;
using Core;
using Core.DTO.Sys;
using Core.DTO.Yk;
using DAL.Database.Yk;
using NPOI.Util;

namespace YK.Service.Yk.Post
{
    public class PostService : IPostService
    {
        private readonly IRepository<yk_post> _postRepository;
        private readonly IValidator<PostModel> _postValidator;
        private readonly IRepository<yk_tag> _tagRepository;
        private readonly IRepository<yk_post_tag> _postTagRepository;
        private readonly IRepository<yk_comment> _commentRepository;
        private readonly IRepository<yk_league> _leagueRepository;
        private readonly IRepository<yk_country> _countryRepository;
        private readonly IRepository<yk_comment_vote> _commentVoteRepository;
        private readonly IRepository<yk_comment_reply> _commentReplyRepository;

        public PostService(
            IRepository<yk_post> postRepository,
            IValidator<PostModel> postValidator,
            IRepository<yk_tag> tagRepository,
            IRepository<yk_post_tag> postTagRepository,
            IRepository<yk_comment> commentRepository,
            IRepository<yk_league> leagueRepository,
            IRepository<yk_country> countryRepository,
            IRepository<yk_comment_vote> commentVoteRepository,
            IRepository<yk_comment_reply> commentReplyRepository )
        {
            _postRepository = postRepository;
            _postValidator = postValidator;
            _tagRepository = tagRepository;
            _postTagRepository = postTagRepository;
            _commentRepository = commentRepository;
            _leagueRepository = leagueRepository;
            _countryRepository = countryRepository;
            _commentVoteRepository = commentVoteRepository;
            _commentReplyRepository = commentReplyRepository;
        }

        public List<PostModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var posts = _postRepository.GetActive();
            var queryResult = BaseQueryExpression<yk_post>.QueryResult(posts, ref queryModel);

            return queryResult.ToList().Select(BuildModel).ToList();
        }

        public List<PostModel> GetNewestPosts(int? limit = null, long? excludePostId = null)
        {
            var posts = _postRepository.GetActive().OrderByDescending(e => e.CreatedTime).AsQueryable();
            if (excludePostId > 0)
            {
                posts = posts.Where(e => e.PostId != excludePostId);
            }
            posts = limit > 0 ? posts.Take((int) limit) : posts.Take(5);

            return posts.ToList().Select(BuildModel).ToList();
        }

        public HomeTimelineModel GetHomeTimeline()
        {
            var result = new HomeTimelineModel()
            {
                SectionOne = new List<CountryWithPosts>(),
                SectionTwo = new List<CountryWithPosts>(),
                SectionThree = new List<CountryWithPosts>()
            };

            _countryRepository.GetActive()
                .Where(e=> e.SectionInHomePage==1)
                .ToList()
                .ForEach(c =>
                {
                    var country = c.TransformTo<CountryModel>();
                    var posts = _postRepository.GetActive()
                        .Where(e => e.League.CountryId == c.CountryId)
                        .OrderByDescending(e => e.CreatedTime)
                        .Take(12)
                        .ToList()
                        .Select(BuildModel)
                        .ToList();

                    var countryWithPost = new CountryWithPosts()
                    {
                        Country = country,
                        Posts = posts
                    };
                    result.SectionOne.Add(countryWithPost);
                });

            _countryRepository.GetActive()
                .Where(e => e.SectionInHomePage == 2)
                .Take(3)
                .ToList()
                .ForEach(c =>
                {
                    var country = c.TransformTo<CountryModel>();
                    var posts = _postRepository.GetActive()
                        .Where(e => e.League.CountryId == c.CountryId)
                        .OrderByDescending(e => e.CreatedTime)
                        .Take(3)
                        .ToList()
                        .Select(BuildModel)
                        .ToList();

                    var countryWithPost = new CountryWithPosts()
                    {
                        Country = country,
                        Posts = posts
                    };
                    result.SectionTwo.Add(countryWithPost);
                });

            return result;
        }

        private PostModel BuildModel(yk_post post)
        {
            var postModel = post.TransformTo<PostModel>();
            postModel.AuthorName = post.Author != null ? post.Author.Username : "";
            postModel.CategoryLabel = post.Category != null ? post.Category.CategoryLabel : "";
            postModel.TeamLabel = post.Team != null ? post.Team.TeamLabel : "";
            postModel.LeagueLabel = post.League != null ? post.League.LeagueLabel : "";
            postModel.LeagueLogoUrl = post.League != null ? post.League.LogoUrl : "";
            return postModel;
        }


        public PostModel GetSinglePost(long postId)
        {
            ReadPost(postId);

            var post = _postRepository.GetActive().FirstOrDefault(e => e.PostId == postId);
            if (post == null)
                throw new Exception("Post not found.");

            var postModel = post.TransformTo<PostModel>();
            postModel.AuthorName = post.Author != null ? post.Author.Username : "";
            postModel.CategoryLabel = post.Category != null ? post.Category.CategoryLabel : "";
            postModel.TeamLabel = post.Team != null ? post.Team.TeamLabel : "";
            postModel.LeagueLabel = post.League != null ? post.League.LeagueLabel : "";
            postModel.LeagueLogoUrl = post.League != null ? post.League.LogoUrl : "";
            postModel.Tags = new List<string>();

            post.PostTags.Where(e => e.IsActive).ToList()
                .ForEach(pt =>
                {
                    postModel.Tags.Add(pt.Tag.TagLabel);
                });

            return postModel;
        }

        public void ReadPost(long postId)
        {
            var post = _postRepository.GetActive().FirstOrDefault(e => e.PostId == postId);
            if (post == null)
                throw new Exception("Post not found.");

            if (HttpContext.Current.User.Identity.Name != post.Author.Username)
                post.Read = post.Read + 1;

            _postRepository.Save(post);
            _postRepository.Commit();
        }

        public List<CommentModel> GetComments(long postId)
        {
            var post = _postRepository.GetActive().FirstOrDefault(e => e.PostId == postId);
            if (post == null)
                throw new Exception("Post not found.");

            var comments = _commentRepository.GetActive()
                .Where(e => e.PostId == postId)
                .OrderByDescending(e => e.CreatedTime)
                .ToList();
            var result = comments.Select(BuildComment).ToList();
            return result;
        }

        private CommentModel BuildComment(yk_comment c)
        {
            var data = c.TransformTo<CommentModel>();
            data.Username = c.User != null ? c.User.Username : "";
            data.UpVotes = c.CommentVotes.Where(e => e.IsActive && e.IsUpVote).Count();
            data.DownVotes = c.CommentVotes.Where(e => e.IsActive && !e.IsUpVote).Count();
            data.UserAvatar = c.User != null ? c.User.UserAvatar : null;
            data.Replies = GetReplies(c.CommentId);
            return data;
        }

        public IOperationInfo Save(PostModel dataModel)
        {
            var info = _postValidator.GenerateOperationInfo();
            try
            {
                info = _postValidator.Validate(dataModel, info);

                var post = dataModel.PostId > 0
                    ? _postRepository.Query().FirstOrDefault(e => e.PostId == dataModel.PostId)
                    : new yk_post();

                post = dataModel.MapExcludeKeyTo(post);
                _postRepository.Save(post);
                _postRepository.Commit();

                var postTags = _postTagRepository.Query().Where(e => e.PostId == dataModel.PostId).ToList();
                _postTagRepository.BulkDelete(postTags);
                _postTagRepository.Commit();

                if (dataModel.Tags != null && dataModel.Tags.Any())
                {
                    dataModel.Tags.ForEach(tagText =>
                    {
                        var tag = _tagRepository.GetActive().FirstOrDefault(e => e.TagLabel == tagText);
                        if (tag == null)
                        {
                            tag = new yk_tag()
                            {
                                TagLabel = tagText
                            };
                            _tagRepository.Save(tag);
                            _tagRepository.Commit(); ;
                        }

                        var postTag = new yk_post_tag()
                        {
                            PostId = post.PostId,
                            TagId = tag.TagId
                        };
                        _postTagRepository.Save(postTag);
                    });
                    _postTagRepository.Commit();
                }

                info.AttachData("post", post.TransformTo<PostModel>());
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo SaveComment(CommentModel dataModel)
        {
            var info = new OperationInfo();
            try
            {
                var comment = new yk_comment()
                {
                    UserId = dataModel.UserId,
                    PostId = dataModel.PostId,
                    Content = dataModel.Content
                };
                _commentRepository.Save(comment);
                _commentRepository.Commit();

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public List<PostModel> GetTimeline(long? leagueId, long? categoryId, long? teamId, long? countryId, ref BaseQueryModel queryModel)
        {
            var posts = _postRepository.GetActive();

            if (leagueId > 0)
            {
                posts = posts.Where(e => e.LeagueId == leagueId);
            }
            if (categoryId > 0)
            {
                posts = posts.Where(e => e.CategoryId == categoryId);
            }
            if (teamId > 0)
            {
                posts = posts.Where(e => e.TeamId == teamId);
            }
            if (countryId > 0)
            {
                posts = posts.Where(e => e.League.CountryId == countryId);
            }
            var queryResult = BaseQueryExpression<yk_post>.QueryResult(posts, ref queryModel);
            return queryResult.ToList().Select(BuildModel).ToList();
        }

        public IOperationInfo VoteComment(long commentId, long userId, bool isUpVote)
        {
            var info = new OperationInfo();
            try
            {
                var commentVote = _commentVoteRepository.GetActive()
                    .FirstOrDefault(e => e.CommentId == commentId && e.UserId == userId) ??
                                  new yk_comment_vote();

                commentVote.CommentId = commentId;
                commentVote.UserId = userId;
                commentVote.IsUpVote = isUpVote;

                _commentVoteRepository.Save(commentVote);
                _commentVoteRepository.Commit();

                var comment = GetComment(commentId);
                info.datas.Add("comment", comment);
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public CommentModel GetComment(long commentId)
        {
            var comment = _commentRepository.GetActive()
                .First(e => e.CommentId == commentId);
            return BuildComment(comment);
        }

        public List<CommentReplyModel> GetReplies(long? commentId)
        {
            var data = _commentReplyRepository.GetActive()
                .Where(e => e.CommentId == commentId)
                .OrderByDescending(e=> e.CreatedTime)
                .ToList();
            return data.Select(BuildReply).ToList();
        }

        private CommentReplyModel BuildReply(yk_comment_reply entity)
        {
            var model = entity.TransformTo<CommentReplyModel>();
            model.Username = entity.User != null ? entity.User.Username : "";
            model.UserAvatar = entity.User != null ? entity.User.UserAvatar : null;
            return model;
        }

        public IOperationInfo SaveReply(CommentReplyModel model)
        {
            var info = new OperationInfo();
            try
            {
                var data = new yk_comment_reply()
                {
                    CommentId = model.CommentId,
                    UserId = model.UserId,
                    Content = model.Content
                };
                _commentReplyRepository.Save(data);
                _commentReplyRepository.Commit();

                var replies = GetReplies(model.CommentId);
                info.datas.Add("replies", replies);

                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long postId)
        {
            var info = new OperationInfo();
            try
            {
                var post = _postRepository.GetActive().FirstOrDefault(e => e.PostId == postId);
                if (post != null)
                {
                    _postRepository.SoftDelete(post);
                    _postRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
