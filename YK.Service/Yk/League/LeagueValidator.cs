﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.League
{
    public class LeagueValidator : BaseValidator<LeagueModel>
    {
        private readonly IRepository<yk_league> _LeagueRepository;

        public LeagueValidator(
            IRepository<yk_league> LeagueRepository)
        {
            _LeagueRepository = LeagueRepository;
        }

        public override IOperationInfo Validate(LeagueModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.LeagueLabel))
            {
                info.AttachValidation("LeagueLabel", "League label cannot be empty.");
            }

            if (info.Valid())
            {
                var League = _LeagueRepository.GetActive()
                    .FirstOrDefault(e => e.LeagueCode == dataModel.LeagueCode);

                if (League != null && League.LeagueCode != dataModel.LeagueCode)
                {
                    info.AttachValidation("Duplicate", "League code [" + dataModel.LeagueCode + "] is already existed.");
                }

                var League1 = _LeagueRepository.GetActive()
                   .FirstOrDefault(e => e.LeagueLabel == dataModel.LeagueLabel);

                if (League1 != null && League1.LeagueLabel != dataModel.LeagueLabel)
                {
                    info.AttachValidation("Duplicate", "League label [" + dataModel.LeagueLabel + "] is already existed.");
                }
            }

            return info;
        }
    }
}
