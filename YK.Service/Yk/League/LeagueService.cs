﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.League
{
    public class LeagueService : ILeagueService
    {
        private readonly IRepository<yk_league> _leagueRepository;
        private readonly IValidator<LeagueModel> _leagueValidator;

        public LeagueService(
            IRepository<yk_league> leagueRepository,
            IValidator<LeagueModel> leagueValidator)
        {
            _leagueRepository = leagueRepository;
            _leagueValidator = leagueValidator;
        }

        public List<LeagueModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var leagues = _leagueRepository.GetActive();
            var queryResult = BaseQueryExpression<yk_league>.QueryResult(leagues, ref queryModel);

            return queryResult.ToList().Select(BuildModel).ToList();
        }

        public List<LeagueModel> GetAll()
        {
            var leagues = _leagueRepository.GetActive()
                .ToList();
            return leagues.Select(BuildModel).ToList();
        }

        private LeagueModel BuildModel(yk_league league)
        {
            var data = league.TransformTo<LeagueModel>();
            data.CountryLabel = league.Country != null ? league.Country.CountryLabel : "";
            return data;
        }

        public LeagueModel GetOne(long? leagueId)
        {
            if (!(leagueId > 0)) return null;

            var league = _leagueRepository.GetActive()
                .FirstOrDefault(e=> e.LeagueId==leagueId);

            return league == null ? null : league.TransformTo<LeagueModel>();
        }

        public IOperationInfo Save(LeagueModel dataModel)
        {
            var info = _leagueValidator.GenerateOperationInfo();
            try
            {
                info = _leagueValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.LeagueId > 0
                        ? _leagueRepository.Query().First(e => e.LeagueId == dataModel.LeagueId)
                        : new yk_league();

                    course = dataModel.MapExcludeKeyTo(course);
                    _leagueRepository.Save(course);
                    _leagueRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long leagueId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _leagueRepository.Query().FirstOrDefault(e => e.LeagueId == leagueId);
                if (course != null)
                {
                    _leagueRepository.SoftDelete(course);
                    _leagueRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
