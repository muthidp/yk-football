﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.League
{
    public interface ILeagueService
    {
        List<LeagueModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<LeagueModel> GetAll();
        LeagueModel GetOne(long? leagueId);
        IOperationInfo Save(LeagueModel dataModel);
        IOperationInfo Delete(long leagueId);
    }
}
