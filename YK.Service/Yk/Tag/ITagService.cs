﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Tag
{
    public interface ITagService
    {
        List<TagModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<TagModel> GetAll();
        IOperationInfo Save(TagModel dataModel);
        IOperationInfo Delete(long TagId);
    }
}
