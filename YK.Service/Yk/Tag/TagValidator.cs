﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Tag
{
    public class TagValidator : BaseValidator<TagModel>
    {
        private readonly IRepository<yk_tag> _TagRepository;

        public TagValidator(
            IRepository<yk_tag> TagRepository)
        {
            _TagRepository = TagRepository;
        }

        public override IOperationInfo Validate(TagModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.TagLabel))
            {
                info.AttachValidation("TagLabel", "Tag label cannot be empty.");
            }

            if (info.Valid())
            {
                var tag = _TagRepository.GetActive()
                    .FirstOrDefault(e => e.TagLabel == dataModel.TagLabel);

                if (tag != null && tag.TagLabel != dataModel.TagLabel)
                {
                    info.AttachValidation("Duplicate", "Tag label [" + dataModel.TagLabel + "] is already existed.");
                }
            }

            return info;
        }
    }
}
