﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Tag
{
    public class TagService : ITagService
    {
        private readonly IRepository<yk_tag> _TagRepository;
        private readonly IValidator<TagModel> _TagValidator;

        public TagService(
            IRepository<yk_tag> TagRepository,
            IValidator<TagModel> TagValidator)
        {
            _TagRepository = TagRepository;
            _TagValidator = TagValidator;
        }

        public List<TagModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var categories = _TagRepository.GetActive()
                .QueryResult(ref queryModel)
                .ToList();

            return categories.Select(e => e.TransformTo<TagModel>()).ToList();
        }

        public List<TagModel> GetAll()
        {
            var categories = _TagRepository.GetActive()
                .ToList();
            return categories.Select(e => e.TransformTo<TagModel>()).ToList();
        }

        public IOperationInfo Save(TagModel dataModel)
        {
            var info = _TagValidator.GenerateOperationInfo();
            try
            {
                info = _TagValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.TagId > 0
                        ? _TagRepository.Query().First(e => e.TagId == dataModel.TagId)
                        : new yk_tag();

                    course = dataModel.MapExcludeKeyTo(course);
                    _TagRepository.Save(course);
                    _TagRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long TagId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _TagRepository.Query().FirstOrDefault(e => e.TagId == TagId);
                if (course != null)
                {
                    _TagRepository.SoftDelete(course);
                    _TagRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
