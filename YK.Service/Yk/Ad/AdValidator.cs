﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Ad
{
    public class AdValidator : BaseValidator<AdModel>
    {
        private readonly IRepository<yk_ad> _AdRepository;

        public AdValidator(
            IRepository<yk_ad> AdRepository)
        {
            _AdRepository = AdRepository;
        }

        public override IOperationInfo Validate(AdModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.AdIdentifier))
            {
                info.AttachValidation("AdIdentifier", "Ad identifier cannot be empty.");
            }

            if (info.Valid())
            {
                var Ad = _AdRepository.GetActive()
                    .FirstOrDefault(e => e.AdIdentifier == dataModel.AdIdentifier);

                if (Ad != null && Ad.AdIdentifier != dataModel.AdIdentifier)
                {
                    info.AttachValidation("Duplicate", "Ad identifier [" + dataModel.AdIdentifier + "] is already existed.");
                }
            }

            return info;
        }
    }
}
