﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Ad
{
    public interface IAdService
    {
        List<AdModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<AdModel> GetAll();
        IOperationInfo Save(AdModel dataModel);
        IOperationInfo Delete(long AdId);
    }
}
