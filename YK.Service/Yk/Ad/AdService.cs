﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Ad
{
    public class AdService : IAdService
    {
        private readonly IRepository<yk_ad> _AdRepository;
        private readonly IValidator<AdModel> _AdValidator;

        public AdService(
            IRepository<yk_ad> AdRepository,
            IValidator<AdModel> AdValidator)
        {
            _AdRepository = AdRepository;
            _AdValidator = AdValidator;
        }

        public List<AdModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var Ads = _AdRepository.GetActive()
                .QueryResult(ref queryModel)
                .ToList();

            return Ads.Select(e => e.TransformTo<AdModel>()).ToList();
        }

        public List<AdModel> GetAll()
        {
            var Ads = _AdRepository.GetActive()
                .ToList();
            return Ads.Select(e => e.TransformTo<AdModel>()).ToList();
        }

        public IOperationInfo Save(AdModel dataModel)
        {
            var info = _AdValidator.GenerateOperationInfo();
            try
            {
                info = _AdValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.AdId > 0
                        ? _AdRepository.Query().First(e => e.AdId == dataModel.AdId)
                        : new yk_ad();

                    course = dataModel.MapExcludeKeyTo(course);
                    _AdRepository.Save(course);
                    _AdRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long AdId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _AdRepository.Query().FirstOrDefault(e => e.AdId == AdId);
                if (course != null)
                {
                    _AdRepository.SoftDelete(course);
                    _AdRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
