﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Category
{
    public class CategoryValidator : BaseValidator<CategoryModel>
    {
        private readonly IRepository<yk_category> _categoryRepository;

        public CategoryValidator(
            IRepository<yk_category> categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public override IOperationInfo Validate(CategoryModel dataModel, IOperationInfo info)
        {
            if (String.IsNullOrEmpty(dataModel.CategoryCode))
            {
                info.AttachValidation("CategoryCode", "Category code cannot be empty.");
            }
            if (String.IsNullOrEmpty(dataModel.CategoryLabel))
            {
                info.AttachValidation("CategoryLabel", "Category label cannot be empty.");
            }

            if (info.Valid())
            {
                var category = _categoryRepository.GetActive()
                    .FirstOrDefault(e => e.CategoryCode == dataModel.CategoryCode);

                if (category != null && category.CategoryCode != dataModel.CategoryCode)
                {
                    info.AttachValidation("Duplicate", "Category code [" + dataModel.CategoryCode + "] is already existed.");
                }
            }

            return info;
        }
    }
}
