﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;
using DAL.Database.Yk;

namespace YK.Service.Yk.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<yk_category> _categoryRepository;
        private readonly IValidator<CategoryModel> _categoryValidator;
        private readonly IRepository<yk_team> _teamRepository;

        public CategoryService(
            IRepository<yk_category> categoryRepository,
            IValidator<CategoryModel> categoryValidator,
            IRepository<yk_team> teamRepository)
        {
            _categoryRepository = categoryRepository;
            _categoryValidator = categoryValidator;
            _teamRepository = teamRepository;
        }

        public List<CategoryModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel)
        {
            var categories = _categoryRepository.GetActive()
                .QueryResult(ref queryModel)
                .ToList();

            return categories.Select(e => e.TransformTo<CategoryModel>()).ToList();
        }

        public List<CategoryModel> GetAll()
        {
            var categories = _categoryRepository.GetActive()
                .ToList();
            return categories.Select(e => e.TransformTo<CategoryModel>()).ToList();
        }


        public List<CategoryMenuModel> GetAllForMenu()
        {
            List<CategoryMenuModel> info = new List<CategoryMenuModel>();
            var categories = _categoryRepository.GetActive()
                .ToList();

            categories.ForEach(c =>
            {
                CategoryMenuModel model = c.TransformTo<CategoryMenuModel>();
                info.Add(model);
            });


            var teams = _teamRepository.GetActive().Where(t => t.ShowInMenu).ToList();

            teams.ForEach(c =>
            {
                CategoryMenuModel model = c.TransformTo<CategoryMenuModel>();
                model.CategoryLabel = c.TeamLabel;
                info.Add(model);
            });

            return info;
        }

        public IOperationInfo Save(CategoryModel dataModel)
        {
            var info = _categoryValidator.GenerateOperationInfo();
            try
            {
                info = _categoryValidator.Validate(dataModel, info);
                if (info.Valid())
                {
                    var course = dataModel.CategoryId > 0
                        ? _categoryRepository.Query().First(e => e.CategoryId == dataModel.CategoryId)
                        : new yk_category();

                    course = dataModel.MapExcludeKeyTo(course);
                    _categoryRepository.Save(course);
                    _categoryRepository.Commit();

                    info.Success();
                }
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }

        public IOperationInfo Delete(long categoryId)
        {
            var info = new OperationInfo();
            try
            {
                var course = _categoryRepository.Query().FirstOrDefault(e => e.CategoryId == categoryId);
                if (course != null)
                {
                    _categoryRepository.SoftDelete(course);
                    _categoryRepository.Commit();
                }
                info.Success();
            }
            catch (Exception ex)
            {
                info.ExceptionOccurred().AttachException(ex);
            }
            return info;
        }
    }
}
