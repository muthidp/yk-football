﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.DTO.Yk;

namespace YK.Service.Yk.Category
{
    public interface ICategoryService
    {
        List<CategoryModel> GetAllWithBaseQuery(ref BaseQueryModel queryModel);
        List<CategoryModel> GetAll();
        IOperationInfo Save(CategoryModel dataModel);
        IOperationInfo Delete(long categoryId);
        List<CategoryMenuModel> GetAllForMenu();
    }
}
