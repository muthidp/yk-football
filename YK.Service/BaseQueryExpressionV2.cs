﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using Core;
using LinqKit;

namespace YK.Service
{
    public static class BaseQueryExpressionV2
    {
        public static IQueryable<BaseEntityModel> QueryResult(this IQueryable<BaseEntityModel> data, ref BaseQueryModel queryModel)
        {
            var result = data
                .DefaultSearchQueryable(ref queryModel)
                .DefaultSortQueryable(ref queryModel)
                .DefaultPaginateQueryable(ref queryModel);

            return result;
        }

        public static IQueryable<BaseEntityModel> DefaultSearchQueryable(this IQueryable<BaseEntityModel> data, ref BaseQueryModel searchQuery)
        {
            var query = "";
            if (searchQuery.Search == null)
            {
                searchQuery.TotalData = data.Count();
                searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
                return data;
            }
            if (String.IsNullOrEmpty(searchQuery.Search.Keyword))
            {
                searchQuery.TotalData = data.Count();
                searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
                return data;
            }
            var model = searchQuery;
            model.Search.Fields.ForEach(f =>
            {
                query = query + String.Format("{0}.ToString().ToLower().Contains(\"{1}\")", f, model.Search.Keyword.ToLower());
                query = query + " || ";
            });

            query = query.Substring(0, query.Length - 4);

            var searchExpression = query;
            var result = !String.IsNullOrEmpty(searchExpression) ? data.Where(searchExpression) : data;
            searchQuery.TotalData = result.Count();
            searchQuery.TotalPage = GetTotalPage(searchQuery.TotalData, searchQuery.RowPerPage);
            return result;
        }

        public static IQueryable<BaseEntityModel> DefaultSortQueryable(this IQueryable<BaseEntityModel> data, ref BaseQueryModel searchQuery)
        {
            if (!String.IsNullOrEmpty(searchQuery.SortBy))
            {
                return searchQuery.IsSortAsc ? data.OrderBy(searchQuery.SortBy + " ASC") : data.OrderBy(searchQuery.SortBy + " DESC");
            }
            return data;
        }

        public static IQueryable<BaseEntityModel> DefaultPaginateQueryable(this IQueryable<BaseEntityModel> data, ref BaseQueryModel searchQuery)
        { 
            var take = searchQuery.RowPerPage;
            var skip = (searchQuery.Page - 1) * take;
            return data.Skip(skip).Take(take);
        }

        private static int GetTotalPage(int totalData, int rowPerPage)
        {
            return (totalData/rowPerPage) + 1;
        }
    }
}
