﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class CategoryModel
    {
        public long CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryLabel { get; set; }
        
    }


    public class CategoryMenuModel
    {
        public long CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryLabel { get; set; }

        public long TeamId { get; set; }
        public string TeamLabel { get; set; }
    }
}
