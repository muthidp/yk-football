﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class TeamModel
    {
        public long TeamId { get; set; }
        public string TeamCode { get; set; }
        public string TeamLabel { get; set; }
        public string Description { get; set; }
        public string LogoUrl { get; set; }

        public long? LeagueId { get; set; }
        public string LeagueLabel { get; set; }
        public bool ShowInMenu { get; set; }
        public long? FootballApiId { get; set; }
    }
}
