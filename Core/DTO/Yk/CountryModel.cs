﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class CountryModel
    {
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryLabel { get; set; }
        public string LogoUrl { get; set; }
        public int SectionInHomePage { get; set; }
    }
}
