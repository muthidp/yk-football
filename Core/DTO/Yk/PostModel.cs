﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class PostModel
    {
        public long PostId { get; set; }
        public long? UserId { get; set; }
        public string AuthorName { get; set; }
        public long? CategoryId { get; set; }
        public string CategoryLabel { get; set; }
        public long? TeamId { get; set; }
        public string TeamLabel { get; set; }
        public long? LeagueId { get; set; }
        public string LeagueLabel { get; set; }
        public string LeagueLogoUrl { get; set; }
        public string PictureUrl { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Read { get; set; }
        public List<string> Tags { get; set; } 
        public DateTime? CreatedTime { get; set; }
    }

    public class CountryWithPosts
    {
        public CountryModel Country { get; set; }
        public List<PostModel> Posts { get; set; } 
    }

    public class HomeTimelineModel
    {
        public List<CountryWithPosts> SectionOne { get; set; }
        public List<CountryWithPosts> SectionTwo { get; set; }
        public List<CountryWithPosts> SectionThree { get; set; }
    }
}
