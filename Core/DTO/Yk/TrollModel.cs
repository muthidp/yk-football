﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class TrollModel
    {
        public long TrollId { get; set; }
        public string Title { get; set; }
        public string ContentUrl { get; set; }
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public int Likes { get; set; }
        public int Shares { get; set; }
        public DateTime? CreatedTime { get; set; }
        public List<TrollCommentModel> Comments { get; set; } 
        public List<string> UserLikes { get; set; } 
    }

    public class TrollCommentModel
    {
        public long? TrollId { get; set; }
        public long CommentId { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
        public string UserAvatar { get; set; }
        public string Content { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
        public List<CommentReplyModel> Replies { get; set; } 
    }
}
