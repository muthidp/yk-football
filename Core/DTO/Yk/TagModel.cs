﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class TagModel
    {
        public long TagId { get; set; }
        public string TagCode { get; set; }
        public string TagLabel { get; set; }
    }
}
