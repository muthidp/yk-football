﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class AdModel
    {
        public long AdId { get; set; }
        public string AdIdentifier { get; set; }
        public string LocationPage { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string ImageUrl { get; set; }
    }
}
