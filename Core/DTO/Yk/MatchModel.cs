﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class MatchModel
    {
        public long MatchId { get; set; }

        public long? FootballMatchId { get; set; }

        public DateTime? MatchDateTime { get; set; }

        public long? FootballlLocalTeamId { get; set; }
        public string FootballlLocalTeamName { get; set; }
        public string LocalTeamScore { get; set; }

        public long? FootballlVisitorTeamId { get; set; }
        public string FootballlLocalVisitorName { get; set; }
        public string VisitorTeamScore { get; set; }

        public string MatchStatus { get; set; }

        public string MatchEtScore { get; set; }
        public string MatchHtScore { get; set; }
        public string MatchFtScore { get; set; }

        public string MatchCommentaryAvailable { get; set; }
    }

    public class LeagueWithMatches : LeagueModel
    {
        public List<MatchModel> Matches { get; set; } 
    }

    public class MatchCommentModel
    {
        public long MatchCommentId { get; set; }
        public string Comment { get; set; }
    }

    public class MatchEventModel
    {
        public long? MatchId { get; set; }
        public long? FootballMatchEventId { get; set; }
        public long? FootballMatchId { get; set; }
        public string Type { get; set; }
        public int Minute { get; set; }
        public int? ExtraMinute { get; set; }
        public long? TeamId { get; set; }
        public long? FootballTeamId { get; set; }
        public string Player { get; set; }
        public string Assist { get; set; }
        public string Result { get; set; }
    }
}
