﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class LeagueModel
    {
        public long LeagueId { get; set; }
        public string LeagueCode { get; set; }
        public string LeagueLabel { get; set; }
        public string Description { get; set; }
        public string LogoUrl { get; set; }
        public long? CountryId { get; set; }
        public string CountryLabel { get; set; }
        public long? FootballApiId { get; set; }
        public bool IsCupCompetition { get; set; }
    }
}
