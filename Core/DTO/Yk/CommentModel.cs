﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Yk
{
    public class CommentModel
    {
        public long CommentId { get; set; }
        public long? PostId { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
        public string Content { get; set; }
        public DateTime? CreatedTime { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }
        public string UserAvatar { get; set; }
        public List<CommentReplyModel> Replies { get; set; } 
    }

    public class CommentReplyModel
    {
        public long? CommentId { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
        public string Content { get; set; }
        public string UserAvatar { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
