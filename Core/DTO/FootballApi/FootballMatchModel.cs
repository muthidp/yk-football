﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.FootballApi
{
    public class FootballMatchModel
    {
        public string commentary_available { get; set; }
        public string comp_id { get; set; }
        public string date { get; set; }
        public string et_score { get; set; }
        public string formatted_date { get; set; }
        public string ft_score { get; set; }
        public string ht_score { get; set; }
        public string id { get; set; }
        public string localteam_id { get; set; }
        public string localteam_name { get; set; }
        public string localteam_score { get; set; }
        public string season_beta { get; set; }
        public string static_id { get; set; }
        public string status { get; set; }
        public string time { get; set; }
        public string timer { get; set; }
        public string venue_beta { get; set; }
        public string venue_city_beta { get; set; }
        public string venue_id_beta { get; set; }
        public string visitorteam_id { get; set; }
        public string visitorteam_name { get; set; }
        public string visitorteam_score { get; set; }
        public string week_beta { get; set; }
    }

    public class FootballMatchEventModel
    {
        public string id { get; set; }
        public string type { get; set; }
        public string minute { get; set; }
        public string extra_min { get; set; }
        public string team { get; set; }
        public string player { get; set; }
        public string player_id { get; set; }
        public string assist { get; set; }
        public string assist_id { get; set; }
        public string result { get; set; }
    }
}
