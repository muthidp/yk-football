﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO.Sys
{
    public class UserModel
    {
        public long UserId { get; set; }
        public string UserUniqueId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public long? RoleId { get; set; }
        public string DefaultRoute { get; set; }
        public long? InstitutionId { get; set; }
        public bool Deactivated { get; set; }
        public string Password { get; set; }
        public string Provider { get; set; }
        public string UserAvatar { get; set; }
    }

    public class UserInfoModel : UserModel
    {
        public RoleModel Role { get; set; }
        public InstitutionModel Institution { get; set; }
        public List<string> Acls { get; set; } 
    }
}
