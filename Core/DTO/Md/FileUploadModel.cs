﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.DTO.Md
{
    public class FileUploadModel
    {
        public HttpPostedFileBase FileUploaded { get; set; }
        public long InstitutionId { get; set; }
    }
}
