﻿using System.Security.Cryptography;
using System.Text;

namespace Core.Helper
{
    public class EncryptionHelper
    {
        public static string GetMd5Hash(string input)
        {
            var md5Hash = MD5.Create();
            var data = md5Hash.ComputeHash(Encoding.Unicode.GetBytes(input));
            var sBuilder = new StringBuilder();
            foreach (var t in data)
            {
                sBuilder.Append(t.ToString("X2"));
            }
            return sBuilder.ToString();
        }
    }
}
