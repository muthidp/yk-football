﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class OperationInfo : IOperationInfo
    {
        public OperationInfo()
        {
            exceptions = new List<string>();
            datas = new Dictionary<string, dynamic>();
            validations = new Dictionary<string, object>();
        }

        public bool exceptionOccurred { get; set; }
        public List<string> exceptions { get; set; }

        public Dictionary<string, dynamic> datas { get; set; }

        public Dictionary<string, object> validations { get; set; }
        public bool success { get; set; }
        public bool executed { get; set; }

        public IOperationInfo AttachException(Exception ex)
        {
            exceptions.Add(ex.GetExceptionMessage());
            return this;
        }

        public void AttachData(string identifier, object data)
        {
            if (datas.ContainsKey(identifier))
                datas[identifier] = data;
            else
                datas.Add(identifier, data);
        }

        public object GetData(string identifier)
        {
            return datas.ContainsKey(identifier) ? datas[identifier] : null;
        }

        public bool Valid()
        {
            return validations.Count == 0;
        }

        public IOperationInfo Success()
        {
            success = true;
            return this;
        }

        public IOperationInfo Fail()
        {
            success = false;
            return this;
        }

        //public IOperationInfo Executed()
        //{
        //    executed = true;
        //    return this;
        //}

        //public IOperationInfo NotExecuted()
        //{
        //    executed = false;
        //    success = false;
        //    return this;
        //}

        public IOperationInfo ExceptionOccurred()
        {
            executed = false;
            success = false;
            exceptionOccurred = true;
            return this;
        }

        public IOperationInfo AttachValidation(string identifier, string messsage)
        {
            if (!validations.Any(e => e.Key.Equals(identifier)))
            {
                validations.Add(identifier, messsage);
            }
            return this;
        }

        public bool IsSuccess()
        {
            return success;
        }
    }
}
