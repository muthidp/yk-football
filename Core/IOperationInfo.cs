﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IOperationInfo
    {
        bool Valid();
        IOperationInfo Success();
        IOperationInfo Fail();
        //IOperationInfo Executed();
        //IOperationInfo NotExecuted();
        IOperationInfo ExceptionOccurred();
        IOperationInfo AttachValidation(string identifier, string message);
        IOperationInfo AttachException(Exception ex);
        bool IsSuccess();
        void AttachData(string identifier, object data);
        object GetData(string identifier);
    }
}
