﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class ValidationInfo : IValidationInfo
    {
        public string identifier { get; set; }
        public string message { get; set; }
        public string validationType { get; set; }

        public IValidationInfo Fail(string _identifier, string _message, string type = "VALIDATION_ERROR")
        {
            identifier = _identifier;
            message = _message;
            validationType = type;
            return this;
        }

        public static IValidationInfo Create(string _identifier, string _message, string type = "VALIDATION_ERROR")
        {
            var info = new ValidationInfo()
            {
                identifier = _identifier,
                message = _message,
                validationType = type
            };
            return info;
        }
    }
}
