﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface ISpWrapper
    {
        IEnumerable<TResult> ExecuteQueryStoredProcedure<TResult>(IStoredProcedure<TResult> procedure);
        int ExecuteNonQueryStoredProcedure(IStoredProcedure procedure);
    }
}
