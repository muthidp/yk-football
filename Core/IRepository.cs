﻿using System.Collections.Generic;
using System.Linq;


namespace Core
{
    public interface IRepository<TModel> where TModel : BaseEntityModel
    {
        void Save(TModel model);
        void BulkInsert(IEnumerable<TModel> range);
        void SoftDelete(TModel model);
        void Delete(TModel model);
        void BulkDelete(IEnumerable<TModel> model);
        IQueryable<TModel> Query();
        IQueryable<TModel> GetActive();
        void Commit();
        void SetDefaultProp(ref TModel model, bool isAdd);
    }
}
