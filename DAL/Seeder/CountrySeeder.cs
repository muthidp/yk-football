﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Yk;

namespace DAL.Seeder
{
    public class CountrySeeder : DataSeeder<Tuple<string, string, int>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, int>("England", "England", 1));
            Records.Add(new Tuple<string, string, int>("Germany", "Germany", 2));
            Records.Add(new Tuple<string, string, int>("Spain", "Spain", 2));
            Records.Add(new Tuple<string, string, int>("Italy", "Italy", 2));
            Records.Add(new Tuple<string, string, int>("Others", "Others", 3));
        }

        public override void Seed(YK_Database context)
        {
            Records.ForEach(r =>
            {
                var country = context.YkCountries.FirstOrDefault(e => e.CountryCode.Equals(r.Item1))
                           ?? new yk_country();

                country.CountryCode = r.Item1;
                country.CountryLabel = r.Item2;
                country.SectionInHomePage = r.Item3;

                country.IsActive = true;
                country.LastUpdatedBy = Constant.Seeder;
                country.LastUpdatedTime = DateTime.Now;

                if (country.CountryId > 0)
                {
                    context.Entry(country).State = EntityState.Modified;
                }
                else
                {
                    country.CreatedBy = Constant.Seeder;
                    country.CreatedTime = DateTime.Now;
                    context.YkCountries.Add(country);
                }
                context.SaveChanges();
            });
        }
    }
}
