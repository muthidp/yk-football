﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using Core.Enum;
using DAL.Database.Md;

namespace DAL.Seeder
{
    public class MediaDescriptorSeeder : DataSeeder<Tuple<string, string, MediaType, bool, string>, YK_Database>
    {
        public override void Set()
        {
            Records = new List<Tuple<string, string, MediaType, bool, string>>
                    {
                        new Tuple<string, string, MediaType, bool, string>("video/mp4", "MP4 Video", MediaType.Video, false,
                                                                        "/Content/Images/icon-video-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("audio/mp3", "MP3 Audio", MediaType.Audio, false,
                                                                        "/Content/Images/icon-music-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("image/png", "PNG Image", MediaType.Picture, true,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("image/jpeg", "JPEG Image", MediaType.Picture, true,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("image/jpg", "JPG Image", MediaType.Picture, true,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("image/tiff", "TIFF Image", MediaType.Picture, true,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("image/bmp", "BMP Image", MediaType.Picture, true,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("application/json", "JSON Data", MediaType.DataFormat, false,
                                                                        "/Content/Images/icon-script-thumbnails.png"),
                        new Tuple<string, string, MediaType, bool, string>("text/html", "HTML Page", MediaType.HtmlScript, false,
                                                                        "/Content/Images/icon-picture-thumbnails.jpg"),
                        new Tuple<string, string, MediaType, bool, string>("text/javascript", "Javascript File", MediaType.HtmlScript, false,
                                                                        "/Content/Images/icon-script-thumbnails.png"),
                        new Tuple<string, string, MediaType, bool, string>("text/css", "CSS File", MediaType.HtmlScript, false,
                                                                        "/Content/Images/icon-script-thumbnails.png"),
                    };
        }

        public override void Seed(YK_Database context)
        {
            context.MdMediaDescriptors.ToList().ForEach(md =>
            {
                md.IsActive = false;
                context.Entry(md).State = EntityState.Modified;
            });
            context.SaveChanges();

            Records.ForEach(k =>
                {
                    var mdd = context.MdMediaDescriptors.FirstOrDefault(e => e.MimeTypeDefinition == k.Item1) ??
                              new md_media_descriptor();

                    mdd.MimeTypeDefinition = k.Item1;
                    mdd.MediaDescriptorLabel = k.Item2;
                    mdd.MediaType = k.Item3;
                    mdd.Previewable = k.Item4;
                    mdd.DefaultThumbnailDisplay = k.Item5;
                    mdd.IsActive = true;

                    mdd.LastUpdatedBy = "__SEED_ENGINE__";
                    mdd.LastUpdatedTime = DateTime.Now;

                    if (mdd.MediaDescriptorId > 0)
                        context.Entry(mdd).State = EntityState.Modified;
                    else
                        context.MdMediaDescriptors.Add(mdd);
                });
            context.SaveChanges();
        }
    }
}
