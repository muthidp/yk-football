﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Yk;

namespace DAL.Seeder
{
    public class TeamSeeder : DataSeeder<Tuple<string, string, string, bool>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string, bool>("LEICESTER CITY FC", "LEICESTER CITY FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("TOTTENHAM HOTSPUR FC", "TOTTENHAM HOTSPUR FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("ARSENAL FC", "ARSENAL FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("MANCHESTER CITY FC", "MANCHESTER CITY FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("WEST HAM UNITED FC", "WEST HAM UNITED FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("MANCHESTER UNITED FC", "MANCHESTER UNITED FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("LIVERPOOL FC", "LIVERPOOL FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("STOKE CITY FC", "STOKE CITY FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("CHELSEA FC", "CHELSEA FC", "Premier League", true));
            Records.Add(new Tuple<string, string, string, bool>("EVERTON FC", "EVERTON FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("WEST BROMWICH ALBION FC", "WEST BROMWICH ALBION FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("WATFORD FC", "WATFORD FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("AFC BOURNEMOUTH", "AFC BOURNEMOUTH", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("CRYSTAL PALACE FC", "CRYSTAL PALACE FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("SWANSEA CITY AFC", "SWANSEA CITY AFC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("SUNDERLAND AFC", "SUNDERLAND AFC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("NORWICH CITY FC", "NORWICH CITY FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("NEWCASTLE UNITED FC", "NEWCASTLE UNITED FC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("SUNDERLAND AFC", "SUNDERLAND AFC", "Premier League", false));
            Records.Add(new Tuple<string, string, string, bool>("ASTON VILLA FC", "ASTON VILLA FC", "Premier League", false));

            Records.Add(new Tuple<string, string, string, bool>("FC BARCELONA", "FC BARCELONA", "Primera División", true));
            Records.Add(new Tuple<string, string, string, bool>("CLUB ATLÉTICO DE MADRID", "CLUB ATLÉTICO DE MADRID", "Primera División", true));
            Records.Add(new Tuple<string, string, string, bool>("REAL MADRID CLUB DE FÚTBOL", "REAL MADRID CLUB DE FÚTBOL", "Primera División", true));
            Records.Add(new Tuple<string, string, string, bool>("VILLARREAL CLUB DE FÚTBOL", "VILLARREAL CLUB DE FÚTBOL", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("SEVILLA FC", "SEVILLA FC", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("REAL CLUB CELTA DE VIGO", "REAL CLUB CELTA DE VIGO", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("ATHLETIC CLUB BILBAO", "ATHLETIC CLUB BILBAO", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("SD EIBAR", "SD EIBAR", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("VALENCIA CLUB DE FÚTBOL", "VALENCIA CLUB DE FÚTBOL", "Primera División", true));
            Records.Add(new Tuple<string, string, string, bool>("REAL BETIS BALOMPIÉ", "REAL BETIS BALOMPIÉ", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("REAL SOCIEDAD DE FÚTBOL", "REAL SOCIEDAD DE FÚTBOL", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("MÁLAGA CLUB DE FÚTBOL", "MÁLAGA CLUB DE FÚTBOL", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("REAL CLUB DEPORTIVO DE LA CORUÑA", "REAL CLUB DEPORTIVO DE LA CORUÑA", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("UD LAS PALMAS", "UD LAS PALMAS", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("REIAL CLUB DEPORTIU ESPANYOL", "REIAL CLUB DEPORTIU ESPANYOL", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("GETAFE CLUB DE FÚTBOL", "GETAFE CLUB DE FÚTBOL", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("RAYO VALLECANO", "RAYO VALLECANO", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("GRANADA CF", "GRANADA CF", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("REAL SPORTING DE GIJÓN", "REAL SPORTING DE GIJÓN", "Primera División", false));
            Records.Add(new Tuple<string, string, string, bool>("LEVANTE UD", "LEVANTE UD", "Primera División", false));

            Records.Add(new Tuple<string, string, string, bool>("JUVENTUS FC", "JUVENTUS FC", "Serie A", true));
            Records.Add(new Tuple<string, string, string, bool>("SSC NAPOLI", "SSC NAPOLI", "Serie A", true));
            Records.Add(new Tuple<string, string, string, bool>("AS ROMA", "AS ROMA", "Serie A", true));
            Records.Add(new Tuple<string, string, string, bool>("ACF FIORENTINA", "ACF FIORENTINA", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("FC INTERNAZIONALE MILANO", "FC INTERNAZIONALE MILANO", "Serie A", true));
            Records.Add(new Tuple<string, string, string, bool>("AC MILAN", "AC MILAN", "Serie A", true));
            Records.Add(new Tuple<string, string, string, bool>("US SASSUOLO CALCIO", "US SASSUOLO CALCIO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("SS LAZIO", "SS LAZIO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("BOLOGNA FC 1909", "BOLOGNA FC 1909", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("AC CHIEVO VERONA", "AC CHIEVO VERONA", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("EMPOLI FC", "EMPOLI FC", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("TORINO FC", "TORINO FC", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("UC SAMPDORIA", "UC SAMPDORIA", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("GENOA CFC", "GENOA CFC", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("ATALANTA BERGAMASCA CALCIO", "ATALANTA BERGAMASCA CALCIO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("UDINESE CALCIO", "UDINESE CALCIO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("US CITTÀ DI PALERMO", "US CITTÀ DI PALERMO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("FROSINONE CALCIO", "FROSINONE CALCIO", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("CARPI FC 1909", "CARPI FC 1909", "Serie A", false));
            Records.Add(new Tuple<string, string, string, bool>("HELLAS VERONA FC", "HELLAS VERONA FC", "Serie A", false));


            Records.Add(new Tuple<string, string, string, bool>("FC BAYERN MÜNCHEN", "FC BAYERN MÜNCHEN", "Bundesliga", true));
            Records.Add(new Tuple<string, string, string, bool>("BV BORUSSIA 09 DORTMUND", "BV BORUSSIA 09 DORTMUND", "Bundesliga", true));
            Records.Add(new Tuple<string, string, string, bool>("HERTHA BSC", "HERTHA BSC", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("FC SCHALKE 04", "FC SCHALKE 04", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("1. FSV MAINZ 05", "1. FSV MAINZ 05", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("BORUSSIA VFL MÖNCHENGLADBACH", "BORUSSIA VFL MÖNCHENGLADBACH", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("VFL WOLFSBURG", "VFL WOLFSBURG", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("TSV BAYER 04 LEVERKUSEN", "TSV BAYER 04 LEVERKUSEN", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("FC INGOLSTADT 04", "FC INGOLSTADT 04", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("VFB STUTTGART 1893", "VFB STUTTGART 1893", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("1. FC KÖLN", "1. FC KÖLN", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("HAMBURGER SV", "HAMBURGER SV", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("SV WERDER BREMEN", "SV WERDER BREMEN", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("FC AUGSBURG", "FC AUGSBURG", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("SV DARMSTADT 1898", "SV DARMSTADT 1898", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("EINTRACHT FRANKFURT", "EINTRACHT FRANKFURT", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("TSG 1899 HOFFENHEIM", "TSG 1899 HOFFENHEIM", "Bundesliga", false));
            Records.Add(new Tuple<string, string, string, bool>("HANNOVER 96", "HANNOVER 96", "Bundesliga", false));
        }

        public override void Seed(YK_Database context)
        {
            Records.ForEach(r =>
            {
                var Team = context.YkTeams.FirstOrDefault(e => e.TeamCode.Equals(r.Item1))
                           ?? new yk_team();

                Team.TeamCode = r.Item1;
                Team.TeamLabel = r.Item2;
                Team.ShowInMenu = r.Item4;

                var league = context.YkLeagues.FirstOrDefault(e => e.LeagueCode.Equals(r.Item3) && e.IsActive);
                
                if (league != null)
                    Team.LeagueId = league.LeagueId;

                Team.IsActive = true;
                Team.LastUpdatedBy = Constant.Seeder;
                Team.LastUpdatedTime = DateTime.Now;

                if (Team.TeamId > 0)
                {
                    context.Entry(Team).State = EntityState.Modified;
                }
                else
                {
                    Team.CreatedBy = Constant.Seeder;
                    Team.CreatedTime = DateTime.Now;
                    context.YkTeams.Add(Team);
                }
                context.SaveChanges();
            });
        }
    }
}
