﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Yk;

namespace DAL.Seeder
{
    public class LeagueSeeder : DataSeeder<Tuple<string, string, string, string, long?, bool>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string, string, long?, bool>("Premier League", "Premier League", "From England.", "England", 1204, false));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("Primera División", "Primera División", "From Spain. Previously known as La Liga.", "Spain", 1399, false));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("Serie A", "Serie A", "From Itali.", "Italy", 1269, false));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("Bundesliga", "Bundesliga", "From Germany.", "Germany", 1229, false));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("UEFA Champions League", "UEFA Champions League", "UEFA Champions League.", null, 1005, true));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("UEFA Europa League", "UEFA Europa League", "UEFA Europa League.", null, 1007, true));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("Copa Del Rey", "Copa Del Rey", "Copa Del Rey.", null, 1397, true));
            Records.Add(new Tuple<string, string, string, string, long?, bool>("FA Cup", "FA Cup", "FA Cup.", null, 1198, true));
        }

        public override void Seed(YK_Database context)
        {
            Records.ForEach(r =>
            {
                var league = context.YkLeagues.FirstOrDefault(e => e.LeagueCode.Equals(r.Item1))
                           ?? new yk_league();

                league.LeagueCode = r.Item1;
                league.LeagueLabel = r.Item2;
                league.Description = r.Item3;
                league.FootballApiId = r.Item5;
                league.IsCupCompetition = r.Item6;

                league.IsActive = true;
                league.LastUpdatedBy = Constant.Seeder;
                league.LastUpdatedTime = DateTime.Now;

                var country = context.YkCountries.FirstOrDefault(e => e.IsActive && e.CountryCode.Equals(r.Item4));
                if (country != null)
                {
                    league.CountryId = country.CountryId;
                }
                else
                {
                    league.CountryId = null;
                }

                if (league.LeagueId > 0)
                {
                    context.Entry(league).State = EntityState.Modified;
                }
                else
                {
                    league.CreatedBy = Constant.Seeder;
                    league.CreatedTime = DateTime.Now;
                    context.YkLeagues.Add(league);
                }
                context.SaveChanges();
            });
        }
    }
}
