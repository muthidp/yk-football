﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ModuleGroupSeeder : DataSeeder<Tuple<string, string, string, int>, YK_Database>
    {
        public override void Set()
        {

            Records.Add(new Tuple<string, string, string, int>("ပင္မစာမ်က္ႏွာ", "home", "", 1));
            Records.Add(new Tuple<string, string, string, int>("သတင္းမ်ား", "", "", 2));
            Records.Add(new Tuple<string, string, string, int>("ပြဲစဥ္မ်ား", "match", "", 3));
            Records.Add(new Tuple<string, string, string, int>("ဟာသရပ္ဝန္း", "", "", 4));
            Records.Add(new Tuple<string, string, string, int>("Master Data", "", "ACCESS_MASTER_DATA", 5));
        }

        public override void Seed(YK_Database context)
        {
            var acls = context.SysAcls.ToList();

            Records.ForEach(r =>
            {
                var mg = context.SysModuleGroups.FirstOrDefault(e => e.Label.Equals(r.Item1))
                         ?? new sys_module_group();

                mg.Label = r.Item1;
                mg.Route = r.Item2;

                if (String.IsNullOrEmpty(r.Item3))
                {
                    mg.AclId = null;
                }
                else
                {
                    var acl = acls.FirstOrDefault(e => e.Key.Equals(r.Item3));
                    if (acl != null)
                        mg.AclId = acl.AclId;  
                }
                

                mg.Sequence = r.Item4;

                mg.IsActive = true;
                mg.LastUpdatedBy = Constant.Seeder;
                mg.LastUpdatedTime = DateTime.Now;

                if (mg.ModuleGroupId > 0)
                {
                    context.Entry(mg).State = EntityState.Modified;
                }
                else
                {
                    mg.CreatedBy = Constant.Seeder;
                    mg.CreatedTime = DateTime.Now;
                    context.SysModuleGroups.Add(mg);
                }
            });
            context.SaveChanges();
        }
    }
}
