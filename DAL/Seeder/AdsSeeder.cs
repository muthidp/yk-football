﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Yk;

namespace DAL.Seeder
{
    public class AdsSeeder : DataSeeder<Tuple<string, string>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string>("Home1", "Home"));
            Records.Add(new Tuple<string, string>("Home2", "Home"));
            Records.Add(new Tuple<string, string>("Home3", "Home"));
            Records.Add(new Tuple<string, string>("Home4", "Home"));
            Records.Add(new Tuple<string, string>("PostDetail1", "PostDetail"));
            Records.Add(new Tuple<string, string>("PostDetail2", "PostDetail"));
            Records.Add(new Tuple<string, string>("PostDetail3", "PostDetail"));
            Records.Add(new Tuple<string, string>("Match1", "Match"));
            Records.Add(new Tuple<string, string>("Match2", "Match"));
            Records.Add(new Tuple<string, string>("Match3", "Match"));
            Records.Add(new Tuple<string, string>("LeagueTimeline1", "LeagueTimeline"));
            Records.Add(new Tuple<string, string>("LeagueTimeline2", "LeagueTimeline"));
            Records.Add(new Tuple<string, string>("LeagueTimeline3", "LeagueTimeline"));
            Records.Add(new Tuple<string, string>("LeagueTimeline4", "LeagueTimeline"));
            Records.Add(new Tuple<string, string>("TrollTimeline1", "TrollTimeline"));
            Records.Add(new Tuple<string, string>("TrollTimeline2", "TrollTimeline"));
            Records.Add(new Tuple<string, string>("TrollTimeline3", "TrollTimeline"));
            Records.Add(new Tuple<string, string>("SingleTroll1", "SingleTroll"));
            Records.Add(new Tuple<string, string>("SingleTroll2", "SingleTroll"));
            Records.Add(new Tuple<string, string>("SingleTroll3", "SingleTroll"));
        }

        public override void Seed(YK_Database context)
        {
            Records.ForEach(r =>
            {
                var Ads = context.YkAds.FirstOrDefault(e => e.AdIdentifier.Equals(r.Item1))
                           ?? new yk_ad();

                Ads.AdIdentifier = r.Item1;
                Ads.LocationPage = r.Item2;

                Ads.IsActive = true;
                Ads.LastUpdatedBy = Constant.Seeder;
                Ads.LastUpdatedTime = DateTime.Now;

                if (Ads.AdId > 0)
                {
                    context.Entry(Ads).State = EntityState.Modified;
                }
                else
                {
                    Ads.CreatedBy = Constant.Seeder;
                    Ads.CreatedTime = DateTime.Now;
                    context.YkAds.Add(Ads);
                }
                context.SaveChanges();
            });
        }
    }
}
