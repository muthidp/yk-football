﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class AclSeeder : DataSeeder<Tuple<string, string, List<string>>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA", "Access Master Data", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_COUNTRY", "Access Master Data Country", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_CATEGORY", "Access Master Data Category", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_LEAGUE", "Access Master Data League", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_TEAM", "Access Master Data Team", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_TAG", "Access Master Data Tag", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ACCESS_MASTER_DATA_ADV", "Access Master Data Adv", new List<string> { "ROOT" }));
            Records.Add(new Tuple<string, string, List<string>>("ADD_MATCH_COMMENT", "Add Match Comment", new List<string> { "ROOT" }));
        }

        public override void Seed(YK_Database context)
        {
            context.SysAcls.ToList().ForEach(e =>
            {
                e.IsActive = false;
                context.Entry(e).State = EntityState.Modified;
            });
            context.SaveChanges();

            var roles = context.SysRoles.Where(e => e.IsActive).ToList();

            Records.ForEach(r =>
            {
                var acl = context.SysAcls.FirstOrDefault(e => e.Key.Equals(r.Item1))
                          ?? new sys_acl();

                acl.Key = r.Item1;
                acl.Description = r.Item2;

                acl.IsActive = true;
                acl.LastUpdatedBy = Constant.Seeder;
                acl.LastUpdatedTime = DateTime.Now;

                if (acl.AclId > 0)
                {
                    context.Entry(acl).State = EntityState.Modified;
                    ;
                }
                else
                {
                    acl.CreatedBy = Constant.Seeder;
                    acl.CreatedTime = DateTime.Now;
                    context.SysAcls.Add(acl);
                }
                context.SaveChanges();

                r.Item3.ForEach(_role =>
                {
                    var role = roles.FirstOrDefault(e => e.RoleCode.Equals(_role));
                    if (role != null)
                    {
                        var roleAcl = context.SysRoleAcls.FirstOrDefault(
                            e => e.RoleId == role.RoleId && e.AclId == acl.AclId)
                                      ?? new sys_role_acl();

                        roleAcl.RoleId = role.RoleId;
                        roleAcl.AclId = acl.AclId;

                        roleAcl.IsActive = true;
                        roleAcl.LastUpdatedBy = Constant.Seeder;
                        roleAcl.LastUpdatedTime = DateTime.Now;

                        if (roleAcl.RoleAclId > 0)
                        {
                            context.Entry(roleAcl).State = EntityState.Modified;
                        }
                        else
                        {
                            roleAcl.CreatedBy = Constant.Seeder;
                            roleAcl.CreatedTime = DateTime.Now;
                            context.SysRoleAcls.Add(roleAcl);
                        }
                    }
                });
                context.SaveChanges();
            });
            
        }
    }
}
