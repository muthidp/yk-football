﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ApplicationSettingSeeder : DataSeeder<Tuple<string, string>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string>("FILEBUCKET", "/Filebucket/"));
            Records.Add(new Tuple<string, string>("FOOTBALL_API_KEY", "565ec012251f932ea4000001362ef0f8b2df4847719cef5985b9c226"));
        }

        public override void Seed(YK_Database context)
        {
            context.SysApplicationSettings.RemoveRange(context.SysApplicationSettings.ToList());
            context.SaveChanges();

            Records.ForEach(r =>
            {
                var setting = context.SysApplicationSettings.FirstOrDefault(e => e.KeyValue.Equals(r.Item1))
                              ?? new sys_application_setting();

                setting.KeyValue = r.Item1;
                setting.SettingValueDefault = r.Item2;
                setting.IsActive = true;

                setting.CreatedBy = "__SEEDER__";
                setting.CreatedTime = DateTime.Now;
                setting.LastUpdatedBy = "__SEEDER__";
                setting.LastUpdatedTime = DateTime.Now;

                if (setting.ApplicationSettingId > 0)
                {
                    context.Entry(setting).State = EntityState.Modified;
                }
                else
                {
                    context.SysApplicationSettings.Add(setting);
                }
            });
            context.SaveChanges();
        }
    }
}
