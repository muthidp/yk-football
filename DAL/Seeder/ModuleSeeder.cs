﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Seeder
{
    public class ModuleSeeder : DataSeeder<Tuple<string, string, string, string>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string, string, string>("Adv", "master/ad", "Master Data", "ACCESS_MASTER_DATA_ADV"));
            Records.Add(new Tuple<string, string, string, string>("Country", "master/country", "Master Data", "ACCESS_MASTER_DATA_COUNTRY"));
            Records.Add(new Tuple<string, string, string, string>("Category", "master/category", "Master Data", "ACCESS_MASTER_DATA_CATEGORY"));
            Records.Add(new Tuple<string, string, string, string>("League", "master/league", "Master Data", "ACCESS_MASTER_DATA_LEAGUE"));
            Records.Add(new Tuple<string, string, string, string>("Team", "master/team", "Master Data", "ACCESS_MASTER_DATA_TEAM"));
            Records.Add(new Tuple<string, string, string, string>("Tag", "master/tag", "Master Data", "ACCESS_MASTER_DATA_TAG"));
            Records.Add(new Tuple<string, string, string, string>("Add New Post", "post/0", "Master Data", "ADD_NEW_POST"));
            Records.Add(new Tuple<string, string, string, string>("Timeline", "trollTimeline", "ဟာသရပ္ဝန္း", ""));
            Records.Add(new Tuple<string, string, string, string>("Add New Troll", "addTroll", "ဟာသရပ္ဝန္း", ""));
        } 

        public override void Seed(YK_Database context)
        {
            context.SysModules.ToList().ForEach(e =>
            {
                e.IsActive = false;
                context.Entry(e).State = EntityState.Modified;
            });
            context.SaveChanges();

            var moduleGroups = context.SysModuleGroups.Where(e => e.IsActive).ToList();
            var acls = context.SysAcls.Where(e => e.IsActive).ToList();

            Records.ForEach(r =>
            {
                var module = context.SysModules.FirstOrDefault(e => e.Label.Equals(r.Item1))
                             ?? new sys_module();

                module.Label = r.Item1;
                module.Route = r.Item2;

                var mg = moduleGroups.FirstOrDefault(e => e.Label.Equals(r.Item3));
                if (mg != null)
                    module.ModuleGroupId = mg.ModuleGroupId;

                if (String.IsNullOrEmpty(r.Item4))
                {
                    module.AclId = null;
                }
                else
                {
                    var acl = acls.FirstOrDefault(e => e.Key.Equals(r.Item4));
                    if (acl != null)
                        module.AclId = acl.AclId;
                }

                module.IsActive = true;
                module.LastUpdatedBy = Constant.Seeder;
                module.LastUpdatedTime = DateTime.Now;

                if (module.ModuleId > 0)
                {
                    context.Entry(module).State = EntityState.Modified;
                }
                else
                {
                    module.CreatedBy = Constant.Seeder;
                    module.CreatedTime = DateTime.Now;
                    context.SysModules.Add(module);
                }
                context.SaveChanges();

            });
        }
    }
}
