﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Yk;

namespace DAL.Seeder
{
    public class CategorySeeder : DataSeeder<Tuple<string, string>, YK_Database>
    {
        public override void Set()
        {
            Records.Add(new Tuple<string, string>("Breaking News", "Breaking News"));
            Records.Add(new Tuple<string, string>("Features", "Features"));
            Records.Add(new Tuple<string, string>("Interviews", "Interviews"));
            Records.Add(new Tuple<string, string>("Transfers", "Transfers"));
            Records.Add(new Tuple<string, string>("Forecasts", "Forecasts"));
        }

        public override void Seed(YK_Database context)
        {
            Records.ForEach(r =>
            {
                var Category = context.YkCategories.FirstOrDefault(e => e.CategoryCode.Equals(r.Item1))
                           ?? new yk_category();

                Category.CategoryCode = r.Item1;
                Category.CategoryLabel = r.Item2;

                Category.IsActive = true;
                Category.LastUpdatedBy = Constant.Seeder;
                Category.LastUpdatedTime = DateTime.Now;

                if (Category.CategoryId > 0)
                {
                    context.Entry(Category).State = EntityState.Modified;
                }
                else
                {
                    Category.CreatedBy = Constant.Seeder;
                    Category.CreatedTime = DateTime.Now;
                    context.YkCategories.Add(Category);
                }
                context.SaveChanges();
            });
        }
    }
}
