namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editTrollTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_troll", "Title", c => c.String());
            AddColumn("dbo.yk_troll", "ContentUrl", c => c.String());
            AddColumn("dbo.yk_troll", "Likes", c => c.Int(nullable: false));
            AddColumn("dbo.yk_troll", "Shares", c => c.Int(nullable: false));
            DropColumn("dbo.yk_troll", "ImageUrl");
        }
        
        public override void Down()
        {
            AddColumn("dbo.yk_troll", "ImageUrl", c => c.String());
            DropColumn("dbo.yk_troll", "Shares");
            DropColumn("dbo.yk_troll", "Likes");
            DropColumn("dbo.yk_troll", "ContentUrl");
            DropColumn("dbo.yk_troll", "Title");
        }
    }
}
