namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReinitMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sys_application_setting",
                c => new
                    {
                        ApplicationSettingId = c.Long(nullable: false, identity: true),
                        KeyValue = c.String(),
                        SettingValueDefault = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ApplicationSettingId);
            
            CreateTable(
                "dbo.md_media_asset",
                c => new
                    {
                        MediaAssetId = c.Long(nullable: false, identity: true),
                        MediaDescriptorId = c.Long(),
                        Title = c.String(),
                        Description = c.String(),
                        Filename = c.String(),
                        RelativePath = c.String(),
                        AbsolutePath = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MediaAssetId)
                .ForeignKey("dbo.md_media_descriptor", t => t.MediaDescriptorId)
                .Index(t => t.MediaDescriptorId);
            
            CreateTable(
                "dbo.md_media_descriptor",
                c => new
                    {
                        MediaDescriptorId = c.Long(nullable: false, identity: true),
                        MediaDescriptorLabel = c.String(),
                        MimeTypeDefinition = c.String(),
                        Previewable = c.Boolean(nullable: false),
                        DefaultThumbnailDisplay = c.String(),
                        MediaType = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MediaDescriptorId);
            
            CreateTable(
                "dbo.sys_acl",
                c => new
                    {
                        AclId = c.Long(nullable: false, identity: true),
                        Key = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.AclId);
            
            CreateTable(
                "dbo.sys_institution",
                c => new
                    {
                        InstitutionId = c.Long(nullable: false, identity: true),
                        InstitutionCode = c.String(),
                        InstitutionLabel = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.InstitutionId);
            
            CreateTable(
                "dbo.sys_module_group",
                c => new
                    {
                        ModuleGroupId = c.Long(nullable: false, identity: true),
                        Label = c.String(),
                        Route = c.String(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ModuleGroupId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_module",
                c => new
                    {
                        ModuleId = c.Long(nullable: false, identity: true),
                        Label = c.String(),
                        Route = c.String(),
                        ModuleGroupId = c.Long(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ModuleId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .ForeignKey("dbo.sys_module_group", t => t.ModuleGroupId)
                .Index(t => t.ModuleGroupId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_role_acl",
                c => new
                    {
                        RoleAclId = c.Long(nullable: false, identity: true),
                        RoleId = c.Long(),
                        AclId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.RoleAclId)
                .ForeignKey("dbo.sys_acl", t => t.AclId)
                .ForeignKey("dbo.sys_role", t => t.RoleId)
                .Index(t => t.RoleId)
                .Index(t => t.AclId);
            
            CreateTable(
                "dbo.sys_role",
                c => new
                    {
                        RoleId = c.Long(nullable: false, identity: true),
                        RoleCode = c.String(),
                        RoleLabel = c.String(),
                        DefaultRoute = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.sys_user",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        Username = c.String(nullable: false, maxLength: 40),
                        Password = c.String(maxLength: 40),
                        Email = c.String(maxLength: 120),
                        RoleId = c.Long(),
                        InstitutionId = c.Long(),
                        Deactivated = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.sys_institution", t => t.InstitutionId)
                .ForeignKey("dbo.sys_role", t => t.RoleId)
                .Index(t => t.Username)
                .Index(t => t.RoleId)
                .Index(t => t.InstitutionId);
            
            CreateTable(
                "dbo.yk_category",
                c => new
                    {
                        CategoryId = c.Long(nullable: false, identity: true),
                        CategoryCode = c.String(),
                        CategoryLabel = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.yk_comment",
                c => new
                    {
                        CommentId = c.Long(nullable: false, identity: true),
                        PostId = c.Long(),
                        Content = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.yk_post", t => t.PostId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.yk_post",
                c => new
                    {
                        PostId = c.Long(nullable: false, identity: true),
                        UserId = c.Long(),
                        CategoryId = c.Long(),
                        TeamId = c.Long(),
                        Title = c.String(),
                        Body = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.PostId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .ForeignKey("dbo.yk_category", t => t.CategoryId)
                .ForeignKey("dbo.yk_team", t => t.TeamId)
                .Index(t => t.UserId)
                .Index(t => t.CategoryId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "dbo.yk_team",
                c => new
                    {
                        TeamId = c.Long(nullable: false, identity: true),
                        TeamCode = c.String(),
                        TeamLabel = c.String(),
                        Description = c.String(),
                        LogoUrl = c.String(),
                        LeagueId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("dbo.yk_league", t => t.LeagueId)
                .Index(t => t.LeagueId);
            
            CreateTable(
                "dbo.yk_league",
                c => new
                    {
                        LeagueId = c.Long(nullable: false, identity: true),
                        LeagueCode = c.String(),
                        LeagueLabel = c.String(),
                        Description = c.String(),
                        LogoUrl = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.LeagueId);
            
            CreateTable(
                "dbo.yk_post_tag",
                c => new
                    {
                        PostTagId = c.Long(nullable: false, identity: true),
                        PostId = c.Long(),
                        TagId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.PostTagId)
                .ForeignKey("dbo.yk_post", t => t.PostId)
                .ForeignKey("dbo.yk_tag", t => t.TagId)
                .Index(t => t.PostId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.yk_tag",
                c => new
                    {
                        TagId = c.Long(nullable: false, identity: true),
                        TagLabel = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_post_tag", "TagId", "dbo.yk_tag");
            DropForeignKey("dbo.yk_post_tag", "PostId", "dbo.yk_post");
            DropForeignKey("dbo.yk_comment", "PostId", "dbo.yk_post");
            DropForeignKey("dbo.yk_post", "TeamId", "dbo.yk_team");
            DropForeignKey("dbo.yk_team", "LeagueId", "dbo.yk_league");
            DropForeignKey("dbo.yk_post", "CategoryId", "dbo.yk_category");
            DropForeignKey("dbo.yk_post", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.sys_user", "RoleId", "dbo.sys_role");
            DropForeignKey("dbo.sys_user", "InstitutionId", "dbo.sys_institution");
            DropForeignKey("dbo.sys_role_acl", "RoleId", "dbo.sys_role");
            DropForeignKey("dbo.sys_role_acl", "AclId", "dbo.sys_acl");
            DropForeignKey("dbo.sys_module", "ModuleGroupId", "dbo.sys_module_group");
            DropForeignKey("dbo.sys_module", "AclId", "dbo.sys_acl");
            DropForeignKey("dbo.sys_module_group", "AclId", "dbo.sys_acl");
            DropForeignKey("dbo.md_media_asset", "MediaDescriptorId", "dbo.md_media_descriptor");
            DropIndex("dbo.yk_post_tag", new[] { "TagId" });
            DropIndex("dbo.yk_post_tag", new[] { "PostId" });
            DropIndex("dbo.yk_team", new[] { "LeagueId" });
            DropIndex("dbo.yk_post", new[] { "TeamId" });
            DropIndex("dbo.yk_post", new[] { "CategoryId" });
            DropIndex("dbo.yk_post", new[] { "UserId" });
            DropIndex("dbo.yk_comment", new[] { "PostId" });
            DropIndex("dbo.sys_user", new[] { "InstitutionId" });
            DropIndex("dbo.sys_user", new[] { "RoleId" });
            DropIndex("dbo.sys_user", new[] { "Username" });
            DropIndex("dbo.sys_role_acl", new[] { "AclId" });
            DropIndex("dbo.sys_role_acl", new[] { "RoleId" });
            DropIndex("dbo.sys_module", new[] { "AclId" });
            DropIndex("dbo.sys_module", new[] { "ModuleGroupId" });
            DropIndex("dbo.sys_module_group", new[] { "AclId" });
            DropIndex("dbo.md_media_asset", new[] { "MediaDescriptorId" });
            DropTable("dbo.yk_tag");
            DropTable("dbo.yk_post_tag");
            DropTable("dbo.yk_league");
            DropTable("dbo.yk_team");
            DropTable("dbo.yk_post");
            DropTable("dbo.yk_comment");
            DropTable("dbo.yk_category");
            DropTable("dbo.sys_user");
            DropTable("dbo.sys_role");
            DropTable("dbo.sys_role_acl");
            DropTable("dbo.sys_module");
            DropTable("dbo.sys_module_group");
            DropTable("dbo.sys_institution");
            DropTable("dbo.sys_acl");
            DropTable("dbo.md_media_descriptor");
            DropTable("dbo.md_media_asset");
            DropTable("dbo.sys_application_setting");
        }
    }
}
