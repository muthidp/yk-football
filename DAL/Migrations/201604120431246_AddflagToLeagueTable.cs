namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddflagToLeagueTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_league", "IsCupCompetition", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_league", "IsCupCompetition");
        }
    }
}
