// <auto-generated />
namespace DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addMoreCommentTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addMoreCommentTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201603111550135_addMoreCommentTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
