namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSequnceForModuleGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sys_module_group", "Sequence", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.sys_module_group", "Sequence");
        }
    }
}
