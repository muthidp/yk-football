namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMatchStaticId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_match", "FootballMatchStaticId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_match", "FootballMatchStaticId");
        }
    }
}
