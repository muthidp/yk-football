namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_ad",
                c => new
                    {
                        AdId = c.Long(nullable: false, identity: true),
                        AdIdentifier = c.String(),
                        LocationPage = c.String(),
                        Width = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        ImageUrl = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.AdId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.yk_ad");
        }
    }
}
