namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMatchEvent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_match_event", "FootballMatchEventId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_match_event", "FootballMatchEventId");
        }
    }
}
