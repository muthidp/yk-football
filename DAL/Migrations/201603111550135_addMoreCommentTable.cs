namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMoreCommentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_comment_reply",
                c => new
                    {
                        CommentReplyId = c.Long(nullable: false, identity: true),
                        CommentId = c.Long(),
                        UserId = c.Long(),
                        Content = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CommentReplyId)
                .ForeignKey("dbo.yk_comment", t => t.CommentId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.CommentId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.yk_comment_vote",
                c => new
                    {
                        CommentVoteId = c.Long(nullable: false, identity: true),
                        CommentId = c.Long(),
                        UserId = c.Long(),
                        IsUpVote = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CommentVoteId)
                .ForeignKey("dbo.yk_comment", t => t.CommentId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.CommentId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.yk_troll_comment_reply",
                c => new
                    {
                        TrollCommentReplyId = c.Long(nullable: false, identity: true),
                        CommentId = c.Long(),
                        UserId = c.Long(),
                        Content = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TrollCommentReplyId)
                .ForeignKey("dbo.yk_troll_comment", t => t.CommentId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.CommentId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_troll_comment_reply", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_troll_comment_reply", "CommentId", "dbo.yk_troll_comment");
            DropForeignKey("dbo.yk_comment_reply", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_comment_reply", "CommentId", "dbo.yk_comment");
            DropForeignKey("dbo.yk_comment_vote", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_comment_vote", "CommentId", "dbo.yk_comment");
            DropIndex("dbo.yk_troll_comment_reply", new[] { "UserId" });
            DropIndex("dbo.yk_troll_comment_reply", new[] { "CommentId" });
            DropIndex("dbo.yk_comment_vote", new[] { "UserId" });
            DropIndex("dbo.yk_comment_vote", new[] { "CommentId" });
            DropIndex("dbo.yk_comment_reply", new[] { "UserId" });
            DropIndex("dbo.yk_comment_reply", new[] { "CommentId" });
            DropTable("dbo.yk_troll_comment_reply");
            DropTable("dbo.yk_comment_vote");
            DropTable("dbo.yk_comment_reply");
        }
    }
}
