namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCountry : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_country",
                c => new
                    {
                        CountryId = c.Long(nullable: false, identity: true),
                        CountryCode = c.String(),
                        CountryLabel = c.String(),
                        LogoUrl = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CountryId);
            
            AddColumn("dbo.yk_league", "CountryId", c => c.Long());
            CreateIndex("dbo.yk_league", "CountryId");
            AddForeignKey("dbo.yk_league", "CountryId", "dbo.yk_country", "CountryId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_league", "CountryId", "dbo.yk_country");
            DropIndex("dbo.yk_league", new[] { "CountryId" });
            DropColumn("dbo.yk_league", "CountryId");
            DropTable("dbo.yk_country");
        }
    }
}
