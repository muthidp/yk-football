namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePostTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_post", "LeagueId", c => c.Long());
            AddColumn("dbo.yk_post", "PictureUrl", c => c.String());
            CreateIndex("dbo.yk_post", "LeagueId");
            AddForeignKey("dbo.yk_post", "LeagueId", "dbo.yk_league", "LeagueId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_post", "LeagueId", "dbo.yk_league");
            DropIndex("dbo.yk_post", new[] { "LeagueId" });
            DropColumn("dbo.yk_post", "PictureUrl");
            DropColumn("dbo.yk_post", "LeagueId");
        }
    }
}
