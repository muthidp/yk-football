using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using DAL.Seeder;


namespace DAL.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<YK_Database>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private static string GetSeed(string file)
        {
            var baseDir = string.Format("{0}/../../Seeder", AppDomain.CurrentDomain.BaseDirectory);
            return File.ReadAllText(string.Format("{0}/{1}", baseDir, file));
        }

        protected override void Seed(YK_Database context)
        {
            new ApplicationSettingSeeder().Execute(context);
            new MediaDescriptorSeeder().Execute(context);

            new InstitutionSeeder().Execute(context);
            new RoleSeeder().Execute(context);
            new AclSeeder().Execute(context);
            new ModuleGroupSeeder().Execute(context);
            new ModuleSeeder().Execute(context);
            new UserSeeder().Execute(context);

            new CountrySeeder().Execute(context);
            new CategorySeeder().Execute(context);
            new LeagueSeeder().Execute(context);
            new TeamSeeder().Execute(context);

            new AdsSeeder().Execute(context);
        }

        private static void DropObjects(DbContext context)
        {
            //context.Database.ExecuteSqlCommand(new sp_object().TsqlScriptDrop());
        }

        private static void CreateObjects(DbContext context)
        {
           
        }
    }
}
