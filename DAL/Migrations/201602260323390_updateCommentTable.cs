namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateCommentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_comment", "UserId", c => c.Long());
            CreateIndex("dbo.yk_comment", "UserId");
            AddForeignKey("dbo.yk_comment", "UserId", "dbo.sys_user", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_comment", "UserId", "dbo.sys_user");
            DropIndex("dbo.yk_comment", new[] { "UserId" });
            DropColumn("dbo.yk_comment", "UserId");
        }
    }
}
