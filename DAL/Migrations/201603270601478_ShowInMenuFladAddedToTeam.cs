namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ShowInMenuFladAddedToTeam : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_team", "ShowInMenu", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_team", "ShowInMenu");
        }
    }
}
