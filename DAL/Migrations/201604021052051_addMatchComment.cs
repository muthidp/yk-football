namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMatchComment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_match_comment",
                c => new
                    {
                        MatchCommentId = c.Long(nullable: false, identity: true),
                        MatchId = c.Long(),
                        Comment = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchCommentId)
                .ForeignKey("dbo.yk_match", t => t.MatchId)
                .Index(t => t.MatchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_match_comment", "MatchId", "dbo.yk_match");
            DropIndex("dbo.yk_match_comment", new[] { "MatchId" });
            DropTable("dbo.yk_match_comment");
        }
    }
}
