namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userProfilePict : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sys_user", "ProfilePicture", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.sys_user", "ProfilePicture");
        }
    }
}
