namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class matchTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_match",
                c => new
                    {
                        MatchId = c.Long(nullable: false, identity: true),
                        FootballApiId = c.Long(),
                        MatchDateTime = c.DateTime(nullable: false),
                        LocalTeamId = c.Long(),
                        LocalTeamScore = c.String(),
                        VisitorTeamId = c.Long(),
                        VisitorTeamScore = c.String(),
                        MatchStatus = c.String(),
                        MatchEtScore = c.String(),
                        MatchHtScore = c.String(),
                        MatchFtScore = c.String(),
                        MatchCommentaryAvailable = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("dbo.yk_team", t => t.LocalTeamId)
                .ForeignKey("dbo.yk_team", t => t.VisitorTeamId)
                .Index(t => t.LocalTeamId)
                .Index(t => t.VisitorTeamId);
            
            AddColumn("dbo.yk_league", "FootballApiId", c => c.Long());
            AddColumn("dbo.yk_team", "FootballApiId", c => c.Long());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_match", "VisitorTeamId", "dbo.yk_team");
            DropForeignKey("dbo.yk_match", "LocalTeamId", "dbo.yk_team");
            DropIndex("dbo.yk_match", new[] { "VisitorTeamId" });
            DropIndex("dbo.yk_match", new[] { "LocalTeamId" });
            DropColumn("dbo.yk_team", "FootballApiId");
            DropColumn("dbo.yk_league", "FootballApiId");
            DropTable("dbo.yk_match");
        }
    }
}
