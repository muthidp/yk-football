namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtrollvote : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_troll_comment_vote",
                c => new
                    {
                        CommentVoteId = c.Long(nullable: false, identity: true),
                        CommentId = c.Long(),
                        UserId = c.Long(),
                        IsUpVote = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.CommentVoteId)
                .ForeignKey("dbo.yk_troll_comment", t => t.CommentId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.CommentId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_troll_comment_vote", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_troll_comment_vote", "CommentId", "dbo.yk_troll_comment");
            DropIndex("dbo.yk_troll_comment_vote", new[] { "UserId" });
            DropIndex("dbo.yk_troll_comment_vote", new[] { "CommentId" });
            DropTable("dbo.yk_troll_comment_vote");
        }
    }
}
