namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserUniqueId : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.sys_user", new[] { "Username" });
            AddColumn("dbo.sys_user", "UserUniqueId", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.sys_user", "Username", c => c.String(nullable: false));
            CreateIndex("dbo.sys_user", "UserUniqueId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.sys_user", new[] { "UserUniqueId" });
            AlterColumn("dbo.sys_user", "Username", c => c.String(nullable: false, maxLength: 40));
            DropColumn("dbo.sys_user", "UserUniqueId");
            CreateIndex("dbo.sys_user", "Username");
        }
    }
}
