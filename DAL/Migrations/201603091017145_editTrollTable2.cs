namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editTrollTable2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_troll_user_like",
                c => new
                    {
                        TrollUserLikeId = c.Long(nullable: false, identity: true),
                        TrollId = c.Long(),
                        UserId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TrollUserLikeId)
                .ForeignKey("dbo.yk_troll", t => t.TrollId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.TrollId)
                .Index(t => t.UserId);
            
            DropColumn("dbo.yk_troll", "Likes");
        }
        
        public override void Down()
        {
            AddColumn("dbo.yk_troll", "Likes", c => c.Int(nullable: false));
            DropForeignKey("dbo.yk_troll_user_like", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_troll_user_like", "TrollId", "dbo.yk_troll");
            DropIndex("dbo.yk_troll_user_like", new[] { "UserId" });
            DropIndex("dbo.yk_troll_user_like", new[] { "TrollId" });
            DropTable("dbo.yk_troll_user_like");
        }
    }
}
