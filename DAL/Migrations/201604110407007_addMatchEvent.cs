namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMatchEvent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_match_event",
                c => new
                    {
                        MatchEventId = c.Long(nullable: false, identity: true),
                        MatchId = c.Long(),
                        FootballMatchId = c.Long(),
                        Type = c.String(),
                        Minute = c.Int(nullable: false),
                        ExtraMinute = c.Int(),
                        TeamId = c.Long(),
                        FootballTeamId = c.Long(),
                        Player = c.String(),
                        Assist = c.String(),
                        Result = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchEventId)
                .ForeignKey("dbo.yk_match", t => t.MatchId)
                .ForeignKey("dbo.yk_team", t => t.TeamId)
                .Index(t => t.MatchId)
                .Index(t => t.TeamId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_match_event", "TeamId", "dbo.yk_team");
            DropForeignKey("dbo.yk_match_event", "MatchId", "dbo.yk_match");
            DropIndex("dbo.yk_match_event", new[] { "TeamId" });
            DropIndex("dbo.yk_match_event", new[] { "MatchId" });
            DropTable("dbo.yk_match_event");
        }
    }
}
