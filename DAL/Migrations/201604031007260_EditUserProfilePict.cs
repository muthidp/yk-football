namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUserProfilePict : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.sys_user", "UserAvatar", c => c.String());
            DropColumn("dbo.sys_user", "ProfilePicture");
        }
        
        public override void Down()
        {
            AddColumn("dbo.sys_user", "ProfilePicture", c => c.String());
            DropColumn("dbo.sys_user", "UserAvatar");
        }
    }
}
