namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSectionForCountry : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_country", "SectionInHomePage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_country", "SectionInHomePage");
        }
    }
}
