namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatePostTable1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_post", "Read", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_post", "Read");
        }
    }
}
