namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTrollTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.yk_troll_comment",
                c => new
                    {
                        TrollCommentId = c.Long(nullable: false, identity: true),
                        TrollId = c.Long(),
                        UserId = c.Long(),
                        Content = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TrollCommentId)
                .ForeignKey("dbo.yk_troll", t => t.TrollId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.TrollId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.yk_troll",
                c => new
                    {
                        TrollId = c.Long(nullable: false, identity: true),
                        ImageUrl = c.String(),
                        UserId = c.Long(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.TrollId)
                .ForeignKey("dbo.sys_user", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.yk_troll_comment", "UserId", "dbo.sys_user");
            DropForeignKey("dbo.yk_troll_comment", "TrollId", "dbo.yk_troll");
            DropForeignKey("dbo.yk_troll", "UserId", "dbo.sys_user");
            DropIndex("dbo.yk_troll", new[] { "UserId" });
            DropIndex("dbo.yk_troll_comment", new[] { "UserId" });
            DropIndex("dbo.yk_troll_comment", new[] { "TrollId" });
            DropTable("dbo.yk_troll");
            DropTable("dbo.yk_troll_comment");
        }
    }
}
