namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editmatchtable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.yk_match", "FootballLeagueId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.yk_match", "FootballLeagueId");
        }
    }
}
