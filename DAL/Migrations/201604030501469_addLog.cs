namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.sys_log",
                c => new
                    {
                        LogId = c.Long(nullable: false, identity: true),
                        LogIdentifier = c.String(),
                        LogDetail = c.String(),
                        Status = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.String(),
                        CreatedTime = c.DateTime(),
                        LastUpdatedBy = c.String(),
                        LastUpdatedTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.LogId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.sys_log");
        }
    }
}
