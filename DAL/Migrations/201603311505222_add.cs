namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.yk_match", "LocalTeamId", "dbo.yk_team");
            DropForeignKey("dbo.yk_match", "VisitorTeamId", "dbo.yk_team");
            DropIndex("dbo.yk_match", new[] { "LocalTeamId" });
            DropIndex("dbo.yk_match", new[] { "VisitorTeamId" });
            AddColumn("dbo.yk_match", "FootballMatchId", c => c.Long());
            AddColumn("dbo.yk_match", "FootballlLocalTeamId", c => c.Long());
            AddColumn("dbo.yk_match", "FootballlLocalTeamName", c => c.String());
            AddColumn("dbo.yk_match", "FootballlVisitorTeamId", c => c.Long());
            AddColumn("dbo.yk_match", "FootballlLocalVisitorName", c => c.String());
            AlterColumn("dbo.yk_match", "MatchDateTime", c => c.DateTime());
            DropColumn("dbo.yk_match", "FootballApiId");
            DropColumn("dbo.yk_match", "LocalTeamId");
            DropColumn("dbo.yk_match", "VisitorTeamId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.yk_match", "VisitorTeamId", c => c.Long());
            AddColumn("dbo.yk_match", "LocalTeamId", c => c.Long());
            AddColumn("dbo.yk_match", "FootballApiId", c => c.Long());
            AlterColumn("dbo.yk_match", "MatchDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.yk_match", "FootballlLocalVisitorName");
            DropColumn("dbo.yk_match", "FootballlVisitorTeamId");
            DropColumn("dbo.yk_match", "FootballlLocalTeamName");
            DropColumn("dbo.yk_match", "FootballlLocalTeamId");
            DropColumn("dbo.yk_match", "FootballMatchId");
            CreateIndex("dbo.yk_match", "VisitorTeamId");
            CreateIndex("dbo.yk_match", "LocalTeamId");
            AddForeignKey("dbo.yk_match", "VisitorTeamId", "dbo.yk_team", "TeamId");
            AddForeignKey("dbo.yk_match", "LocalTeamId", "dbo.yk_team", "TeamId");
        }
    }
}
