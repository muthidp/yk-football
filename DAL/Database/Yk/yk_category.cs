﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_category : BaseEntityModel
    {
        [Key]
        public long CategoryId { get; set; }

        public string CategoryCode { get; set; }

        public string CategoryLabel { get; set; }
    }
}
