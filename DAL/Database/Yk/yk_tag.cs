﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_tag : BaseEntityModel
    {
        [Key]
        public long TagId { get; set; }

        public string TagLabel { get; set; }
    }
}
