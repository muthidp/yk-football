﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_troll : BaseEntityModel
    {
        [Key]
        public long TrollId { get; set; }

        public string Title { get; set; }

        public string ContentUrl { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }

        public int Shares { get; set; }

        public virtual ICollection<yk_troll_comment> Comments { get; set; }
        public virtual ICollection<yk_troll_user_like> UserLikes { get; set; } 
    }
}
