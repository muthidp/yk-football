﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_match : BaseEntityModel
    {
        [Key]
        public long MatchId { get; set; }

        public long? FootballMatchId { get; set; }

        // match_id for API v2.0
        public long? FootballMatchStaticId { get; set; }

        public long? FootballLeagueId { get; set; }

        public DateTime? MatchDateTime { get; set; }

        public long? FootballlLocalTeamId { get; set; }
        public string FootballlLocalTeamName { get; set; }
        public string LocalTeamScore { get; set; }

        public long? FootballlVisitorTeamId { get; set; }
        public string FootballlLocalVisitorName { get; set; }
        public string VisitorTeamScore { get; set; }

        public string MatchStatus { get; set; }

        public string MatchEtScore { get; set; }
        public string MatchHtScore { get; set; }
        public string MatchFtScore { get; set; }

        public string MatchCommentaryAvailable { get; set; }

        public virtual ICollection<yk_match_event> Events { get; set; } 

    }
}
