﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Database.Yk
{
    public class yk_team : BaseEntityModel
    {
        [Key]
        public long TeamId { get; set; }

        public string TeamCode { get; set; }

        public string TeamLabel { get; set; }

        public string Description { get; set; }

        public string LogoUrl { get; set; }

        public bool ShowInMenu { get; set; }

        [ForeignKey("League")]
        public long? LeagueId { get; set; }
        public virtual yk_league League { get; set; }

        public long? FootballApiId { get; set; }
    }
}
