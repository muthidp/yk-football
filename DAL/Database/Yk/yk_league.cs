﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_league : BaseEntityModel
    {
        [Key]
        public long LeagueId { get; set; }

        public string LeagueCode { get; set; }

        public string LeagueLabel { get; set; }

        public string Description { get; set; }

        public string LogoUrl { get; set; }

        public long? FootballApiId { get; set; }

        public bool IsCupCompetition { get; set; }

        [ForeignKey("Country")]
        public long? CountryId { get; set; }
        public virtual yk_country Country { get; set; }

        public virtual ICollection<yk_team> Teams { get; set; } 
    }
}
