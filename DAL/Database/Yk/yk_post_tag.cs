﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_post_tag : BaseEntityModel
    {
        [Key]
        public long PostTagId { get; set; }

        [ForeignKey("Post")]
        public long? PostId { get; set; }
        public virtual yk_post Post { get; set; }

        [ForeignKey("Tag")]
        public long? TagId { get; set; }
        public virtual yk_tag Tag { get; set; }
    }
}
