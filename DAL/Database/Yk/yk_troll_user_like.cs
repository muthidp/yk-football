﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_troll_user_like : BaseEntityModel
    {
        [Key]
        public long TrollUserLikeId { get; set; }

        [ForeignKey("Troll")]
        public long? TrollId { get; set; }
        public virtual yk_troll Troll { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }
    }
}
