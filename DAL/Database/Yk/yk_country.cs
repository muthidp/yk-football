﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_country : BaseEntityModel
    {
        [Key]
        public long CountryId { get; set; }

        public string CountryCode { get; set; }

        public string CountryLabel { get; set; }

        public string LogoUrl { get; set; }

        public int SectionInHomePage { get; set; }
    }
}
