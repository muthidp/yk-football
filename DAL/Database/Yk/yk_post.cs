﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_post : BaseEntityModel
    {
        [Key]
        public long PostId { get; set; }

        [ForeignKey("Author")]
        public long? UserId { get; set; }

        public virtual sys_user Author { get; set; }

        [ForeignKey("Category")]
        public long? CategoryId { get; set; }

        public virtual yk_category Category { get; set; }

        [ForeignKey("League")]
        public long? LeagueId { get; set; }

        public virtual yk_league League { get; set; }

        [ForeignKey("Team")]
        public long? TeamId { get; set; }

        public virtual yk_team Team { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public string PictureUrl { get; set; }

        public int Read { get; set; }

        public virtual ICollection<yk_post_tag> PostTags { get; set; } 
    }
}
