﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_match_comment : BaseEntityModel
    {
        [Key]
        public long MatchCommentId { get; set; }

        [ForeignKey("Match")]
        public long? MatchId { get; set; }
        public virtual yk_match Match { get; set; }

        public string Comment { get; set; }
    }
}
