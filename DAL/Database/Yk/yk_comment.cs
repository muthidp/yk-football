﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_comment : BaseEntityModel
    {
        [Key]
        public long CommentId { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }

        [ForeignKey("Post")]
        public long? PostId { get; set; }
        public virtual yk_post Post { get; set; }

        public string Content { get; set; }

        public virtual ICollection<yk_comment_reply> CommentReplies { get; set; } 
        public virtual ICollection<yk_comment_vote> CommentVotes { get; set; } 
    }
}
