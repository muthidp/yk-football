﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Yk
{
    public class yk_match_event : BaseEntityModel
    {
        [Key]
        public long MatchEventId { get; set; }

        public long? MatchId { get; set; }
        public virtual yk_match Match { get; set; }

        public long? FootballMatchEventId { get; set; }

        public long? FootballMatchId { get; set; }

        public string Type { get; set; }

        public int Minute { get; set; }

        public int? ExtraMinute { get; set; }

        [ForeignKey("Team")]
        public long? TeamId { get; set; }
        public virtual yk_team Team { get; set; }

        public long? FootballTeamId { get; set; }

        public string Player { get; set; }

        public string Assist { get; set; }

        public string Result { get; set; }
    }
}
