﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Database.Yk
{
    public class yk_ad : BaseEntityModel
    {
        [Key]
        public long AdId { get; set; }

        public string AdIdentifier { get; set; }

        public string LocationPage { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public string ImageUrl { get; set; }
    }
}
