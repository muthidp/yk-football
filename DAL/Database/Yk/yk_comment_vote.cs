﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_comment_vote : BaseEntityModel
    {
        [Key]
        public long CommentVoteId { get; set; }

        [ForeignKey("Comment")]
        public long? CommentId { get; set; }
        public virtual yk_comment Comment { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }

        public bool IsUpVote { get; set; }
    }
}
