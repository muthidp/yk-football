﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Yk
{
    public class yk_troll_comment_reply : BaseEntityModel
    {
        [Key]
        public long TrollCommentReplyId { get; set; }

        [ForeignKey("Comment")]
        public long? CommentId { get; set; }
        public virtual yk_troll_comment Comment { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual sys_user User { get; set; }

        public string Content { get; set; }
    }
}
