﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_institution : BaseEntityModel
    {
        [Key]
        public long InstitutionId { get; set; }

        public string InstitutionCode { get; set; }

        public string InstitutionLabel { get; set; }
    }
}
