﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_log : BaseEntityModel
    {
        [Key]
        public long LogId { get; set; }

        public string LogIdentifier { get; set; }

        public string LogDetail { get; set; }

        public LogStatus Status { get; set; }
    }

    public enum LogStatus
    {
        OK,
        ERROR
    }
}
