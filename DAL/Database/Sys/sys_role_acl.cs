﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_role_acl : BaseEntityModel
    {
        [Key]
        public long RoleAclId { get; set; }

        [ForeignKey("Role")]
        public long? RoleId { get; set; }
        public virtual sys_role Role { get; set; }

        [ForeignKey("Acl")]
        public long? AclId { get; set; }
        public virtual sys_acl Acl { get; set; }
    }
}
