﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_module : BaseEntityModel
    {
        [Key]
        public long ModuleId { get; set; }

        public string Label { get; set; }

        public string Route { get; set; }

        [ForeignKey("ModuleGroup")]
        public long? ModuleGroupId { get; set; }
        public virtual sys_module_group ModuleGroup { get; set; }

        [ForeignKey("Acl")]
        public long? AclId { get; set; }
        public virtual sys_acl Acl { get; set; }
    }
}
