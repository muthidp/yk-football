﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_application_setting : BaseEntityModel
    {
        [Key]
        public long ApplicationSettingId { get; set; }

        public string KeyValue { get; set; }

        public string SettingValueDefault { get; set; }
    }
}
