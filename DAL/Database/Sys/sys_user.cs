﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;

namespace DAL.Database.Sys
{
    public class sys_user : BaseEntityModel
    {

        [Key] // Primary key
        public long UserId { get; set; }

        [Required]
        [Index]
        [Display(Name = "User Unique Id")]
        [StringLength(40), MinLength(3), MaxLength(40)]
        public string UserUniqueId { get; set; }

        [Required]
        public string Username { get; set; }

        [Display(Name = "Password")]
        [StringLength(40), MaxLength(40)]
        public string Password { get; set; }

        [Display(Name = "eMail Address")]
        [StringLength(120), MaxLength(120)]
        public string Email { get; set; }

        [ForeignKey("Role")]
        public long? RoleId { get; set; }
        public virtual sys_role Role { get; set; }

        [ForeignKey("Institution")]
        public long? InstitutionId { get; set; }
        public virtual sys_institution Institution { get; set; }

        [Display(Name = "Deactivated")]
        public bool Deactivated { get; set; }

        public string UserAvatar { get; set; }

        //Either local or facebook
        public string Provider { get; set; }

    }
}
