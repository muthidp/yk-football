﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using DAL.Database.Sys;

namespace DAL.Database.Md
{
    public class md_media_asset : BaseEntityModel
    {
        [Key]
        public long MediaAssetId { get; set; }

        public long? MediaDescriptorId { get; set; }
        public virtual md_media_descriptor MediaDescriptor { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Filename { get; set; }

        public string RelativePath { get; set; }

        public string AbsolutePath { get; set; }
    }
}
