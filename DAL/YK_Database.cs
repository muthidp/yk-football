﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DAL.Database;
using DAL.Database.Md;
using DAL.Database.Sys;
using DAL.Database.Yk;

namespace DAL
{
    public class YK_Database : DbContext
    {
        public YK_Database()
            : base("YK_Database")
        {

        }

        public DbSet<md_media_asset> MdMediaAssets { get; set; }
        public DbSet<md_media_descriptor> MdMediaDescriptors { get; set; }

        public DbSet<sys_acl> SysAcls { get; set; }
        public DbSet<sys_application_setting> SysApplicationSettings { get; set; }
        public DbSet<sys_institution> SysInstitutions { get; set; }
        public DbSet<sys_module> SysModules { get; set; }
        public DbSet<sys_module_group> SysModuleGroups { get; set; }
        public DbSet<sys_role> SysRoles { get; set; }
        public DbSet<sys_role_acl> SysRoleAcls { get; set; }
        public DbSet<sys_user> SysUsers { get; set; }
        public DbSet<sys_log> SysLogs { get; set; }

        public DbSet<yk_category> YkCategories { get; set; }
        public DbSet<yk_comment> YkComments { get; set; }
        public DbSet<yk_country> YkCountries { get; set; }
        public DbSet<yk_post> YkPosts { get; set; }
        public DbSet<yk_post_tag> YkPostTags { get; set; }
        public DbSet<yk_tag> YkTags { get; set; }
        public DbSet<yk_team> YkTeams { get; set; }
        public DbSet<yk_league> YkLeagues { get; set; }
        public DbSet<yk_ad> YkAds { get; set; }

        public DbSet<yk_troll> YkTrolls { get; set; }
        public DbSet<yk_troll_comment> YkTrollComments { get; set; }
        public DbSet<yk_troll_user_like> YkTrollUserLikes { get; set; }

        public DbSet<yk_comment_reply> YkCommentReplies { get; set; }
        public DbSet<yk_comment_vote> YkCommentVotes { get; set; }
        public DbSet<yk_troll_comment_vote> YkTrollCommentVotes { get; set; }
        public DbSet<yk_troll_comment_reply> YkTrollCommentReplies { get; set; }

        public DbSet<yk_match> YkMatches { get; set; }
        public DbSet<yk_match_comment> YkMatchComments { get; set; }
        public DbSet<yk_match_event> YkMatchEvents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            System.Data.Entity.Database.SetInitializer<YK_Database>(null);
            //base.OnModelCreating(modelBuilder);
        }

    }
}