﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Seeder
{
    public abstract class DataSeeder<T, TContext>
    {
        protected DataSeeder()
        {
            Records = new List<T>();
        }

        public void Execute(TContext context)
        {
            Set();
            Seed(context);
        }

        public List<T> Records { get; set; }
        public abstract void Set();
        public abstract void Seed(TContext context);
    }
}
